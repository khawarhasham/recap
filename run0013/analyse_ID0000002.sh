#!/bin/bash
set -e
pegasus_lite_version_major="4"
pegasus_lite_version_minor="2"
pegasus_lite_version_patch="0"

. pegasus-lite-common.sh

pegasus_lite_init

# cleanup in case of failures
trap pegasus_lite_exit INT TERM EXIT

# work dir
pegasus_lite_setup_work_dir

# figure out the worker package to use
pegasus_lite_worker_package

# stage in 
pegasus-transfer 1>&2 << EOF
# src 1 condorpool
s3://admin@uweopenstack/wflocalscratch19082014114904/khawar/pegasus/WordCountWF/run0013/wordlist1
# dst 1 condorpool
file://$PWD/wordlist1
# src 2 condorpool
s3://admin@uweopenstack/wflocalscratch19082014114904/khawar/pegasus/WordCountWF/run0013/wordcount-analyse-1.0
# dst 2 condorpool
file://$PWD/wordcount-analyse-1.0
EOF

# set the xbit for any executables staged
/bin/chmod +x wordcount-analyse-1.0 

# execute the tasks
pegasus-kickstart -n wordcount::analyse:1.0 -N ID0000002 -R condorpool -L WordCountWF -T 2014-08-19T11:49:04+01:00 ./wordcount-analyse-1.0  -i  wordlist1  -o  analysis1 

# stage out
pegasus-transfer 1>&2 << EOF
# src 1 condorpool
file://$PWD/analysis1
# dst 1 condorpool
s3://admin@uweopenstack/wflocalscratch19082014114904/khawar/pegasus/WordCountWF/run0013/analysis1
EOF

