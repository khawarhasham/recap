import importlib
import sys

__author__ = 'khawar'

def getPlugin(wms_plugin, app, db):
    try:
        s = "plugin." + wms_plugin
#        print 'factory loading %s' % s
#        print sys.path

        wrapper_mod = importlib.import_module("plugin." + wms_plugin)
        #print wrapper_mod
#        for i in dir(wrapper_mod):
#            print i

        plugin_class = getattr(wrapper_mod, wms_plugin)
        plugin_obj = plugin_class(app, db)
        app.logger.debug("Plugin loadded")
        return plugin_obj
    except:
        print sys.exc_info()
        raise Exception("Plugin %s not found" % wms_plugin)


if __name__=='__main__':
    getPlugin('pegasus', { }, { })
