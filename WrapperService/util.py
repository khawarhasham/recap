from subprocess import Popen, PIPE
import shlex
import csv

__author__ = 'khawar'

def monitor_job(condor_id):
    CMd="condor_q -run %s" % condor_id
    args = shlex.split(CMd)
    sub_proc = Popen(args, shell=False, stdout=PIPE)
    sub_proc.stdout.readline()
    lines = sub_proc.stdout.readlines()
    print lines[2:]
    return lines[2:]

def monitor_all_jobs():
    CMd="condor_q -run"
    args = shlex.split(CMd)
    sub_proc = Popen(args, shell=False, stdout=PIPE)
    sub_proc.stdout.readline()
    lines = sub_proc.stdout.readlines()
    print lines[2:]
    return lines[2:]

def monitor_pool():
    CMd="condor_status -server"
    args = shlex.split(CMd)
    sub_proc = Popen(args, shell=False, stdout=PIPE)
    sub_proc.stdout.readline()
    lines = sub_proc.stdout.readlines()
    #print lines[:]
    #return lines[2:]
    return lines

#print monitor_pool()
