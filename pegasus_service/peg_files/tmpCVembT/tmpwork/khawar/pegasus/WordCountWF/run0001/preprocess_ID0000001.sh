#!/bin/bash
set -e
pegasus_lite_version_major="4"
pegasus_lite_version_minor="2"
pegasus_lite_version_patch="0"

. pegasus-lite-common.sh

pegasus_lite_init

# cleanup in case of failures
trap pegasus_lite_exit INT TERM EXIT

# work dir
pegasus_lite_setup_work_dir

# figure out the worker package to use
pegasus_lite_worker_package

# stage in 
pegasus-transfer 1>&2 << EOF
# src 1 condorpool
s3://admin@uweopenstack/wflocalscratch29112014225508/khawar/pegasus/WordCountWF/run0001/wordcount-preprocess-1.0
# dst 1 condorpool
file://$PWD/wordcount-preprocess-1.0
# src 2 condorpool
s3://admin@uweopenstack/wflocalscratch29112014225508/khawar/pegasus/WordCountWF/run0001/wordlist
# dst 2 condorpool
file://$PWD/wordlist
EOF

# set the xbit for any executables staged
/bin/chmod +x wordcount-preprocess-1.0 

# execute the tasks
pegasus-kickstart -n wordcount::preprocess:1.0 -N ID0000001 -R condorpool -L WordCountWF -T 2014-11-29T22:55:09+00:00 ./wordcount-preprocess-1.0  -i  wordlist  -o  wordlist1   wordlist2 

# stage out
pegasus-transfer 1>&2 << EOF
# src 1 condorpool
file://$PWD/wordlist1
# dst 1 condorpool
s3://admin@uweopenstack/wflocalscratch29112014225508/khawar/pegasus/WordCountWF/run0001/wordlist1
# src 2 condorpool
file://$PWD/wordlist2
# dst 2 condorpool
s3://admin@uweopenstack/wflocalscratch29112014225508/khawar/pegasus/WordCountWF/run0001/wordlist2
EOF

