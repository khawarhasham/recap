#!/bin/bash
set -e
pegasus_lite_version_major="4"
pegasus_lite_version_minor="2"
pegasus_lite_version_patch="0"

. pegasus-lite-common.sh

pegasus_lite_init

# cleanup in case of failures
trap pegasus_lite_exit INT TERM EXIT

# work dir
pegasus_lite_setup_work_dir

# figure out the worker package to use
pegasus_lite_worker_package

# stage in 
pegasus-transfer 1>&2 << EOF
# src 1 condorpool
s3://admin@uweopenstack/wflocalscratch29112014170929/khawar/pegasus/WordCountWF/run0001/analysis2
# dst 1 condorpool
file://$PWD/analysis2
# src 2 condorpool
s3://admin@uweopenstack/wflocalscratch29112014170929/khawar/pegasus/WordCountWF/run0001/analysis1
# dst 2 condorpool
file://$PWD/analysis1
EOF

# execute the tasks
pegasus-kickstart -n wordcount::merge:1.0 -N ID0000004 -R condorpool -L WordCountWF -T 2014-11-29T17:09:30+00:00 /opt/parallel_jobs/peg_jobs/wordcount/merge.py  -i  analysis1   analysis2  -o  merge_output 

# stage out
pegasus-transfer 1>&2 << EOF
# src 1 condorpool
file://$PWD/merge_output
# dst 1 condorpool
s3://admin@uweopenstack/wflocalscratch29112014170929/khawar/pegasus/WordCountWF/run0001/merge_output
EOF

