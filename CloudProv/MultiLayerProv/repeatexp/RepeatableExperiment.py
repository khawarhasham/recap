from __future__ import with_statement
import os
import shutil

from multilayer.cloud.CloudVirtLayer import CloudVirtLayer
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
import multilayer.util.Util as Util
from multilayer.wms.submitter import PegasusWMS


virt_layer = CloudVirtLayer()
flavors = virt_layer.get_flavors()

script='''#!/usr/bin/env bash
ls'''

key_path=os.path.expanduser('~/.ssh/id_rsa.pub')
with open(key_path) as fp:
     key = fp.read()

#workflow id to repeat
wf_id=114
dbconn = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
dbconn.get_connection()

''' retrieve all files used for submission
THERE could be an issue with wfSite because of output and scratch locations due to timestamp.
old bucket names are not old to be recreated by cloud api
'''
SQL="SELECT wfID, wfDAG, wfSite, wfProps from WorkflowSource where peg_wfid=%s" %wf_id
result = dbconn.execute_query({'sql': SQL})
wf_record = result.fetchone()
cloudprov_wfid = wf_record['wfID']
wfDAG = wf_record['wfDAG']
wfSite = wf_record['wfSite']
wfProps = wf_record['wfProps']

#store these files to execution directory
PEGASUS_EXEC_DIR = Defaults.config['pegasus_execution']['exec_dir']
repeat_dir = "%s/Repeat_%s" % (PEGASUS_EXEC_DIR, wf_id)
try:
    os.mkdir(repeat_dir)
    f = open("%s/workflow.dax"%repeat_dir, "w")
    f.write(wfDAG)
    f.close()

    f = open("%s/sites.xml"%repeat_dir, "w")
    f.write(wfSite)
    f.close()
    #copy props to new directory
    shutil.copyfile("%s/pegasusrc"%PEGASUS_EXEC_DIR, "%s/"%repeat_dir)
    shutil.copyfile("%s/tc.data"%PEGASUS_EXEC_DIR, "%s/"%repeat_dir)
except:
    print 'unable to create %s' % repeat_dir


''' get list of vms to recreate similar environment on cloud '''
SQL="select * from WfCloudMapping where wfid=%s" % cloudprov_wfid
result = dbconn.execute_query({'sql': SQL})
hosts = result.fetchall()
dbconn.close_connection()
#check for unique names
hostnames = []
for host_info in hosts:
    nodename = host_info['nodename']
    nodename_parts = nodename.split(".")
    #print nodename_parts[0]
    flavorId = host_info['flavorid']
    flavor = Util.get_flavor(flavorId, flavors)
    image_name = host_info['image_name']
    print 'use %s with %s but testing uses wf_peg_repeat' % (flavor.name, image_name)
    if nodename not in hostnames:
        hostnames.append(nodename)
        virt_layer.deploy_node(nodename_parts[0]+"_rep", key, script, 'wf_peg_repeat', flavor.name)
    else:
        print 'This hostname %s is already provisioned' % nodename


#virt_layer.deploy_node("mynode", key, script, 'wf_peg_repeat', 'm1.small')
#virt_layer.deploy_node("mynode", key, script, 'uwe-vm-snapshot', 'm1.small')

#once we have the nodes. now submit workflow
PegasusWMS.submit_workflow("workflow", exec_dir=Defaults.config['pegasus_execution']['exec_dir'])

''' establish mapping to repeatWF table with time
    origWfID is the wfid in cloudprov db retrived against peg_wfid
    repeatWfID is the newly given ID from pegasus
    ASSUMES: this assumes that there is no change in anything what so ever
'''