import ConfigParser
import os

__author__ = 'khawar'

def get_config():
    pwd = os.getcwd()
    if ( pwd.rfind("/multilayer") > 0):
        loc = pwd.rfind("/multilayer")
        #pwd = pwd[0: loc + 11]
        pwd = pwd[0:loc]+"/conf"
        #print pwd
    elif ( pwd.endswith("MultiLayerProv")):
        #pwd = pwd+"/multilayer"
        pwd = pwd+"/conf"
    else:
        loc = pwd.find("MultiLayerProv/")
        #pwd = pwd[0:loc + 15] +"multilayer"
        pwd = pwd[0:loc + 15] +"conf"
    print 'loading properties from dir %s' % pwd
    config = ConfigParser.ConfigParser()
    config.readfp(open(pwd+"/multilayer2.conf"))
    #print config
    multiconfig = {}
    for section in config.sections():
        multiconfig[section]={}
        for name, value in config.items(section):
            multiconfig[section][name] = value
    return multiconfig

if __name__=='__main__':
    conf = get_config()
    print conf['db_settings']