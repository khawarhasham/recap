import argparse
import sys

__author__ = 'khawar'

#-a analyse -T60 -i wordlist1 -o analysis1
def parse_args():
    parser = argparse.ArgumentParser(description='Process Program arguments.')
    parser.add_argument("-n", "--nodes", dest="nodes", type=int, #nargs="+",
                        help="Number of Nodes to create")
    parser.add_argument("-t", "--test", dest="test", #nargs="+",
                        help="""Test type. w,r,all,ic,fc,tc: w=Write,r=Read,all=app, ic=int comp,
                           fc=floating comp, tc=text like comp""")

    args = parser.parse_args()
    #print args
    return args

if __name__=='__main__':
    parse_args()