from libcloud.compute.types import NodeState
from pprint import pprint
import logging

LOG = logging.getLogger("provlog")

def get_flavor(flavorId, flavors):
    for flavor in flavors:
        if (flavorId == flavor.id):
            return flavor
    return None

def get_flavor_name (flavor_name, flavors):
    for flavor in flavors:
        if ( flavor_name == flavor.name ):
             return flavor
    return None
    
def show_flavor(flavor):
    print flavor
    LOG.debug( '{:>3} \t {:<12} {:<8}  {:<6}  {:>2}  {:>3}'.format(flavor.id, flavor.name,
    flavor.ram, flavor.disk, flavor.vcpus, flavor.price )  )

def show_node(node):
    print node
    print node.extra
    #print node.bandwidth
    LOG.debug('{:>37} \t {:<16}  {:<16} {:>6} {:>40} {:>6}'.format(node.id, node.name, node.private_ips[0],
        node.state==NodeState.RUNNING, node.extra['imageId'], node.extra['flavorId']) )
    LOG.debug (node.extra)

def get_node(node):
    return {'id': node.id, 'name':node.name, 'ip': node.private_ips[0],
            'img': node.extra['imageId'], 'flavorId': node.extra['flavorId']
            }
            
def show_nodes(nodes, flavors):
    line=['ID','Name','Private IP','Running', 'ImageID', 'FlavorID']
    LOG.debug( '{:>20} {:>30} {:>20} {:>12} {:>25} {:>20}'.format(*line) )
    for node in nodes:
        show_node(node)
        #pprint(driver.ex_get_node_details(node.id))
        found_flavor = get_flavor(node.extra['flavorId'], flavors)

        if ( found_flavor != None): 
            show_flavor(found_flavor)

def show_images(images):
    LOG.debug( '\n\n Available Images\n\n'  )
    #pprint(images)
    line=['ID','Name','minRAM','minDisk', 'status']
    LOG.debug( '{:>20} {:>30} {:>36} {:>12} {:>25}'.format(*line)  )

    for image in images:
        LOG.debug( '{:37} \t {:<40} {:>4} {:>12} {:>25}'.format(image.id, image.name,
              image.extra['minRam'], image.extra['minDisk'], image.extra['status']) )
        #look for image metadata if any

def show_flavors(flavors):
    print '\n\n Flavors \n\n'
    line=[' ID',' Name', ' RAM MB', ' Disk GB', ' vCPUs']
    print '{:>3} \t {:12} {:<8} {:>4} {:>2}'.format(*line)
    for flavor in flavors:
        show_flavor(flavor)
