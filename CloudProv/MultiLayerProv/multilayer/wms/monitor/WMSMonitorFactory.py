import importlib
import traceback
import sys

__author__ = 'khawar'

'''
dynamically load plugin
'''
def get_monitor(plugin_name):
    try:
       monitor_mod = importlib.import_module("multilayer.wms.monitor." + plugin_name)
       monitor_class = getattr(monitor_mod, plugin_name)
       monitor_obj = monitor_class()
       return monitor_obj
    except:
        print sys.exc_info()
        raise Exception("Plugin %s not found" % plugin_name)


if __name__=='__main__':
    obj = get_monitor("PegasusMonitor")
    obj.set_wfid("731074aa-0663-4242-a62d-381b167b99aa")
    obj.start()
    #my_import("multilayer.monitor.plugin.PegasusMonitor")