import threading
import logging
import time
import traceback
import tempfile
import sys
import os

from multilayer.parser.PegasusParser import PegasusParser
from multilayer.persistency import PersistencyFactory
from kwms.DAGParser import DAGParser

from multilayer import MultiLayerCloudProv
import multilayer.Defaults as Defaults

#to interact with Pegasus WMS service Wrapper for job running info
from multilayer.client.PegasusServiceClient import get_jobmon_detail, get_wms_files, get_cpool_mips

__author__ = 'khawar'

class PegasusMonitor(threading.Thread):

    def __init__(self):
        #super(PegasusMonitor, self).__init__()
        threading.Thread.__init__(self)
        self.wfuid = None
        self.wfid = None
        self.cloudprov_wfid = None #for cloudprov db
        self.pegasus_parser = PegasusParser()
        self.dbconn = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
        #self.condor_mips = get_cpool_mips()  #uncomment when the service is active
        self.targetdb = self.dbconn.get_connection()

        '''this is required to get job' inputs and outputs from the dax description.
        this helps in avoiding DB access to get this information.
        '''
        self.dagparser = None

        self.LOG = logging.getLogger("provlog")

    def set_cloudprovWFid(self, id):
        self.cloudprov_wfid = id
        #once we have this id, we can get DAX file from CloudProv db
        conn = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
        SQL="SELECT wfDAG from WorkflowSource where wfID=%s" % self.cloudprov_wfid
        result = conn.execute_query({'sql': SQL})
        record = result.fetchone()

        if ( record ):
            dag_data = record['wfDAG']
            self.dag_file = tempfile.NamedTemporaryFile(suffix='.xml', delete=False)
            self.dag_file.write(dag_data)
            self.dag_file.close()
            #instantiate the dagparser
            self.dagparser = DAGParser(self.dag_file.name)
            self.dagparser.get_jobs()
            print 'DagParser is initialized with %s' % self.dag_file.name
        #close this DB object
        conn.check_and_close()

    def set_wfid(self, wfuuid):
        self.wfuid = wfuuid
        self.wfid = None
        self.LOG.debug("WfID : %s" % self.wfuid)
        for i in xrange(3):
            try:
                self.LOG.debug ('wfid retrieval attempt %s' % i)
                self.wfid  = self.get_workflowid()
                if self.wfid is not None:
                    break
                self.LOG.debug('PegasusMonitor going to sleep' )
                time.sleep(15)
            except:
                #print sys.exc_info()
                self.LOG.error("Error - ", exc_info=True)

        if(not self.wfid):
            self.LOG.error( 'could not get wfid' )
            #raise exception and exist this monitoring

        self.LOG.debug( "wfid => %s" % self.wfid)

    def run(self):
        self.LOG.debug( 'Thread works on %s' % self.wfid)
        wfend_state = self.get_wfCurrentState('WORKFLOW_TERMINATED')
        print wfend_state
        #print wfend_state
        job_egdes = self.get_jobedge()
        self.LOG.debug('WF state %s' % wfend_state)
        while ( not self.__check_wf_terminated(wfend_state) ):
            try:
                #self.get_jobs()
                self.LOG.debug( 'GET JOB EXECUTION' )
                #update mips information
                #self.condor_mips = get_cpool_mips()

                self.get_jobexecution()

                #sleep for a few (5) seconds and repeat
                time.sleep(10)
                wfend_state = self.get_wfCurrentState('WORKFLOW_TERMINATED')
                self.LOG.debug('WF state %s' % wfend_state)
            except:
                traceback.print_exc(file=sys.stdout)
                time.sleep(5)
            finally:
                #self.dbconn.check_and_close()
                pass

        #close db connection
        self.dbconn.check_and_close()

        #if (True):
        #    return
        #update the provenance info in VirtualLayerProvenance
        '''depending upon the MAPPING_TYPE config, call static or dynamic mapping'''
        if ( Defaults.config['cloud_settings']['mapping_type']=='static'):
            MultiLayerCloudProv.establishJobResourceMapping(self.wfid, self.cloudprov_wfid)
        elif ( Defaults.config['cloud_settings']['mapping_type']=='eager'):
            MultiLayerCloudProv.establish_eager_mapping(self.wfid, self.cloudprov_wfid)
        elif ( Defaults.config['cloud_settings']['mapping_type']=='lazy'):
            MultiLayerCloudProv.establish_lazy_mapping(self.wfid, self.cloudprov_wfid)

        #now add cpu specs returend from job logs
        #cpu_map = self.process_cpu_specs()
        #MultiLayerCloudProv.establish_cpuspec_mapping(cpu_map)

        print " %s provenance is added " % self.wfid
        #there seems some repeatitions here
        job_file_outputs = self.pegasus_parser.get_jobdetails(self.wfid, self.dagparser)
        #comment file mapping for dummy wf
        #MultiLayerCloudProv.establish_jobfile_mapping(job_file_outputs, self.cloudprov_wfid)
        os.unlink(self.dag_file.name)

    def __check_wf_terminated(self, dbresult):
        #print dbresult
        if ( dbresult and dbresult[0]=='WORKFLOW_TERMINATED'):
            print "workflow is terminated"
            return True
        return False

    def get_workflowid(self):

        SQL="SELECT wf_id from workflow where wf_uuid='%s'" % self.wfuid
        #print SQL
        result = self.dbconn.execute_query({'sql':SQL})
        record = result.fetchone()
        print record
        if ( record ):
            return record[0]
        return None

    def get_jobs(self):
        SQL="SELECT job_id, exec_job_id, wf_id from job where wf_id in (select wf_id from workflow where wf_uuid='%s')" % self.wfuid
        jobs = self.dbconn.execute_query({'sql': SQL})
        for job in jobs:
            print job
            #self.wfid = job['wf_id']
            self.job_instances(job['job_id'])

    def job_instances(self, jobid):
        SQL="""SELECT job_instance_id, job_submit_seq FROM `job_instance`
               WHERE job_id=%s""" % jobid
        instances = self.dbconn.execute_query({'sql': SQL})
        for inst in instances:
            print inst
            self.job_state(inst['job_instance_id'])

    def job_state(self, job_insid):
        SQL="""SELECT state, jobstate_submit_seq, `timestamp`
               FROM jobstate WHERE job_instance_id=%s
               ORDER BY jobstate_submit_seq""" % job_insid
        states = self.dbconn.execute_query({'sql': SQL})
        for state in states:
            self.LOG.debug (state)

    def get_jobexecution(self):
        '''include job state as well in this query'''
        SQL="""SELECT job.exec_job_id,job_instance.*,workflow.submit_dir, workflow.dax_file
               FROM `job`,job_instance, workflow
               WHERE (job.job_id=job_instance.job_id) and (job.wf_id=workflow.wf_id)
               and job.wf_id=%s order by job_submit_seq
            """ % self.wfid

        instances = self.dbconn.execute_query({'sql': SQL})
        self.LOG.debug("jobs :%s " % instances.rowcount)

        for inst in instances:
            job_id = inst['job_id']

            self.LOG.debug(" Job process %s on host %s" % (job_id, inst['host_id']))

            ''' start the phase 1 for eager mapping if configured so'''
            if (Defaults.config['cloud_settings']['mapping_type']=='eager' ):
                condor_jobdetail = get_jobmon_detail(inst['sched_id'])
                host_name = self.process_condor_detail(condor_jobdetail)
                #if host_name is None (somehow) then do nothing
                if (host_name):
                    hostrecord = {'jobid': job_id,
                                  'wfID':self.cloudprov_wfid,
                                  'hostname':host_name,
                                  'condor_id': inst['sched_id'],
                                  'mon_time':time.time()
                                 }
                    print '\n\n Eager Temp Mapping start'
                    MultiLayerCloudProv.create_eager_temp_mapping(hostrecord, self.condor_mips)


            host = self.get_jobhost(inst['host_id'])
            print 'got host %s for job %s in wf %s \n' % (host, job_id, self.wfid)
            self.job_state(inst['job_instance_id'])
            output = inst['stdout_text']
            workdir = inst['submit_dir']
            if output!=None:
                #since swift api is down. so comment it. otherwise uncomment
                self.pegasus_parser.processOutputText(output, job_id)
                #pass


    def process_cpu_specs(self):
        '''include job state as well in this query'''
        SQL="""SELECT job.exec_job_id,job_instance.*,workflow.submit_dir, workflow.dax_file
               FROM `job`,job_instance, workflow
               WHERE (job.job_id=job_instance.job_id) and (job.wf_id=workflow.wf_id)
               and job.wf_id=%s order by job_submit_seq
            """ % self.wfid

        instances = self.dbconn.execute_query({'sql': SQL})
        self.LOG.debug("jobs :%s " % instances.rowcount)
        cpuspecs_map = []
        for inst in instances:
            job_id = inst['job_id']
            workdir = inst['submit_dir']
            #get CPU specs from job output and add it to the mapping
            cpu_specs = self.process_job_files(workdir, job_id, inst['job_instance_id'])
            d = {'wfID':self.wfid, 'jobid':job_id}
            if cpu_specs is not None:
                print cpu_specs
                cpu_hostname = cpu_specs.pop('hostname', None)
                #print cpu_hostname
                if ( cpu_hostname ):
                    cpu_specs['mips'] = self.condor_mips['%s'%cpu_hostname]

                d.update(cpu_specs)

            cpuspecs_map.append(d)

        print cpuspecs_map
        return cpuspecs_map

    def process_job_files(self, submit_dir, job_id, job_inst_id):
        files = get_wms_files(submit_dir, job_inst_id)
        if files['out'] is not None:
            #remove initial debug lines
            data = files['out']
            ind = data.find("if we")
            ind2 = data.find("\n", ind)
            xml_data = data[ind2+1:]
            #print xml_data

            f = tempfile.NamedTemporaryFile(suffix='.xml', delete=False)
            f.write(xml_data)
            f.close()

            cpu_detail = self.pegasus_parser.parse_machine_fromXML(f.name)
            os.unlink(f.name)

            return cpu_detail


    def get_jobhost(self, host_id):
        if ( host_id == None): return None

        SQL="SELECT * from host where host_id=%s and wf_id=%s" % (host_id, self.wfid)
        result = self.dbconn.execute_query({'sql': SQL})
        host = result.fetchone()
        self.LOG.debug(host)
        return host

    def get_wfstate(self):
        SQL="""SELECT state, `timestamp`, status
               FROM workflowstate where wf_id=%s""" % self.wfid
        result = self.dbconn.execute_query({'sql': SQL})
        wfstates = result.fetchall()
        return wfstates

    def get_wfCurrentState(self, state):
        SQL="""SELECT state, `timestamp`, status
               FROM workflowstate where wf_id=%s and state='%s'""" % (self.wfid, state)
        result = self.dbconn.execute_query({'sql': SQL})
        wfstate = result.fetchone()
        return wfstate

    def get_jobedge(self):
        SQL="""SELECT parent_exec_job_id as parent_id, child_exec_job_id as child_id
               FROM `job_edge`
               WHERE wf_id=%s and child_exec_job_id in (
               SELECT parent_exec_job_id FROM job_edge WHERE wf_id=%s) """ % (self.wfid, self.wfid)

        result = self.dbconn.execute_query({'sql': SQL})
        jobedges = result.fetchall()
        #for edge in jobedges:
        #    print edge
        return jobedges

    def show(self):
        print 'working'

    def process_condor_detail(self, jobdetail):
        print jobdetail
        if ( len(jobdetail)>1):
            jobline = jobdetail[1]
            job_columns = jobline.split()
            host_name = job_columns[-1]
            return host_name
        return None

if __name__ == '__main__':
    monitor = PegasusMonitor()
    monitor.wfid=556              #montage 533, wordcount 330
    monitor.set_cloudprovWFid(89) #montage 78, wordcount 21
    #monitor.process_cpu_specs()
    #monitor.set_wfid("912b73b8-8b6b-4e29-a46e-861cfa10eab2")
    #monitor.setDaemon(True)
    monitor.start()
