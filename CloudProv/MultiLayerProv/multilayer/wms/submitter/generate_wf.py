from Pegasus.DAX3 import ADAG, File, Link, Job, PFN, Executable
from Pegasus.DAX3 import *

# Create ADAG object
diamond = ADAG("WordCountWF")
site="condorpool"
# Add input file to the DAX-level replica catalog
a = File("wordlist")
a.addPFN(PFN("file:///opt/parallel_jobs/wordfile","local"))
diamond.addFile(a)

# Add executables to the DAX-level replica catalog
e_preprocess = Executable(namespace="wordcount", name="preprocess", version="1.0", os="linux", arch="x86_64", installed="true")
e_preprocess.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py", site))
diamond.addExecutable(e_preprocess)

analyse = Executable(namespace="wordcount", name="analyse", version="1.0", os="linux", arch="x86_64", installed="true")
analyse.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/wordcount/analyse.py", site))
diamond.addExecutable(analyse)

merge = Executable(namespace="wordcount", name="merge", version="1.0", os="linux", arch="x86_64", installed="true")
merge.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/wordcount/merge.py", site))
diamond.addExecutable(merge)

# Add a preprocess job
preprocess = Job(e_preprocess)
b1 = File("wordlist1")
b2 = File("wordlist2")
preprocess.addArguments("-i",a,"-o",b1,b2)
preprocess.uses(a, link=Link.INPUT, transfer=True, register=True)
preprocess.uses(b1, link=Link.OUTPUT, transfer=True, register=True)
preprocess.uses(b2, link=Link.OUTPUT, transfer=True, register=True)
diamond.addJob(preprocess)

# Add left analyse job
frl = Job(analyse)
c1 = File("analysis1")
frl.addArguments("-i",b1,"-o",c1)
frl.uses(b1, link=Link.INPUT, transfer=True, register=True)
frl.uses(c1, link=Link.OUTPUT, transfer=True, register=True)
diamond.addJob(frl)

# Add right analyse job
frr = Job(analyse)
c2 = File("analysis2")
frr.addArguments("-i",b2,"-o",c2)
frr.uses(b2, link=Link.INPUT, transfer=True, register=True)
frr.uses(c2, link=Link.OUTPUT, transfer=True, register=True)
diamond.addJob(frr)

# Add Analyze job
analyze = Job(merge)
d = File("merge_output")
analyze.addArguments("-i",c1,c2,"-o",d)
analyze.uses(c1, link=Link.INPUT, transfer=True, register=True)
analyze.uses(c2, link=Link.INPUT, transfer=True, register=True)
analyze.uses(d, link=Link.OUTPUT, transfer=True, register=True)
analyze.invoke(When.AT_END, '/home/khawar/phdcode/pegasus_notifier')
diamond.addJob(analyze)

# Add control-flow dependencies
diamond.depends(parent=preprocess, child=frl)
diamond.depends(parent=preprocess, child=frr)
diamond.depends(parent=frl, child=analyze)
diamond.depends(parent=frr, child=analyze)

# Write the DAX to stdoutdd notification for workflow
diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
diamond.invoke(When.ON_SUCCESS, '/usr/bin/mail -s "publish result" khawarhasham@gmail.com')

# Write the DAX to stdout
#import sys
#diamond.writeXML(sys.stdout)
 
# Write the DAX to a file
f = open("wordcount.dax","w")
diamond.writeXML(f)
f.close()
