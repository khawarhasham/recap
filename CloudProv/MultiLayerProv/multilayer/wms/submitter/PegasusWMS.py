from subprocess import Popen, PIPE, STDOUT
import subprocess
import re
from multilayer.wms.monitor import WMSMonitorFactory
from multilayer import Defaults
from multilayer.parser.PegasusParser import PegasusParser
from multilayer.wms.submitter.WMS import WMS

__author__ = 'khawar'

'''
needs following
1- .dax file
2- site.xml
3- tc.data
4- pegasusrc
5- submit file (as it has planner arguments)
'''

class PegasusWMS(WMS):

    def __init__(self):
        super(PegasusWMS, self).__init__()

    def submit_workflow(self, name, **kwargs):
        exec_dir = kwargs['exec_dir']
        return self.call_command(exec_dir, name)

    def call_command(self, wf_folder, wfname):
        arg_str = "pegasus-plan --conf pegasusrc --sites condorpool --dir tmpwork --output-site s3 --dax %s --submit" % wfname
        #arg_str = "./submit %s" % wfname
        args = arg_str.split(" ")
        print arg_str
        #curr_path = os.getcwd()
        #process = subprocess.Popen(args, stdout=subprocess.PIPE, cwd="%s/%s" % (curr_path, PEGASUS_DIR) )
        process = subprocess.Popen(args, stdout=subprocess.PIPE, cwd=wf_folder  )
        out, err = process.communicate()
        if ( err == None ):
            print 'process the output to get wfuuid'
            pattern = r"(.+)\s+(\w+:)\s+ pegasus-status -l (.+)"
            m = re.search(pattern, out)
            if ( m ):
                wf_work_dir = m.group(3)
                wf_work_dir = wf_work_dir.strip()

                brainfile = "%s/braindump.txt" % wf_work_dir
                f = open (brainfile, "r")
                brain_data = f.read()
                f.close()
                return brain_data
            else:
                return "out:%s <BR/> err:%s" % (out, err)

    def submit(self, name, **kwargs):
        print ("%s" % kwargs)
        exec_dir = kwargs['exec_dir']
        #out = subprocess.check_output(['/bin/sh', "+x",
        #                               "submit"])
        cmd1 = "cd %s" % exec_dir
        cmd2 = "sh +x submit"
        final = Popen("{}; {}".format(cmd1, cmd2), shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        out, nothing = final.communicate()

        peg_parser = PegasusParser()
        output = peg_parser.parse_submit_output(out)
        print output
        if ( output != None ):
            work_dir = output[2].strip()
            brain_dump = work_dir+"/braindump.txt"
            braindump_props = peg_parser.parse_braindump(brain_dump)
            monitor = WMSMonitorFactory.get_monitor("PegasusMonitor")
            monitor.set_wfid(braindump_props['wf_uuid'])
            monitor.start()

if __name__ == '__main__':
    pegwms = PegasusWMS()
    pegwms.submit("wordcount", exec_dir=Defaults.config['pegasus_execution']['exec_dir'])