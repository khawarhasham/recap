import requests
from requests.auth import HTTPBasicAuth
from multilayer.parser.PegasusParser import PegasusParser
from multilayer.wms.monitor.WMSMonitorFactory import get_monitor
from PegasusSiteGenerator import generate_site, local_site
import tempfile
import time
import os

__author__ = 'khawar'

SERVICE_URL = 'http://robb.cccs.uwe.ac.uk:3434/pegasus_service/api/v1.0'
auth_user = 'khawar'
auth_passwd = 'oxy123'
K_AUTH = HTTPBasicAuth(auth_user, auth_passwd)

def submit_dummywf(filename):
    '''for host monitoring'''
    site_xml = local_site()
    site_file = tempfile.NamedTemporaryFile( delete=False)
    site_file.write( site_xml)
    site_file.close( )
    files = {'dax': open( filename, 'r'), 'site': open( site_file.name, 'rb')}
    res = requests.post("%s/submit2" % SERVICE_URL, files=files, auth=K_AUTH)
    os.unlink(site_file.name)
    # process the response
    if ( res.status_code == 200):
        # check_file(res._content)
        json_obj = res.json()
        print json_obj

    else:
        print res._content

def submit_dummy_overhead(filename):
    '''donot perform any CAP capturing to test performance overhead'''
    site_xml = generate_site()
    site_file = tempfile.NamedTemporaryFile( delete=False)
    site_file.write( site_xml)
    site_file.close( )

    files = {'dax': open( filename, 'r'), 'site': open( site_file.name, 'rb')}
    res = requests.post("%s/submit2" % SERVICE_URL, files=files, auth=K_AUTH)

    # process the response
    if ( res.status_code == 200):
        # check_file(res._content)
        json_obj = res.json()
        print json_obj

    else:
        print res._content

def submit_dynamicWF(filename):
    site_xml = generate_site()
    site_file = tempfile.NamedTemporaryFile( delete=False)
    site_file.write( site_xml)
    site_file.close( )
    # print os.path.abspath(site_file.name)

    files = {'dax': open( filename, 'r'), 'site': open( site_file.name, 'rb')}
    res = requests.post("%s/submit2" % SERVICE_URL, files=files, auth=K_AUTH)

    # remove temp file
    os.unlink(site_file.name)

    # process the response
    if ( res.status_code == 200):
        # check_file(res._content)
        json_obj = res.json()
        print json_obj

    else:
        print res._content


def submit_workflow(filename):
    site_xml = generate_site()
    site_file = tempfile.NamedTemporaryFile(delete=False)
    site_file.write( site_xml)
    site_file.close( )
    # print os.path.abspath(site_file.name)

    files = {'dax': open( filename, 'r'), 'site': open( site_file.name, 'rb')}
    res = requests.post("%s/submit" % SERVICE_URL, files = files, auth = K_AUTH)

    # remove temp file
    os.unlink(site_file.name)

    # process the response
    if ( res.status_code == 200):
        # check_file(res._content)
        #TODO: once workflow is successfully submitted. we can insert WF files in CloudProv db here!
        json_obj = res.json()
        print json_obj
        parser = PegasusParser()
        # props = parser.parse_braindump_txt(res._content)
        props = parser.parse_braindump_txt(json_obj['output'])
        monitor = get_monitor("PegasusMonitor")
        monitor.set_wfid(props['wf_uuid'])
        monitor.set_cloudprovWFid(json_obj['wfid'])
        monitor.start()
        try:
            time.sleep(4)
        except:
            pass

        # update db record with pegasus wfid and cloudprov wfid
        update_cloudprov(monitor.wfid, json_obj['wfid'])
    else:
        print res._content

def update_cloudprov(pegwfid, wfid):
    params = {'wfid': wfid, 'pegwfid': pegwfid}
    res = requests.post("%s/update_wf" % SERVICE_URL, params = params, auth=K_AUTH)
    print res._content

def test_hello():
    res = requests.get( "%s/wfstatus" % SERVICE_URL, auth = K_AUTH)
    if ( res.status_code == 200):
        print res._content
    else:
        res.json
        res._content

def check_file(file_url):
    print file_url
    res = requests.get("http://robb.cccs.uwe.ac.uk:5000%s"%(file_url))
    print res.url
    if ( res.status_code == 200):
        print res._content
    else:
        print res._content

def site_map():
    res = requests.get("%s/site-map" % SERVICE_URL)
    print res.url
    if ( res.status_code == 200):
        print res._content

def get_jobmon_detail(condor_id):
    res = requests.get("%s/jobmon/%s" % (SERVICE_URL, condor_id))
    if ( res.status_code == 200 ):
        #print res._content
        job_info = res.json()
        return job_info[ "%s" % condor_id ]

def get_wms_files(submit_dir, jobinst_id):
    params = {'submit_dir': submit_dir, 'jobinst_id': jobinst_id}
    res = requests.post("%s/wms_get_file" % SERVICE_URL, params=params, auth=K_AUTH)
    if res.status_code == 200:
        files = res.json()
        #print files['out']
        return files

def get_cpool_mips():
    res = requests.get("%s/cpool_mips" % SERVICE_URL, auth=K_AUTH)
    if ( res.status_code == 200 ):
        #print res._content
        job_info = res.json()
        #print job_info["cpool"]
        return process_mips (job_info["cpool"])
        #return  job_info

def process_mips(cpool_mips):
    vm_mips = {}
    for line in cpool_mips[1:]:
        if ( line.find("Machines Avail") >0):
            break
        #print line
        parts = line.split()
        hostname = parts[0]
        hostname = hostname[:hostname.rfind(".")]
        vm_mips[hostname] = int(parts[6])

    print vm_mips
    return vm_mips
if __name__ == '__main__':
    test_hello()
    #get_cpool_mips()
    #get_wms_files('/home/khawar/WrapperService/peg_files/tmpWKzkia/tmpwork/khawar/pegasus/DynamicReqWF/run0001', 2818)
    #site_map()
    '''dummywordcount wf'''
    #submit_dummy_overhead("/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/client/dummywordcount.dax")

    #submit_dummy_overhead("/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/client/dummywordcountO.dax")

    '''wordcount wf'''
    #submit_workflow("/Users/khawar/Documents/CloudProv/run0013/wordcount.dax")
    submit_dummywf("/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/client/dummywf.dax")
    #submit_workflow("/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/client/dummywordcount.dax")
    #submit_workflow("/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/client/dummywordcount_var1.dax")

    '''dynamic req test wf'''
    #submit_dynamicWF("/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/client/dummywf.dax")

    #check_file("/pegasus_service/api/v1.0/uploads/wordcount.dax")
    #update_cloudprov(1,100)
    '''
    jobdetail = get_jobmon_detail(186.0)
    print jobdetail
    jobline = jobdetail[1]
    print jobline
    job_columns = jobline.split()
    print job_columns[-1]
    '''