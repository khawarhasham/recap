from Pegasus.DAX3 import ADAG, File, Link, Job, PFN, Executable
from Pegasus.DAX3 import *


def wordcount_dax():
    # Create ADAG object
    diamond = ADAG("WordCountWF")
    site="condorpool"
    # Add input file to the DAX-level replica catalog
    a = File("wordlist")
    a.addPFN(PFN("file:///opt/parallel_jobs/wordfile","local"))
    diamond.addFile(a)

    # Add executables to the DAX-level replica catalog
    e_preprocess = Executable(namespace="wordcount", name="preprocess", version="1.0", os="linux", arch="x86_64", installed="true")
    e_preprocess.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py", site))
    diamond.addExecutable(e_preprocess)

    analyse = Executable(namespace="wordcount", name="analyse", version="1.0", os="linux", arch="x86_64", installed="true")
    analyse.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/wordcount/analyse.py", site))
    diamond.addExecutable(analyse)

    merge = Executable(namespace="wordcount", name="merge", version="1.0", os="linux", arch="x86_64", installed="true")
    merge.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/wordcount/merge.py", site))
    diamond.addExecutable(merge)

    # Add a preprocess job
    preprocess = Job(e_preprocess)
    b1 = File("wordlist1")
    b2 = File("wordlist2")
    preprocess.addArguments("-i",a,"-o",b1,b2)
    preprocess.uses(a, link=Link.INPUT, transfer=True, register=True)
    preprocess.uses(b1, link=Link.OUTPUT, transfer=True, register=True)
    preprocess.uses(b2, link=Link.OUTPUT, transfer=True, register=True)
    diamond.addJob(preprocess)

    # Add left analyse job
    frl = Job(analyse)
    c1 = File("analysis1")
    frl.addArguments("-i",b1,"-o",c1)
    frl.uses(b1, link=Link.INPUT, transfer=True, register=True)
    frl.uses(c1, link=Link.OUTPUT, transfer=True, register=True)
    diamond.addJob(frl)

    # Add right analyse job
    frr = Job(analyse)
    c2 = File("analysis2")
    frr.addArguments("-i",b2,"-o",c2)
    frr.uses(b2, link=Link.INPUT, transfer=True, register=True)
    frr.uses(c2, link=Link.OUTPUT, transfer=True, register=True)
    diamond.addJob(frr)

    # Add Analyze job
    analyze = Job(merge)
    d = File("merge_output")
    analyze.addArguments("-i",c1,c2,"-o",d)
    analyze.uses(c1, link=Link.INPUT, transfer=True, register=True)
    analyze.uses(c2, link=Link.INPUT, transfer=True, register=True)
    analyze.uses(d, link=Link.OUTPUT, transfer=True, register=True)
    analyze.invoke(When.AT_END, '/home/khawar/phdcode/pegasus_notifier')
    diamond.addJob(analyze)

    # Add control-flow dependencies
    diamond.depends(parent=preprocess, child=frl)
    diamond.depends(parent=preprocess, child=frr)
    diamond.depends(parent=frl, child=analyze)
    diamond.depends(parent=frr, child=analyze)

    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
    diamond.invoke(When.ON_SUCCESS, '/usr/bin/mail -s "publish result" khawarhasham@gmail.com')

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "wordcount.dax")

def dummy_wordcount_dax():
    # Create ADAG object
    diamond = ADAG("DummyWordCountWF")
    site="condorpool"

    # Add executables to the DAX-level replica catalog
    e_preprocess = Executable(namespace="dummywordcount", name="dummypre", version="1.0", os="linux", arch="x86_64", installed="true")
    e_preprocess.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummypre.py", site))
    diamond.addExecutable(e_preprocess)

    analyse = Executable(namespace="dummywordcount", name="dummyanalyse", version="1.0", os="linux", arch="x86_64", installed="true")
    analyse.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyse.py", site))
    diamond.addExecutable(analyse)

    merge = Executable(namespace="dummywordcount", name="dummymerge", version="1.0", os="linux", arch="x86_64", installed="true")
    merge.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummymerge.py", site))
    diamond.addExecutable(merge)

    # Add a preprocess job
    preprocess = Job(e_preprocess)
    preprocess.addArguments("2")
    diamond.addJob(preprocess)

    # Add left analyse job
    frl = Job(analyse)
    frl.addArguments("1")
    diamond.addJob(frl)

    # Add right analyse job
    frr = Job(analyse)
    frr.addArguments("2")
    diamond.addJob(frr)

    # Add Analyze job
    analyze = Job(merge)
    analyze.addArguments("1")
    diamond.addJob(analyze)

    # Add control-flow dependencies
    diamond.depends(parent=preprocess, child=frl)
    diamond.depends(parent=preprocess, child=frr)
    diamond.depends(parent=frl, child=analyze)
    diamond.depends(parent=frr, child=analyze)

    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
    diamond.invoke(When.START, '/usr/bin/mail -s "workflow start" khawarhasham@gmail.com')

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "dummywordcount.dax")

'''to test performace overhead in the presence of process monitori.. Other mapping '''
def dummyO_wordcount_dax():
    # Create ADAG object
    diamond = ADAG("DummyWordCountWF")
    site="condorpool"

    # Add executables to the DAX-level replica catalog
    e_preprocess = Executable(namespace="dummywordcount", name="dummypreO", version="1.0", os="linux", arch="x86_64", installed="true")
    e_preprocess.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummypreOTHER.py", site))
    diamond.addExecutable(e_preprocess)

    analyse = Executable(namespace="dummywordcount", name="dummyanalyseO", version="1.0", os="linux", arch="x86_64", installed="true")
    analyse.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyseOTHER.py", site))
    diamond.addExecutable(analyse)

    merge = Executable(namespace="dummywordcount", name="dummymergeO", version="1.0", os="linux", arch="x86_64", installed="true")
    merge.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummymergeOTHER.py", site))
    diamond.addExecutable(merge)

    # Add a preprocess job
    preprocess = Job(e_preprocess)
    preprocess.addArguments("2")
    diamond.addJob(preprocess)

    # Add left analyse job
    frl = Job(analyse)
    frl.addArguments("1")
    diamond.addJob(frl)

    # Add right analyse job
    frr = Job(analyse)
    frr.addArguments("2")
    diamond.addJob(frr)

    # Add Analyze job
    analyze = Job(merge)
    analyze.addArguments("1")
    diamond.addJob(analyze)

    # Add control-flow dependencies
    diamond.depends(parent=preprocess, child=frl)
    diamond.depends(parent=preprocess, child=frr)
    diamond.depends(parent=frl, child=analyze)
    diamond.depends(parent=frr, child=analyze)

    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
    diamond.invoke(When.START, '/usr/bin/mail -s "workflow start" khawarhasham@gmail.com')

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "dummywordcountO.dax")


'''to test performace overhead in the presence of process monitori.. Other mapping '''
def wordcount_variation_dax():
    # Create ADAG object
    diamond = ADAG("wordcountvar")
    site="condorpool"

    # Add executables to the DAX-level replica catalog
    A = Executable(namespace="dummywordcount", name="A", version="1.0", os="linux", arch="x86_64", installed="true")
    A.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummypreOTHER.py", site))
    diamond.addExecutable(A)

    B = Executable(namespace="dummywordcount", name="B", version="1.0", os="linux", arch="x86_64", installed="true")
    B.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyseOTHER.py", site))
    diamond.addExecutable(B)

    C = Executable(namespace="dummywordcount", name="C", version="1.0", os="linux", arch="x86_64", installed="true")
    C.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyseOTHER.py", site))
    diamond.addExecutable(C)

    D = Executable(namespace="dummywordcount", name="D", version="1.0", os="linux", arch="x86_64", installed="true")
    D.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummymergeOTHER.py", site))
    diamond.addExecutable(D)

    # Add a preprocess job
    preprocess = Job(A)
    preprocess.addArguments("2")
    diamond.addJob(preprocess)

    # Add left analyse job
    frl = Job(B)
    frl.addArguments("1")
    diamond.addJob(frl)

    # Add right analyse job
    frr = Job(C)
    frr.addArguments("2")
    diamond.addJob(frr)

    # Add Analyze job
    analyze = Job(D)
    analyze.addArguments("1")
    diamond.addJob(analyze)

    # Add control-flow dependencies
    diamond.depends(parent=preprocess, child=frl)
    diamond.depends(parent=preprocess, child=frr)
    diamond.depends(parent=frl, child=analyze)
    diamond.depends(parent=frr, child=analyze)

    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
    diamond.invoke(When.START, '/usr/bin/mail -s "workflow start" khawarhasham@gmail.com')

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "dummywordcount_var.dax")


'''to test performace overhead in the presence of process monitori.. Other mapping '''
def wordcount_variation1_dax():
    # Create ADAG object
    diamond = ADAG("wordcountvar1")
    site="condorpool"

    # Add executables to the DAX-level replica catalog
    A = Executable(namespace="dummywordcount", name="A", version="1.0", os="linux", arch="x86_64", installed="true")
    A.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummypreOTHER.py", site))
    diamond.addExecutable(A)

    B = Executable(namespace="dummywordcount", name="B", version="1.0", os="linux", arch="x86_64", installed="true")
    B.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyseOTHER.py", site))
    diamond.addExecutable(B)

    C = Executable(namespace="dummywordcount", name="C", version="1.0", os="linux", arch="x86_64", installed="true")
    C.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyseOTHER.py", site))
    diamond.addExecutable(C)

    D = Executable(namespace="dummywordcount", name="D", version="1.0", os="linux", arch="x86_64", installed="true")
    D.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummymergeOTHER.py", site))
    diamond.addExecutable(D)

    # Add a preprocess job
    preprocess = Job(A)
    preprocess.addArguments("2")
    diamond.addJob(preprocess)

    # Add left analyse job
    frl = Job(B)
    frl.addArguments("1")
    diamond.addJob(frl)

    # Add right analyse job
    frr = Job(C)
    frr.addArguments("2")
    diamond.addJob(frr)

    # Add Analyze job
    analyze = Job(D)
    analyze.addArguments("1")
    diamond.addJob(analyze)

    # Add control-flow dependencies
    diamond.depends(parent=preprocess, child=frl)
    #diamond.depends(parent=preprocess, child=frr)
    diamond.depends(parent=frl, child=analyze)
    diamond.depends(parent=frr, child=analyze)

    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
    diamond.invoke(When.START, '/usr/bin/mail -s "workflow start" khawarhasham@gmail.com')

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "dummywordcount_var1.dax")

'''to test performace overhead in the presence of process monitori.. Other mapping '''
def wordcount_variation2_dax():
    # Create ADAG object
    diamond = ADAG("wordcountvar2")
    site="condorpool"

    # Add executables to the DAX-level replica catalog
    A = Executable(namespace="dummywordcount", name="A", version="1.0", os="linux", arch="x86_64", installed="true")
    A.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummypreOTHER.py", site))
    diamond.addExecutable(A)

    B = Executable(namespace="dummywordcount", name="B", version="1.0", os="linux", arch="x86_64", installed="true")
    B.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyseOTHER.py", site))
    diamond.addExecutable(B)

    X = Executable(namespace="dummywordcount", name="X", version="1.0", os="linux", arch="x86_64", installed="true")
    X.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummyanalyseOTHER.py", site))
    diamond.addExecutable(X)

    D = Executable(namespace="dummywordcount", name="D", version="1.0", os="linux", arch="x86_64", installed="true")
    D.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dummywf/dummymergeOTHER.py", site))
    diamond.addExecutable(D)

    # Add a preprocess job
    preprocess = Job(A)
    preprocess.addArguments("2")
    diamond.addJob(preprocess)

    # Add left analyse job
    frl = Job(B)
    frl.addArguments("1")
    diamond.addJob(frl)

    # Add right analyse job
    frr = Job(X)
    frr.addArguments("2")
    diamond.addJob(frr)

    # Add Analyze job
    analyze = Job(D)
    analyze.addArguments("1")
    diamond.addJob(analyze)

    # Add control-flow dependencies
    diamond.depends(parent=preprocess, child=frl)
    diamond.depends(parent=preprocess, child=frr)
    diamond.depends(parent=frl, child=analyze)
    diamond.depends(parent=frr, child=analyze)

    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
    diamond.invoke(When.START, '/usr/bin/mail -s "workflow start" khawarhasham@gmail.com')

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "dummywordcount_var2.dax")

def care_dax():
    # Create ADAG object
    wf = ADAG("WordCount-CareWF")
    site="local"
    # Add input file to the DAX-level replica catalog
    a = File("wordlist")
    a.addPFN(PFN("file:///opt/parallel_jobs/wordfile","local"))
    wf.addFile(a)

    # Add executables to the DAX-level replica catalog
    care_exe = Executable(namespace="wordcount-care", name="care", version="1.0", os="mac", arch="x86_64", installed="false")
    #care_exe.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py", site))
    wf.addExecutable(care_exe)

    # Add a preprocess job
    care_job = Job(care_exe)
    b1 = File("wordlist1")
    b2 = File("wordlist2")
    care_job.addArguments("-o","test.bin", "pre.py", "-i" , a , "-o" , b1, b2)
    care_job.uses(a, link=Link.INPUT, transfer=True, register=True)
    care_job.uses(b1, link=Link.OUTPUT, transfer=True, register=True)
    care_job.uses(b2, link=Link.OUTPUT, transfer=True, register=True)
    wf.addJob(care_job)


    # Write the DAX to stdout
    save_dax(wf, "care.dax")

def dummyjob_dax():
    # Create ADAG object
    diamond = ADAG("DynamicReqWF")
    site="condorpool"
    # Add input file to the DAX-level replica catalog
    a = File("wordlist")
    a.addPFN(PFN("file:///opt/parallel_jobs/wordfile","local"))
    diamond.addFile(a)

    # Add executables to the DAX-level replica catalog
    e_dummyjob = Executable(namespace="dynamicreqwf", name="dummyjob", version="1.0", os="linux", arch="x86_64", installed="true")
    e_dummyjob.addPFN(PFN("file:///opt/parallel_jobs/peg_jobs/dynamic_req/dummyjob.py", site))
    diamond.addExecutable(e_dummyjob)

    # Add a dummy job
    dummyjob = Job(e_dummyjob)
    b2 = File("dummyout")
    dummyjob.addArguments("2","-i", a, "-o", b2)
    dummyjob.uses(a, link=Link.INPUT, transfer=True, register=True)
    dummyjob.uses(b2, link=Link.OUTPUT, transfer=True, register=True)
    dummyjob.invoke(When.START, '/home/khawar/phdcode/dynamic_req_test/PegDynamicTest.py')
    dummyjob.invoke(When.AT_END, '/home/khawar/phdcode/dynamic_req_test/dummyjob_end')
    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.ON_SUCCESS, '/usr/bin/mail -s "publish successful dummywf result" khawarhasham@gmail.com')

    diamond.addJob(dummyjob)

    # Add control-flow dependencies

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "dummywf.dax")

def wordcount_OSDC_dax():
    # Create ADAG object
    diamond = ADAG("WordCountWF")
    site="condorpool"
    # Add input file to the DAX-level replica catalog
    a = File("wordlist")
    a.addPFN(PFN("file:///opt/work_area/pegasus_jobs/wordfile","local"))
    diamond.addFile(a)

    # Add executables to the DAX-level replica catalog
    e_preprocess = Executable(namespace="wordcount", name="preprocess", version="1.0", os="linux", arch="x86_64", installed="true")
    e_preprocess.addPFN(PFN("file:///opt/work_area/pegasus_jobs/wordcount/pre.py", site))
    diamond.addExecutable(e_preprocess)

    analyse = Executable(namespace="wordcount", name="analyse", version="1.0", os="linux", arch="x86_64", installed="true")
    analyse.addPFN(PFN("file:///opt/work_area/pegasus_jobs/wordcount/analyse.py", site))
    diamond.addExecutable(analyse)

    merge = Executable(namespace="wordcount", name="merge", version="1.0", os="linux", arch="x86_64", installed="true")
    merge.addPFN(PFN("file:///opt/work_area/pegasus_jobs/wordcount/merge.py", site))
    diamond.addExecutable(merge)

    # Add a preprocess job
    preprocess = Job(e_preprocess)
    b1 = File("wordlist1")
    b2 = File("wordlist2")
    preprocess.addArguments("-i",a,"-o",b1,b2)
    preprocess.uses(a, link=Link.INPUT, transfer=True, register=True)
    preprocess.uses(b1, link=Link.OUTPUT, transfer=True, register=True)
    preprocess.uses(b2, link=Link.OUTPUT, transfer=True, register=True)
    diamond.addJob(preprocess)

    # Add left analyse job
    frl = Job(analyse)
    c1 = File("analysis1")
    frl.addArguments("-i",b1,"-o",c1)
    frl.uses(b1, link=Link.INPUT, transfer=True, register=True)
    frl.uses(c1, link=Link.OUTPUT, transfer=True, register=True)
    diamond.addJob(frl)

    # Add right analyse job
    frr = Job(analyse)
    c2 = File("analysis2")
    frr.addArguments("-i",b2,"-o",c2)
    frr.uses(b2, link=Link.INPUT, transfer=True, register=True)
    frr.uses(c2, link=Link.OUTPUT, transfer=True, register=True)
    diamond.addJob(frr)

    # Add Analyze job
    analyze = Job(merge)
    d = File("merge_output")
    analyze.addArguments("-i",c1,c2,"-o",d)
    analyze.uses(c1, link=Link.INPUT, transfer=True, register=True)
    analyze.uses(c2, link=Link.INPUT, transfer=True, register=True)
    analyze.uses(d, link=Link.OUTPUT, transfer=True, register=True)
    analyze.invoke(When.AT_END, '/home/khawar/phdcode/pegasus_notifier')
    diamond.addJob(analyze)

    # Add control-flow dependencies
    diamond.depends(parent=preprocess, child=frl)
    diamond.depends(parent=preprocess, child=frr)
    diamond.depends(parent=frl, child=analyze)
    diamond.depends(parent=frr, child=analyze)

    # Write the DAX to stdoutdd notification for workflow
    diamond.invoke(When.AT_END, '/usr/bin/mail -s "Workflow finished" khawarhasham@gmail.com')
    diamond.invoke(When.ON_SUCCESS, '/usr/bin/mail -s "publish result" khawarhasham@gmail.com')

    # Write the DAX to stdout
    #import sys
    #diamond.writeXML(sys.stdout)
    save_dax(diamond, "osdc_wordcount.dax")


def save_dax(dax_obj, filename):
    # Write the DAX to a file
    f = open(filename,"w")
    dax_obj.writeXML(f)
    f.close()
    print 'done'

#wordcount_dax()
#care_dax()
#dummyjob_dax()
#wordcount_OSDC_dax()
#dummy_wordcount_dax()
dummyO_wordcount_dax()
wordcount_variation_dax()
wordcount_variation1_dax()
wordcount_variation2_dax()
'''SEE HOW CAN METADATA BE USED'''
