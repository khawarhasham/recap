import datetime
__author__ = 'khawar'

def generate_site():
    now = datetime.datetime.now()
    now_str = datetime.datetime.strftime(now, "%d%m%Y%H%M%S")
    print now_str
    site_xml = """<?xml version="1.0" encoding="UTF-8"?>
    <sitecatalog xmlns="http://pegasus.isi.edu/schema/sitecatalog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd" version="3.0">
    <site  handle="local" arch="x86" os="LINUX">
    <head-fs>
    <scratch>
    <shared>
    <file-server protocol="file" url="file://" mount-point="./work"/>
    <internal-mount-point mount-point="./work"/>
</shared>
</scratch>
<storage>
<shared>
<file-server protocol="file" url="file://" mount-point="./outputs"/>
<internal-mount-point mount-point="./outputs"/>
</shared>
<!--shared>
<file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wfoutput%s"/>
<internal-mount-point mount-point="./outputs" />
</shared -->
</storage>
</head-fs>
<profile namespace="env" key="S3CFG">/home/khawar/.s3cfg</profile>
<profile namespace="env" key="PEGASUS_HOME" >/home/khawar/pegasus-4.2.0</profile>
</site>
<site handle="s3" arch="x86_64" os="LINUX">
<head-fs>
<storage>
<shared>
<file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wfoutput%s"/>
<internal-mount-point mount-point="/wfoutput%s" />
</shared>
</storage>
</head-fs>
</site>
<site  handle="condorpool" arch="x86" os="LINUX">
<head-fs>
<scratch >
<shared>
<file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wflocalscratch%s"/>
<internal-mount-point mount-point="/wflocalscratch%s" />
</shared>
</scratch>
<storage>
<shared>
<file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wfshared%s"/>
<internal-mount-point mount-point="/wfshared%s" />
</shared>
</storage>
</head-fs>
<profile namespace="pegasus" key="style" >condor</profile>
<profile namespace="env" key="PEGASUS_HOME" >/opt/pegasus/default</profile>
<profile namespace="env" key="S3CFG">/opt/.s3cfg</profile>
<profile namespace="env" key="PYTHONPATH">/opt/parallel_jobs/peg_jobs/wordcount:/opt/parallel_jobs/peg_jobs/dummywf:/opt/parallel_jobs/peg_jobs/dynamic_req</profile>
<profile namespace="condor" key="universe">vanilla</profile>
<profile namespace="condor" key="requirements" >(Target.Arch == "X86_64")</profile>
</site>
</sitecatalog>""" %(now_str, now_str, now_str, now_str, now_str, now_str, now_str)
    return site_xml

def generate_site_intermediaryfiles():
    now = datetime.datetime.now()
    now_str = datetime.datetime.strftime(now, "%d%m%Y%H%M%S")
    print now_str
    site_xml = """<?xml version="1.0" encoding="UTF-8"?>
    <sitecatalog xmlns="http://pegasus.isi.edu/schema/sitecatalog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd" version="3.0">
    <site  handle="local" arch="x86" os="LINUX">
    <head-fs>
    <scratch>
    <shared>
    <file-server protocol="file" url="file://" mount-point="./work"/>
    <internal-mount-point mount-point="./work"/>
    </shared>
    </scratch>
    <storage>
    <shared>
    <file-server protocol="file" url="file://" mount-point="./outputs"/>
    <internal-mount-point mount-point="./outputs"/>
    </shared>
    <!--shared>
    <file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wfoutput%s"/>
    <internal-mount-point mount-point="./outputs" />
    </shared -->
    </storage>
    </head-fs>
    <profile namespace="env" key="S3CFG">/home/khawar/.s3cfg</profile>
    <profile namespace="env" key="PEGASUS_HOME" >/home/khawar/pegasus-4.2.0</profile>
    </site>
    <site handle="s3" arch="x86_64" os="LINUX">
    <head-fs>
    <storage>
    <shared>
    <file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wfoutput%s"/>
    <internal-mount-point mount-point="/wfoutput%s" />
    </shared>
    </storage>
    </head-fs>
    </site>
    <site  handle="condorpool" arch="x86" os="LINUX">
    <head-fs>
    <scratch >
    <shared>
    <file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wflocalscratch%s"/>
    <internal-mount-point mount-point="/wflocalscratch%s" />
    </shared>
    </scratch>
    <storage>
    <shared>
    <file-server protocol="s3" url="s3://admin@uweopenstack" mount-point="/wfshared%s"/>
    <internal-mount-point mount-point="/wfshared%s" />
    </shared>
    </storage>
    </head-fs>
    <profile namespace="pegasus" key="style" >condor</profile>
    <profile namespace="env" key="PEGASUS_HOME" >/opt/pegasus/default</profile>
    <profile namespace="env" key="S3CFG">/opt/.s3cfg</profile>
    <profile namespace="env" key="PYTHONPATH">/opt/parallel_jobs/peg_jobs/wordcount</profile>
    <profile namespace="condor" key="universe">vanilla</profile>
    <profile namespace="condor" key="requirements" >(Target.Arch == "X86_64")</profile>
    </site>
    </sitecatalog>""" %(now_str, now_str, now_str, now_str, now_str, now_str, now_str)
    return site_xml

def montage_local_site():
    return """<?xml version="1.0" encoding="UTF-8"?>
<sitecatalog xmlns="http://pegasus.isi.edu/schema/sitecatalog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd" version="3.0">
    <site handle="local" arch="x86" os="LINUX">
        <grid  type="gt2" contact="localhost/jobmanager-fork" scheduler="Fork" jobtype="auxillary"/>
        <head-fs>
            <scratch>
                <shared>
                    <file-server protocol="file" url="file://" mount-point="/glusterfs/users/mkhahmad/0.2/work/0.2/2015-09-17_120416/scratch"/>
                    <internal-mount-point mount-point="/glusterfs/users/mkhahmad/0.2/work/0.2/2015-09-17_120416/scratch"/>
                </shared>
            </scratch>
            <storage>
                <shared>
                    <file-server protocol="file" url="file://" mount-point="/glusterfs/users/mkhahmad/0.2/work/0.2/2015-09-17_120416/outputs"/>
                    <internal-mount-point mount-point="/glusterfs/users/mkhahmad/0.2/work/0.2/2015-09-17_120416/outputs"/>
                </shared>
            </storage>
        </head-fs>
        <replica-catalog  type="LRC" url="rlsn://dummyValue.url.edu" />
        <profile namespace="env" key="MONTAGE_HOME" >/opt/work_area/Montage_v3.3_patched_4</profile>
        <profile namespace="env" key="PEGASUS_HOME" >/opt/work_area/pegasus</profile>
        <profile namespace="env" key="GLOBUS_TCP_PORT_RANGE" >40000,50000</profile>
    </site>

    <site  handle="local-condor" arch="x86" os="LINUX">
        <head-fs>
            <scratch />
            <storage />
        </head-fs>

        <profile namespace="pegasus" key="style" >condor</profile>
        <profile namespace="condor" key="universe" >vanilla</profile>
        <profile namespace="env" key="PEGASUS_HOME" >/opt/work_area/pegasus</profile>
    </site>


</sitecatalog>"""

def local_site():
    return """<?xml version="1.0" encoding="UTF-8"?>
<sitecatalog xmlns="http://pegasus.isi.edu/schema/sitecatalog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-4.0.xsd" version="4.0">

    <site  handle="local" arch="x86" os="LINUX">
        <directory type="shared-scratch" path="/home/khawar/dynamichost/work">
            <file-server operation="all" url="file:///home/khawar/dynamichost/work"/>
        </directory>
        <directory type="local-storage" path="/home/khawar/dynamichost/outputs">
            <file-server operation="all" url="file:///home/khawar/dynamichost/outputs"/>
        </directory>
    </site>

    <site  handle="condorpool" arch="x86" os="LINUX">
        <profile namespace="pegasus" key="style" >condor</profile>
        <profile namespace="condor" key="universe" >vanilla</profile>
        <profile namespace="env" key="PEGASUS_HOME" >/opt/work_area/pegasus</profile>
    </site>

</sitecatalog>"""