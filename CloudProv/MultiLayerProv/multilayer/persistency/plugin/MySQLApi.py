import logging
from PersistentLayer import PersistentLayer
import traceback

lib_flag=0
try:
    import mysql.connector
    lib_flag = 1
except:
    try:
        import MySQLdb
        lib_flag = 2
    except:
        raise Exception("Could not load any mysql libraries")

__author__ = 'khawar'

if ( lib_flag == 1):
    class MySQLCursorDict(mysql.connector.cursor.MySQLCursor):
        def _row_to_python(self, rowdata, desc=None):
            row = super(MySQLCursorDict, self)._row_to_python(rowdata, desc)
            if row:
                return dict(zip(self.column_names, row))
            return None

class MySQLApi(PersistentLayer):

    def __init__(self, config):
        PersistentLayer.__init__(self, config)
        self.conn = None

    def get_connection(self):
        if ( lib_flag == 1):
            self.config = self.config.pop("dburl", None)
            self.conn = mysql.connector.connect(**self.config)
        elif ( lib_flag == 2):
            if ( not self.config.has_key("dburl")):
                dburl = "mysql://%s:%s@%s:3306/%s" % (self.config['user'], self.config['password'], \
                                                      self.config['host'], self.config['database'])
            else:
                dburl = self.config['dburl']

            self.conn = MySQLdb.connect ( dburl, self.config['user'], self.config['password'], self.config['database'] )

        logging.debug("connected ")
        return self.conn


    def close_connection(self):
        self.conn.close()
        logging.debug("db closed");

    def insert_record(self, record_data):
        db = record_data[0]
        table = record_data[1]
        insert_values = record_data[2]
        #db, table, insert_values
        sql="""insert into %s values(%)""" % (table, insert_values)
        return self.execute_query({'sql':sql})

    def execute_query(self, query_data):
        try:
            sql = query_data['sql']
            logging.debug(sql)
            cursor = self.get_cursor()
            total = cursor.execute(sql)
            if ( sql.startswith("SELECT") ):
                logging.debug("select sql")
                total = -1
            logging.debug("Query result : %s" % total)
            return {'total': total, 'cursor': cursor}
        except:
            logging.error("TRACE: %s" % traceback.format_exc() )

    def get_cursor(self):
        cursor = None
        if ( lib_flag == 1):
            cursor = self.conn.cursor(cursor_class=MySQLCursorDict)
        elif ( lib_flag == 2):
            cursor = self.conn.cursor(MySQLdb.cursors.DictCursor)
        return cursor
