
import logging

"""
interface for persistency layer
"""
__author__ = 'khawar'

class PersistentLayer(object):

    def __init__(self, config):
        #setup logging
        self.LOG = logging.getLogger("persistencylog")

        self.config = config
        self.LOG.debug("PersistentLayer init")

    def get_connection(self):
        pass

    def close_connection(self):
        pass

    def check_and_close(self):
        pass

    def insert_record(self, record_data):
        pass

    def execute_query(self, query_data):
        pass
