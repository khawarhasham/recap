from py2neo import Graph, Node, Relationship, rel
from random import randint
from PersistentLayer import PersistentLayer
import logging

class Neo4jAPI(PersistentLayer):
    
    def __init__(self, config):
        PersistentLayer.__init__(self, config)
        self._disable_logs()
        self.graph_db = Graph(config['dburi'])

    def _disable_logs(self):
        for logname in logging.Logger.manager.loggerDict.keys():
            if logname.find("py2neo")==0:
                logger = logging.getLogger(logname)
                logger.setLevel(logging.WARNING)

    def get_connection(self):
        if ( self.graph_db == None ):
            self.graph_db = Graph()
        return self.graph_db

    def close_connection(self):
        pass

    def clear(self):
        self.graph_db.delete_all()

    def insert_record(self, record_data):
        if ( record_data['c']=='node'):
            return self.createNode(*record_data['values'])
        elif ( record_data['c']=='rel'):
            return self.createRelation(*record_data['values'])

    def execute_query(self, cypher_sql):
        result = self.graph_db.cypher.execute(cypher_sql)
        return result

    def createNode(self, node_info, node_labels):
        '''node_props = {}
        for k,v in node_info.items():
            node_props[k]=v
        '''
        if ( isinstance(node_labels, list) ):
            node_labels = ",".join(node_labels)

        node = Node.cast(node_labels, node_info)
        '''{
                "name": node_info['name'],
                "ip": node_info['ip'],
                "vcpu": node_info['vcpu'],
                "ram": node_info['ram'],
                "disk": node_info['disk'],
                "img": node_info['img'],
                "flavor": node_info['flavorId'],
                }'''

        #node[0].add_labels("host")
        self.graph_db.create(node)

        return node

    def createRelation(self, node1, node2, relname, rel_props=None):
        #print relname
        #print rel_props

        r = Relationship.cast(node1, relname, node2, rel_props)
        self.graph_db.create(r)


    def createProvenanceTaskNode(self, node_info):
        node = self.graph_db.create(
                {
                "name": node_info['name'],
                "type": node_info['type'],
                "start": node_info['start'],
                "finish": node_info['finish'],
                "args": node_info['args']
                }
            )
        node[0].add_labels("task")
        return node
    
    def createProvenanceWorkerNode(self, node_info):
        node = self.graph_db.create(
                {
                "name": node_info['name'],
                "type": 'subtask',
                "start": node_info['start'],
                "finish": node_info['finish'],
                "args": node_info['args']
                }
            )
        node[0].add_labels("subtask")
        return node

    def executeOnRelation(self, task, host):
        task.create_path(("executeOn", {}), host)
        
    def taskrelation(self, task1, task2):
        task1.create_path(("has_task",{}), task2)  
          
    def createProvenanceGraph(self, wfid, hosts, host_details, processes):
        root = self.graph_db.create({
                 'name': wfid,
                 'tasks': 1,
                 'start': 1,
                 'finish': 2
                })
        root[0].add_labels("workflow")
        
        #add tasks to root node
        task = self.createProvenanceTaskNode({'name':'wordcount',
        'type':'mpj', 'start':1, 'finish':2, 'args':''})
        task[0].add_labels("task")
        self.taskrelation(root[0], task[0])
        
        proc = self.createProvenanceTaskNode({'name':'master',
                      'type':'mpj', 'nature':'master', 
                       'start': processes['0']['START'][1], 
                       'finish':processes['0']['FINISH'][1], 'args':''})
        self.taskrelation(task[0], proc[0])               
        for process, process_info in processes.items():
            host = self.createProvenanceHostNode(host_details[process_info['START'][0]])
            if ( process == '0' ):
                proc[0].add_labels("process")
                self.taskrelation(task[0], proc[0])       
                self.executeOnRelation(proc[0], host[0])
            else:
                subtask = self.createProvenanceTaskNode({'name':'worker%s'%process,
            'type':'mpj', 'nature':'worker%s'%process, 
            'start':process_info['START'][1], 
            'finish': process_info['FINISH'][1], 'args':''})
                subtask[0].add_labels("subprocess")
                self.taskrelation(proc[0], subtask[0])
                self.executeOnRelation(subtask[0], host[0])

        print 'provenance graph created'  

if __name__ == '__main__':
    neoApi = Neo4jAPI({'dburi':'http://localhost:7474/db/data'})
    graph_db = neoApi.get_connection()
    node1 = neoApi.insert_record({'c':'node', 'values':[{'name':'khawar','uid':1, 'inst':'uwe'}, 'uwestaff']})
    print node1
    node2 = neoApi.insert_record({'c':'node', 'values':[{'name':'hasham','uid':2, 'inst':'uwe'},'uwestud']})
    print node2
    reln = neoApi.insert_record({'c':'rel', 'values':[node1, node2, 'teach_to', {'since':2001}]})
    reln = neoApi.insert_record({'c':'rel', 'values':[node2, node1, 'taught_by', {'since':2001}]})
    print reln
    neoApi.close_connection()