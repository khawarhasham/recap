from sqlalchemy import *
from PersistentLayer import PersistentLayer

import json
import pprint
import logging
import time
import sys
import traceback

target_db = None

class SqlAlchemyApi(PersistentLayer):

    def __init__(self, config):
        PersistentLayer.__init__(self, config)
        self.conn = None
        self.engine = None
        if ( not self.config.has_key("dburl")):
            dburl = "mysql+mysqldb://%s:%s@%s:3306/%s" % (self.config['user'], self.config['password'], \
                                                          self.config['host'], self.config['database'])
        else:
            dburl = self.config['dburl']
        print dburl

        self.engine = create_engine( dburl ) #, echo=True)

    def get_connection(self):
        self.conn = self.engine.connect()
        self.LOG.debug("connected to %s" % self.config['database'])
        return self.conn
        #return self.get_mysql_connection(host, user, passwd, "pegasusdb")

    def get_mysql_connection(self, host, user, passwd, dbname, port=3306):
        conn_url = "mysql+mysqldb://%s:%s@%s:%s/%s" % \
                   (user, passwd, host, port, dbname)
        self.engine = create_engine(conn_url)#, echo=True)
        mysql_conn = self.engine.connect()
        self.LOG.debug("connected to %s" % dbname)
        return mysql_conn

    def check_and_close(self):
        print 'check and close %s' % self.conn
        if ( self.conn and not self.conn.closed ):
            self.LOG.debug( 'closing connection' )
            self.close_connection()

    def close_connection(self):
        self.conn.close()
        self.LOG.debug( "closing connection")

    def load_metadata(self,db):
        metadata = MetaData(db)
        metadata.reflect(bind=db)
        #for table in reversed(metadata.sorted_tables):
        #    print table
        return metadata

    def execute_query(self, query_data):
        return self.engine.execute(query_data['sql'])

    def insert_record(self, record_data):
        target_db = record_data['conn']
        table_name = record_data['table_name']
        insert_values = record_data['insert_values']
        target_db_metadata = self.load_metadata(target_db)
        trans = target_db.begin()
        try:
            '''then insert in given table'''
            tab_ins = target_db_metadata.tables[table_name].insert()
            self.LOG.debug (tab_ins)
            self.LOG.debug ( insert_values )
            ins_count = target_db.execute(tab_ins, insert_values)
            self.LOG.debug( "inserted records %s out of %s " % (ins_count.rowcount, len(insert_values)))
            trans.commit()
            return True
        except:
            trans.rollback()
            print 'Error in insertion %s' % sys.exc_info()[0]
            self.LOG.error('Error inserting %s ' % table_name)
            self.LOG.error('Error - ', exc_info=True)
            traceback.print_exc(file=sys.stdout)

        return False