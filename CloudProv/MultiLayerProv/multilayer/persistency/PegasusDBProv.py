import PersistencyFactory
import logging
import logging.config
import time
import datetime
import sys
import traceback
import multilayer.Defaults as Defaults

#import pprint
#from xml.dom.minidom import parseString
import os


LOG=logging.getLogger("persistencylog")
LOG.debug("Log init")


#dbconn = MySQLdb.connect ( dburl, dbuser, dbpassword, dbname )
mysqlobj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
mysqlobj.get_connection()

LOG.debug("connected to DB")

pegasus_prov_xml='<?xml version="1.0" ?>'

def test():
    SQL="SELECT * FROM workflow where wf_id=165"
    try:
        result = mysqlobj.execute_query({'sql':SQL})
        records = result.fetchone()
        if ( records ):
            print 'record exists'
        else:
            print 'no record found'

    except:
        print 'error found in test()'
        traceback.print_exc(file=sys.stdout)

    mysqlobj.close_connection()

def workflows_provenane():
    global pegasus_prov_xml
    SQL="SELECT wf_id from workflow"
    try:
        #total = cursor.execute(SQL)
        result = mysqlobj.execute_query({'sql': SQL})
        LOG.debug("Total Workflows found : %s" % result['total'])
        wf_ids = result['cursor'].fetchall()
        for wf_id in wf_ids:
            workflow_provenance(wf_id['wf_id'])
            pegasus_prov_xml='<?xml version="1.0" ?>'
    except:
        LOG.debug("ERROR: %s" % sys.exc_info()[0] )
        LOG.error("TRACE: %s" % traceback.format_exc() )
        
        
def workflow_provenance(workflow_id):
    global pegasus_prov_xml
    SQL="""SELECT wf_id, dag_file_name, user, submit_hostname, timestamp 
        from workflow
        where workflow.wf_id=%s
        """ % workflow_id
    try:
        #result_total = cursor.execute(SQL)
        result = mysqlobj.execute_query({'sql': SQL})
        workflows_result = result.fetchall()
        for row in workflows_result:
            #pprint.pprint( row )
            job_xml = job_provenance(workflow_id)
            submit_time = row['timestamp']
            submit_time = datetime.datetime.utcfromtimestamp(submit_time)
            wf_xml = """<Workflow id='%s' dag_file='%s' submit_host='%s' submit_date='%s'>
 <UserInfo> <name>%s</name> </UserInfo>
 %s
</Workflow>""" % (row['wf_id'], row['dag_file_name'], 
            row['submit_hostname'], submit_time, row['user'], job_xml)
            
            pegasus_prov_xml = "%sv %s" % (pegasus_prov_xml, wf_xml)
            
        print pegasus_prov_xml
        #parseString(pegasus_prov_xml).toprettyxml(indent=' '*2)
        #writeXML(pegasus_prov_xml,
        #         'wf_prov/pegasusProv_wf_%s.xml' % workflow_id)
    except:
        LOG.debug("ERROR: %s" % sys.exc_info()[0] )
        LOG.error("TRACE: %s" % traceback.format_exc() )

def job_provenance(workflow_id):
    """this function fetches job associated with the given workflow_id
       and retrieves their meta-data
    """   
    SQL="""SELECT job_id, exec_job_id, submit_file as job_file, type_desc, executable, argv, task_count 
        from workflow, job 
        where workflow.wf_id=job.wf_id and workflow.wf_id=%s
        """ % workflow_id
    try:
        #result_total = cursor.execute(SQL)
        result = mysqlobj.execute_query({'sql': SQL})
        LOG.debug("workflow %s has jobs records: %s" % (workflow_id, result.rowcount) )
        workflows_jobs_result = result.fetchall()
        jobs_xml=""
        for row in workflows_jobs_result:
            #pprint.pprint( row ) 
            job_id = row['job_id']
            job_xml = job_instance_detail( job_id )
            jobs_xml = """%s<Job id='%s' type='%s'> 
 <Detail>
   <submit_file>%s</submit_file>
   <executable>%s</executable>
   <argv>%s</argv>
   <task_count>%s</task_count>
 </Detail>
<!-- instance info-->
 %s
 </Job>""" % (jobs_xml, row['job_id'], row['type_desc'], 
                            row['job_file'], row['executable'], row['argv'], row['task_count'], job_xml)
            
        return jobs_xml
    except:
        LOG.debug("ERROR: %s" % sys.exc_info()[0] )
        LOG.error("TRACE: %s" % traceback.format_exc() )

def job_instance_detail (job_id):
    SQL="""SELECT job_instance_id, host_id, user, site, stdout_file, stdout_text, stderr_file, stderr_text, exitcode
        from job_instance  
        where job_id=%s
        """ % job_id
    try:
        #result_total = cursor.execute(SQL)
        result = mysqlobj.execute_query({'sql': SQL})
        jobs_result = result.fetchall()
        instance_xml="";
        for row in jobs_result:
            host_id = row['host_id']
            if ( host_id is None): host_info=""
            else: host_info = host_details(host_id)

            instance_xml = """<JobInstance id='%s' user='%s'>
 <exitcode>%s</exitcode>
<ExecutionOutput>
  <stdout_file>%s</stdout_file>
  <stdout_text>%s</stdout_text>
  <stderr_file>%s</stderr_file>
  <stderr_text>%s</stderr_text>
</ExecutionOutput>
<ExecutionLayer>
 <HostInfo>
   <host_id>%s</host_id>
%s
  </HostInfo>
</ExecutionLayer>
</JobInstance>""" % (row['job_instance_id'], row['user'],row['exitcode'], 
             row['stdout_file'], row['stdout_text'], row['stderr_file'], row['stderr_text'], host_id, host_info) 
        return instance_xml
    except:
        LOG.debug("ERROR: %s" % sys.exc_info()[0] )
        LOG.error("TRACE: %s" % traceback.format_exc() )

def host_details(host_id):
    SQL="""SELECT * from host where host_id=%s""" % host_id
    try:
        #cursor.execute( SQL )
        result = mysqlobj.execute_query({'sql': SQL})
        host_info = result.fetchall()
        host_xml=""
        for host in host_info:
            host_xml = """<site>%s</site>
 <hostname>%s</hostname>
 <ip>%s</ip>
 <uname>%s</uname>
 <total_memory>%s</total_memory>""" % (host['site'], 
    host['hostname'], host['ip'], host['uname'], host['total_memory'])
        return host_xml
    except:
        LOG.debug("ERROR: %s" % sys.exc_info()[0] )
        LOG.error("TRACE: %s" % traceback.format_exc() )

def data_provenance(data_id):
    SQL="UPDATE workflow set db_url='mysql://pegasus:oxy123@localhost:3306/pegasusdb'"
    try:
        result = mysqlobj.execute_query({'sql': SQL})
        LOG.debug("updated workflow table %s" % result['total'])
    except:
        LOG.debug("ERROR: %s" % sys.exc_info()[0] )
        LOG.error("TRACE: %s" % traceback.format_exc() )
        LOG.error("SQL: %s" % SQL)


def writeXML(xml, fname):
    f = open(fname, 'w')
    f.write(xml)
    f.close()
    LOG.debug('Provenance XML written to %s'% fname)
    
def db_close():
    global mysqlobj
    mysqlobj.close_connection()
 
if __name__ == '__main__':   
   st_time = time.time()
   #workflows_provenane( )
   #workflow_provenance(105)
   test()
   end_time = time.time()
   LOG.debug("TimeTaken: %s seconds" % ( end_time - st_time))
   LOG.debug("closing db connection")
   db_close()


