#from multilayer.persistency.plugin.SqlAlchmeyApi import SqlAlchemyApi
#from multilayer.persistency.plugin import MySQLApi, Neo4jAPI
import importlib
import sys
"""
This code will act as a factory class to instantiate persistent layer objects
"""
__author__ = 'khawar'

def getPersistentObj(persistency_plugin, config):
    try:
        persistent_mod = importlib.import_module("multilayer.persistency.plugin." + persistency_plugin)
        persistent_class = getattr(persistent_mod, persistency_plugin)
        persistent_obj = persistent_class(config)
        #print persistent_obj
        return persistent_obj
    except:
        print sys.exc_info()
        raise Exception("Plugin %s not found" % persistency_plugin)