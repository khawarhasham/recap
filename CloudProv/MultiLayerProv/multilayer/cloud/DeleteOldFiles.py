__author__ = 'khawar'

from multilayer.cloud.CloudStorageProv import CloudStorageLayer

storage = CloudStorageLayer(verbose=False)
containers = storage.get_containers()
for container in containers:
    if container.name.startswith("wflocalscratch17"):
        print container.name, container.extra['object_count']
        if container.extra['object_count'] == 0:
            container.delete()