from multilayer.Defaults import config
from multilayer.Defaults import COMPUTE_DRIVER, STORE_DRIVER
import logging

import os

class LayerProvenance (object):
    def __init__(self):
        self.LOG = logging.getLogger("provlog")

        self.COMPUTE_DRIVER = COMPUTE_DRIVER
        self.STORE_DRIVER = STORE_DRIVER
        #self.NOVA_SERVICE_NAME = config['cloud_settings'][]
        #for debugging libcloud
        #os.environ['LIBCLOUD_DEBUG'] = '/dev/stderr'
        self.controller_host= config['cloud_settings']['swift_host']

        self.service_name= os.environ['service_name'] if (os.environ.has_key("service_name")) \
                                                    else config['cloud_settings']['service_name']
        #set os_username, os_password, os_auth_url, os_tennat_name envs
        self.OS_USERNAME=os.environ['OS_USERNAME'] if (os.environ.has_key("OS_USERNAME")) \
                                                   else config['cloud_settings']['os_username']

        self.OS_PASSWORD=os.environ['OS_PASSWORD'] if (os.environ.has_key("OS_PASSWORD")) \
                                                   else config['cloud_settings']['os_password']
        #add /tokens for osdc cloud
        self.OS_AUTH_URL=os.environ['OS_AUTH_URL'] if (os.environ.has_key("OS_AUTH_URL")) \
                                                   else config['cloud_settings']['os_auth_url']

        self.OS_TENANT_NAME=os.environ['OS_TENANT_NAME'] if (os.environ.has_key("OS_TENANT_NAME")) \
                                                   else config['cloud_settings']['os_tenant_name']

        self.OS_REGION_NAME=os.environ['OS_REGION_NAME'] if (os.environ.has_key("OS_REGION_NAME")) \
                                                    else config['cloud_settings']['os_region_name']

        #set os_username, os_password, os_auth_url, os_tennat_name envs
        self.S_OS_USERNAME=os.environ['OS_USERNAME'] if (os.environ.has_key("OS_USERNAME")) \
            else config['storage_settings']['os_username']

        self.S_OS_PASSWORD=os.environ['OS_PASSWORD'] if (os.environ.has_key("OS_PASSWORD")) \
            else config['storage_settings']['os_password']
        #add /tokens for osdc cloud
        self.S_OS_AUTH_URL=os.environ['OS_AUTH_URL'] if (os.environ.has_key("OS_AUTH_URL")) \
            else config['storage_settings']['os_auth_url']

        self.S_OS_TENANT_NAME=os.environ['OS_TENANT_NAME'] if (os.environ.has_key("OS_TENANT_NAME")) \
            else config['storage_settings']['os_tenant_name']

        self.S_OS_REGION_NAME=os.environ['OS_REGION_NAME'] if (os.environ.has_key("OS_REGION_NAME")) \
            else config['storage_settings']['os_region_name']


        