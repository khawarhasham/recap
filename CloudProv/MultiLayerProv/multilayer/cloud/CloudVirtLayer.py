import time

from libcloud.compute.providers import get_driver
import libcloud
from libcloud.compute.deployment import MultiStepDeployment
from libcloud.compute.deployment import ScriptDeployment, SSHKeyDeployment
from libcloud.compute.base import NodeAuthSSHKey


libcloud.security.VERIFY_SSL_CERT = False
from multilayer.cloud.LayerProvenance import LayerProvenance

import multilayer.util.Util as Util

'''TODO: can also collect bandwidth value from flavor but it is None at the moment. '''
class CloudVirtLayer(LayerProvenance):

    def __init__(self):
        super(CloudVirtLayer, self).__init__()
        self.driver = None
        self.nodes = None
        self.flavors = None
        self.images = None
        self.refresh=False

    def get_openstack(self):
        #global driver
        OpenStack = get_driver(self.COMPUTE_DRIVER)
        self.driver = OpenStack(self.OS_USERNAME, self.OS_PASSWORD,
                       #host=self.controller_host, port='8774',
                       secure=False,
                       ex_force_auth_url="%s"%self.OS_AUTH_URL,
                       #ex_force_auth_url='http://%s:5000/v2.0' % self.controller_host,
                       ex_force_auth_version='2.0_password',
                       ex_force_service_name=self.service_name,
                       #ex_force_service_name=self.service_name,
                       ex_force_service_type='compute',
                       #ex_tenant_name='uwecloud',
                       #ex_force_service_region='UWE_Region'
                       ex_tenant_name=self.OS_TENANT_NAME,
                       ex_force_service_region=self.OS_REGION_NAME)

        self.LOG.debug(self.driver)

        #get flavor information based on flavor id
        nodesize = self.driver.ex_get_size(1)
        self.LOG.debug("nodes :%s" % nodesize)

    
    def get_nodes(self):
        #global driver, nodes
        if ( self.driver == None):
            self.get_openstack()
            
        self.nodes = self.driver.list_nodes()
        return self.nodes
        
    def get_node(self, node_ip, verbose=False):
        #first populate the nodes collection
        if ( self.nodes == None ): self.get_nodes()
        for node in self.nodes:
            if node_ip in node.private_ips:
               self.LOG.debug('found node %s' % node_ip )
               if ( verbose ): Util.show_node(node)
               return node
        return None
    
    def get_node_by_name(self, node_name, verbose=False):
        #first populate the nodes collection
        if ( self.nodes == None): self.get_nodes()
        for node in self.nodes:
            if node_name == node.name:
               self.LOG.debug('found node %s' % node_name )
               if ( verbose ): Util.show_node(node)
               #return Util.get_node(node)
               return node
        return None
        
    def _get_image(self, image_id, verbose=False):
        #global images
        #print 'searching for image %s ' % image_id
        for image in self.images:
            if ( image_id == image.id):
                if ( verbose ): Util.show_images([ image ])
                return image
                
    def get_image_name (self, name):
        for image in self.images:
            if ( name == image.name ):
                return image
        return None
        
    def get_flavors(self):
        if ( self.driver == None):
            self.get_openstack()
        self.flavors = self.driver.list_sizes()
        return self.flavors

    def get_images(self):
        if ( self.driver == None):
            self.get_openstack()
            
        self.images = self.driver.list_images()
        return self.images

    def get_flavor(self, flavor_id):
        flavor = self.driver.ex_get_size(flavor_id)
        Util.show_flavor(flavor)

    def deploy_node(self, node_name, key, script, image_name, flavor_name):
        st_time = time.time()

        if (self.driver == None): self.get_openstack()
        
        self.get_images()
        self.get_flavors()
        
        image = self.get_image_name(image_name)
        flavor = Util.get_flavor_name(flavor_name, self.flavors)

        #TODO: check if the script requires the startup of condor daemons
        #Script is user dependent. But for prototyping purpose, we need to hard code it

        step1 = SSHKeyDeployment (key)
        step2 = ScriptDeployment (script)
        pubkey = NodeAuthSSHKey(key)
        msd = MultiStepDeployment ( [ step1, step2 ] )
        node = self.driver.deploy_node(name=node_name, image= image, size=flavor,
                                        deploy = msd, ssh_username='ubuntu',
                                        ex_keyname="kh-key", auth= pubkey,
                                        ssh_timeout=50, ssh_interface='private_ips')

        end_time = time.time()
        self.LOG.debug('Time to spawn node %s:%s ms' % (node_name, end_time - st_time) )

    def run_cmd(self):
        pass

if __name__=='__main__':
    cvl = CloudVirtLayer()
    node = cvl.get_node_by_name('uwe-vm1', True)

    nodes = cvl.get_nodes()
    cvl.get_images()
    flavors = cvl.get_flavors()
    cvl.get_flavor(2)
    Util.show_nodes(nodes, flavors)
    
