from libcloud.storage.providers import get_driver as get_storage_driver

from libcloud.storage.types  import ObjectDoesNotExistError

from multilayer.cloud.LayerProvenance import LayerProvenance


VERIFY_SSL_CERT = False

from pprint import pprint
import subprocess
import hashlib

class CloudStorageLayer(LayerProvenance):
    
    def __init__(self, verbose=False):
        super (CloudStorageLayer, self).__init__()
        self.conn=None
        self.verbose=verbose

    def get_swiftstorage(self):
        print self.S_OS_USERNAME
        print self.S_OS_PASSWORD
        print self.S_OS_AUTH_URL
        OpenStackSwift = get_storage_driver(self.STORE_DRIVER)
        self.swift = OpenStackSwift(self.S_OS_USERNAME, self.S_OS_PASSWORD, secure=False,
                           #ex_force_auth_url='http://%s:5000' % self.controller_host,
                           ex_force_auth_url=self.S_OS_AUTH_URL,
                           ex_force_auth_version='2.0_password',
                           #ex_force_service_name='swift',
                           #ex_force_service_type='object-store',
                           ex_tenant_name=self.S_OS_TENANT_NAME,
                           #ex_region_name='UWE_Region',
                           ex_force_service_region='UWE_Region'
                           )

    
        #pprint (self.swift.ex_get_meta_data()  )
    
        '''upload a file '''
        #self.upload_file_to_cloud(container)
        #print swift.ex_get_meta_data()

    def get_containers(self):
        if ( self.conn == None):
            self.get_swiftstorage()

        containers = self.swift.list_containers()
        download_path="~/Documents/study/phd/coding/cloudapi/libcloud_api/"

        if ( self.verbose ): self.show_containers(containers)

        return containers

    def get_container(self, container_name):
        if ( self.conn == None):
            self.get_swiftstorage()

        container = self.swift.get_container(container_name)
        #print container
        return container

    def get_cloudfile(self, container_name, cloudfile):
        if ( self.conn == None):
            self.get_swiftstorage()

        try:
            obj = self.swift.get_object(container_name, cloudfile)
            if ( self.verbose ):
                self.show_file(obj)
            return obj
        except ObjectDoesNotExistError:
            #print container_name, cloudfile, ' does not exist'
            pass

        return None

    def update_file(self, container_name, file_name, metadata):
        container = self.get_container(container_name)
        #help(container)
        fileobj = self.get_cloudfile(container_name, file_name)
        #downloaded = self.swift.download_object(fileobj, "./%s" % fileobj.name)
        upload =True
        if ( upload ):
            filepath = "./%s"%fileobj.name

            fhandle = open(filepath, "rb")
            md5_hash = hashlib.md5(fhandle.read()).hexdigest()
            fhandle.close()
            print md5_hash

            metadata['test'] = 'khawarahmad'
            metadata['nn'] = 1
            metadata['md5_hash'] = md5_hash
            extra = {'meta_data': metadata}
            pprint (extra)

            print 'uploading modified file'

            with open(filepath, "rb") as iterator:
                obj = self.swift.upload_object_via_stream(iterator=iterator,
                                                          container=container,
                                                          object_name=fileobj.name,
                                                          extra=extra)
                print obj
            #print container.upload_object(filepath, fileobj.name, extra=metadata)


    def upload_file_to_cloud(self, container):
        directory = '~/Documents/study/phd/coding/cloudapi/precip-0.3'
        cmd = 'tar cvzpf - %s' % (directory)
        pipe = subprocess.Popen(cmd, bufsize=0, shell=True, stdout=subprocess.PIPE)
        return_code = pipe.poll()
        print 'Uploading object...'

        while return_code is None:
            # Compress data in our directory and stream it directly to CF
            obj = container.upload_object_via_stream(iterator=pipe.stdout,
                                                 object_name='test/precip-0.3.tar')
            return_code = pipe.poll()

        print 'Upload complete, transferred: %s KB' % ((obj.size / 1024))
    
    def cloud_files(self):
        self.get_swiftstorage()
    
    def show_containers(self, containers):
        for container in containers:
            #swift.enable_container_cdn(container)
            print 'Container name=%s, extra info=%s' % (container.name, container.extra)
            objects = container.list_objects()
            for obj in objects:
                self.show_file(obj)
                '''for downloading a file'''
                #path = "%s/%s" % (os.path.expanduser(download_path), obj.name)
                #obj.download(destination_path=path)

    def show_file(self, obj):
        print obj.name, obj.size
        pprint (obj.extra)
        #help(obj)
        print "meta-data: %s" % obj.meta_data
        print "hash: %s" % obj.hash

if __name__=='__main__':
    sl = CloudStorageLayer(verbose=True)
    #sl.update_file("testfiles","hello.txt",{'author':'pegasus'})
    sl.get_container("wfoutput29112014225508")
    sl.get_cloudfile("test-container","hello.txt")
    #sl.get_containers()
    #19ba39d7194b9c10a3b0d6a375e17aec  -> khawar
    #ed076287532e86365e841e92bfc50d8c  -> hello.txt

    #hash of file does not change even if metadata changes
    #so, I guess it will be helpful if we can store md5(file-hash) along with other metadata