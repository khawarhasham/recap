from urlparse import urlparse
from pprint import pprint
from lxml import objectify, etree
import re

from multilayer.persistency import PersistencyFactory
from multilayer.cloud.CloudStorageProv import CloudStorageLayer
import multilayer.Defaults as Defaults


__author__ = 'khawar'
__version__= '0.0.1'

class PegasusParser(object):
    def __init__(self):
        self.mysqlobj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
        self.cloudStorage = CloudStorageLayer(verbose=False)

    def getlocalfiles(self, job_type, log_tupe, jobid):
        job_files = {}
        for tuple in log_tupe:
            print jobid, tuple
            cmd_src_file = tuple[0]
            cmd_dst_file = tuple[1]
            ind = cmd_dst_file.rfind("/")
            file_name = cmd_dst_file[ind+1:]
            container_name = cmd_dst_file[:ind]
            job_files[container_name] = file_name
        return job_files

    def showTuple(self, job_type, log_tuple, jobid):
        print log_tuple
        print '\n\n Job id %s' % jobid
        cloud_files = {}
        for tuple in log_tuple:
            #print tuple
            #print "\n\tTime:%s" % tuple[0]
            #print "\tMethod:%s" % tuple[2]
            print "command:%s" % tuple[3]
            file_command = tuple[3].strip()
            parts = file_command.split(" ")

            if ( len(parts) == 0 and tuple[2] == 'rm'):
                parts.append(file_command)

            if ( tuple[2] != 'rm'):
                parts = parts[1:]

            #print parts
            for part in parts:
                #print part
                if ( part == ''): continue
                o = urlparse(part)
                #print "\tPath: %s" % ( o.path)

                #if the file transfer scheme is S3, which means it was uploaded to cloud storage
                if ( o.scheme == 's3'):
                    ind = o.path.find("/", 2)
                    container_name = o.path[1:ind]
                    key_name = o.path[ind+1:]
                    #print '\nlooking for container=%s key=%s in cloud storage' % (container_name, key_name)
                    '''the following line can be removed because at the time of insertion we are checking this'''
                    #obj = self.cloudStorage.get_cloudfile(container_name, key_name) #uncomment when cloud service is up
                    obj = 1 #remove this line when cloud service is up
                    if ( obj != None):
                        if ( not cloud_files.has_key(container_name)):
                            cloud_files[container_name]=[key_name]
                        else:
                            cloud_files[container_name].append(key_name)
                        #pprint (cloud_files)

        return cloud_files

    def parse_job_output(self, job_output):
        job_output = job_output.encode('ascii', 'ignore')

        job_output = job_output.replace("%0A", "\n")
        job_output = job_output.replace("%0D", "\n")
        job_output = job_output.replace("%27", " ")
        #job_output = job_output.replace("s3", " s3")
        #print job_output

        s3_pattern = r"(.+)\s+(\w+:)\s+pegasus-s3 (.+) -f (.+)"
        s3_pattern2= r"(.+)\s+(\w+:)\s+pegasus-s3 (rm) (.+)"
        wget_pattern = r"wget -q --no-check-certificate (.+) (.+)"

        #m = re.search(s3_pattern, job_output)
        #s3_m = m if (m) else re.search(s3_pattern2, job_output)
        m = re.findall(s3_pattern, job_output)
        s3_m = m if (m) else re.findall(s3_pattern2, job_output)
        if s3_m:
            self.showTuple("" , s3_m)
            #print s3_m
            #print s3_m.group()
            #print s3_m.groups()

        else:
            m = re.search(wget_pattern, job_output)
            if m:
                print m.group()
                print m.groups()

    def processOutputText(self, job_output, jobid):

        if ( job_output is None ):
            print 'output for job %s is None ' % jobid
            return None

        #print jobid
        '''convert unicode to str '''
        job_output = job_output.encode('ascii', 'ignore')

        job_output = job_output.replace("%0A", "\n")
        job_output = job_output.replace("%0D", "\n")
        job_output = job_output.replace("%27", " ")
        #job_output = job_output.replace("s3", " s3")
        #print job_output

        s3_pattern = r"(.+)\s+(\w+:)\s+pegasus-s3 (.+) -f (.+)"
        s3_pattern2= r"(.+)\s+(\w+:)\s+pegasus-s3 (rm) (.+)"
        wget_pattern = r"wget -q --no-check-certificate (.+) (.+)"
        cp_pattern = r"/bin/cp -f -L (.+) (.+)"

        #m = re.search(s3_pattern, job_output)
        #s3_m = m if (m) else re.search(s3_pattern2, job_output)
        m = re.findall(s3_pattern, job_output)
        s3_m = m if (m) else re.findall(s3_pattern2, job_output)
        cp_m = re.findall(cp_pattern, job_output)
        if s3_m:
            return self.showTuple("" , s3_m, jobid)
            #print s3_m
            #print s3_m.group()
            #print s3_m.groups()
        elif cp_m:
            return self.getlocalfiles("", cp_m, jobid)
        else:
            m = re.search(wget_pattern, job_output)
            if m:
                print m.group()
                print m.groups()

    def parseJobOutput(self, jobid, job_exec_name):

        self.mysqlobj.get_connection()
        SQL = "select job_instance_id, stdout_file, stdout_text, stderr_text from job_instance where job_id=%s" % jobid
        result = self.mysqlobj.execute_query({'sql': SQL})
        output_result = result.fetchone()
        self.mysqlobj.close_connection()
        if ( output_result == None):
            return

        #print output_result
        job_output = output_result['stdout_text']
        job_err = output_result['stderr_text']
        outputs_objs = {jobid:{}}   #job_exec_name:[]
        obj = self.processOutputText(job_output, jobid)
        if ( obj != None ):
            outputs_objs[jobid].update(obj)
        obj = self.processOutputText(job_err, jobid)
        if ( obj!= None):
            outputs_objs[jobid].update(obj)

        return outputs_objs[jobid]

    def iterate(self, output_objs):
        result={}
        for output in output_objs:
            #print output
            for jobname, files in output.items():
                for file in files:
                    for container_name, keys in file.items():
                        #print jobname, container_name, keys[0]
                        result[jobname]=[container_name, keys]
        pprint(result)
        return result

    def get_joboutputs(self, wf_id):

        SQL="""SELECT j.job_id, j.exec_job_id, type_desc, job_submit_seq
               FROM job j, job_instance ji
               WHERE (j.job_id=ji.job_id ) AND wf_id=%s
               AND type_desc in ('stage-in-tx','stage-out-tx', 'cleanup')
               ORDER BY ji.job_submit_seq""" % wf_id

        self.mysqlobj.get_connection()
        job_result = self.mysqlobj.execute_query({'sql': SQL})
        #job_result = result['cursor'].fetchall();
        joboutputs = []
        for job in job_result:
            job_id = job['job_id']
            job_type = job['type_desc']
            seq = job['job_submit_seq']
            print 'processing %s job id %s (%s) for workflow %s ' % (seq, job_id, job_type, wf_id)
            joboutputs.append(self.parseJobOutput(job_id, job['exec_job_id']))

        result = self.iterate (joboutputs)
        self.mysqlobj.close_connection()
        return result

    def get_jobdetails(self, wf_id, dagparser):

        SQL="""SELECT j.job_id, j.exec_job_id, type_desc, job_submit_seq, ji.job_instance_id
               FROM job j, job_instance ji
               WHERE (j.job_id=ji.job_id ) AND wf_id=%s
               ORDER BY ji.job_submit_seq""" % wf_id

        self.mysqlobj.get_connection()
        job_result = self.mysqlobj.execute_query({'sql': SQL})
        #job_result = result['cursor'].fetchall();
        jobdetails = {}
        for job in job_result:

            job_id = job['job_id']
            job_type = job['type_desc']
            seq = job['job_submit_seq']
            job_instance_id = job['job_instance_id']
            print 'processing %s job id %s (%s) for workflow %s ' % (seq, job_id, job_type, wf_id)
            jobdetails[job_instance_id]={'jobname':job['exec_job_id'],'jobid':job_id}

            #build the job arguments
            ''' since we are using DAGPArser. so this db access can be avoided '''

            #args = self.getjob_invocation(wf_id, job_instance_id, job['exec_job_id'])
            args = dagparser.get_job_args(job['exec_job_id'])
            #print args
            if ( args is not None ):

                #jobdetails[job_instance_id].update({'jobArgs': args[0], 'job_in':args[1],
                #                       'job_out': args[2]})
                jobdetails[job_instance_id].update({'jobArgs': args['args'], 'job_in': args['uses']['input'],
                                                    'job_out': args['uses']['output']})
            #process the job inputs and outputs files

            jobdetails[job_instance_id].update({'cloudfiles': self.parseJobOutput(job_id, job['exec_job_id'])})


        pprint(jobdetails)
        self.mysqlobj.close_connection()
        return jobdetails

    def getjob_invocation(self, wfid, job_inst_id, jobname):

        SQL="""SELECT argv
            FROM `invocation`
            WHERE job_instance_id=%s and task_submit_seq=1 and wf_id=%s
            """ % ( job_inst_id, wfid )

        result = self.mysqlobj.execute_query({'sql': SQL})
        #print result.rowcount
        one = result.fetchone()
        if ( one ):
            argv = one['argv']
            if ( argv == None):
                return None

            pattern = r"-i (.+) -o (.+)"
            m = re.search(pattern, argv)
            if ( m ):
                inputs = m.group(1)
                inputs = inputs.split(" ")
                outputs = m.group(2)
                outputs = outputs.split(" ")
                return [ argv, inputs, outputs ]

        return None

    def parse_braindump(self, braindump_path):

        dump = open(braindump_path,"r")
        data = dump.read()
        #print data
        dump.close()

        props = {}
        pattern = r"wf_uuid (.+)"
        m = re.search(pattern, data)
        if ( m ):
            #print m.group()
            #print m.groups()
            props['wf_uuid'] = m.group(1)
        pattern = r"submit_dir (.+)"
        m = re.search(pattern, data)
        if ( m ):
            props['submit_dir'] = m.group(1)

        pattern = r"planner_arguments (.+)"
        m = re.search(pattern, data)
        if ( m ):
            props['planner_args'] = m.group(1)

        pattern = r"dax (.+)"
        m = re.search(pattern, data)
        if ( m ):
            dax_file = m.group(1)
            dax_file = dax_file[dax_file.rfind("/"):]
            props['dax_file'] = "%s/%s" % (props['submit_dir'], dax_file)

        pattern = r"properties (.+)"
        m = re.search(pattern, data)
        if ( m ):
            props['pegasusrc'] = "%s/%s" % (props['submit_dir'], m.group(1))

        #pprint (props)
        return props

    def parse_braindump_txt(self, data):

        props = {}
        pattern = r"wf_uuid (.+)"
        m = re.search(pattern, data)
        if ( m ):
            #print m.group()
            #print m.groups()
            props['wf_uuid'] = m.group(1)
        pattern = r"submit_dir (.+)"
        m = re.search(pattern, data)
        if ( m ):
            props['submit_dir'] = m.group(1)

        pattern = r"planner_arguments (.+)"
        m = re.search(pattern, data)
        if ( m ):
            props['planner_args'] = m.group(1)

        pattern = r"dax (.+)"
        m = re.search(pattern, data)
        if ( m ):
            dax_file = m.group(1)
            dax_file = dax_file[dax_file.rfind("/"):]
            props['dax_file'] = "%s/%s" % (props['submit_dir'], dax_file)

        pattern = r"properties (.+)"
        m = re.search(pattern, data)
        if ( m ):
            props['pegasusrc'] = "%s/%s" % (props['submit_dir'], m.group(1))

        pprint (props)
        return props

    def parse_submit_output(self, submit_output):
        #print submit_output
        pattern = r"(.+)\s+(\w+:)\s+ pegasus-status -l (.+)"
        m = re.search(pattern, submit_output)
        if ( m ):
            print m.group(3)
            return m.groups()

        return None

    def get_wfid(self, wf_uuid):
        SQL = "select wf_id from workflow where wf_uuid='%s'" % wf_uuid

        self.mysqlobj.get_connection()
        result = self.mysqlobj.execute_query({'sql': SQL})
        wf_result = result.fetchone()
        self.mysqlobj.close_connection()

        if ( wf_result==None ):
            return
        wf_id = wf_result['wf_id']
        print wf_id
        return wf_id

    def parse_machine_fromXML(self, filename):

        print "parsing xml %s" % filename
        try:
            tree = etree.parse(filename)
        except:
            print 'not a valid xml. so return'
            return

        nsmap = tree.getroot().nsmap.copy()
        nsmap['xmlns'] = nsmap.pop(None)

        #get hostname from the invocation tag
        cpu_detail = {}
        invocation_node = tree.getroot()
        if (invocation_node and invocation_node.attrib.has_key('hostname')):
            hostname =  invocation_node.attrib['hostname']
            cpu_detail['hostname'] = hostname[:hostname.rfind(".")]

        #print nsmap
        machine_node = tree.xpath(".//xmlns:machine/*", namespaces=nsmap)
        platform='linux'
        for node in machine_node:
            #print node.tag.find("uname")
            if ( node.tag.find('uname')>0):
                #uname tag is available in database
                '''
                uname in db
                system+release+machine
                linux-3.5.0-32-generic-x86_64 is = to
                <uname system="linux" release="3.5.0-32-generic" machine="x86_64">
                '''
                platform= node.attrib["system"]
                osname = node.text
                print platform, osname
            elif ( node.tag.find(platform) > 0):
                cpu = node.xpath("./xmlns:cpu", namespaces=nsmap)
                cpu_detail["count"] = cpu[0].attrib["count"]
                cpu_detail["speed"] = cpu[0].attrib["speed"]
                cpu_detail["vendor"] = cpu[0].attrib["vendor"]
                cpu_detail["processor"] = cpu[0].text
                #print cpu_detail
                return cpu_detail
        return None


if __name__=='__main__':
    parser = PegasusParser()
    #parser.parseJobOutput(1636)
    #parser.get_jobdetails(161)

    props = parser.parse_braindump("/Users/khawar/dev_area/Pegasus/Workflow/run0016/braindump.txt")
    wf_id = parser.get_wfid(props['wf_uuid'])
    #parser.get_joboutputs(wf_id)
    f = open("/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/wms/pegasuswms/submit_output")
    data = f.read()
    f.close()
    parser.parse_submit_output(data)

    #for machine node parsing
    #parser.parse_machine_fromXML("/Users/khawar/Documents/CloudProv/run0013/analyse_ID0000003.out.000");
    parser.parse_machine_fromXML("/var/folders/xf/6l0zggkj3dn4lp0gwl_9y10c0000gr/T/tmpMI5S5k.xml");