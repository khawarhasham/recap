from kombu import Connection, Consumer
import multilayer.Defaults as Defaults
from multilayer.notify.queues import task_queues


__author__ = 'khawar'

class NotificationConsumer(object):
    def __init__(self, queue_name):
        config = Defaults.config['rabbit_settings']
        self.conn = Connection('amqp://guest:guest@%s:5672//' % (config['rabbit_host']))
        self.consumer = self.conn.SimpleQueue(queue_name,
                                              #serializer='pickle',
                                              compression="bzip2")

    def dequeue(self):
        obj = self.consumer.get(block=True, timeout=6)
        print obj
        help(obj)


notify = NotificationConsumer("monitor1")
notify.dequeue()