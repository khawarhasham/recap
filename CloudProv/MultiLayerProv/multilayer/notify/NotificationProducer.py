from kombu import Connection
from kombu.pools import producers

from multilayer.notify.queues import task_exchange
import multilayer.Defaults as Defaults

import datetime

__author__ = 'khawar'

rabbit_host="164.11.100.72"

class NotificationProducer(object):
    def __init__(self, queue_name):
        config = Defaults.config['rabbit_settings']
        self.conn = Connection('amqp://guest:guest@%s:5672//' % (config['rabbit_host']))
        self.producer = self.conn.SimpleQueue(queue_name)

    def enqueue(self, payload, routing_key):
        self.producer.put(payload,
                              #serializer='pickle',
                              compression='bzip2',
                              exchange=task_exchange,
                              declare=[task_exchange],
                              routing_key= routing_key
                              )
        print 'message enqueued in rabbitmq'

    def shutdown(self):
        self.producer.close()
        self.conn.close()

notify = NotificationProducer("monitor1")
notify.enqueue({'test': 'abc',
                'now':[1,2,3,4],
                'wfid':130},
               'monitor')