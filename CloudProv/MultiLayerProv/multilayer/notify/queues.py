from kombu import Exchange, Queue

__author__ = 'khawar'

task_exchange = Exchange('CloudProv', type='direct')
task_queues = {'monitor': Queue('monitor', task_exchange, routing_key='monitor'),
               'submit':  Queue('submit', task_exchange, routing_key='submit'),
               'other':   Queue('lopri', task_exchange, routing_key='lopri')}