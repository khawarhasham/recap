from pprint import pprint
import logging
import json
import sys
from datetime import datetime
import time
import traceback

from multilayer.cloud.CloudVirtLayer import CloudVirtLayer
from multilayer.cloud.CloudStorageProv import CloudStorageLayer
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
from multilayer.util import Util

# This assumes you don't have SSL set up.
print Defaults.config
provdb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings']) #change from db_settings
LOG = logging.getLogger("provlog")

'''lazy mapping Phase 2. Phase 1 is handled through monitor.plugin.VirtLayerMonitor'''
def establish_lazy_mapping(wfid, cloudprovid):

    '''get all jobs for which there is no host id and its mapping in temp table.
    since we can rely on host info from pegasus db so we ll use the mapping from temp table.
    moreover, it also assumes that the required VMs are already destroyed and no longer available
    in the Cloud infrastructure'''

    #, host.host_id,hostname,ip from job_instance as inst left join host on inst.host_id=host.host_id
    jobSQL = """select job_id, host_id, hostname, ip from job_instance as inst, host as h
             where job_id in (select job_id from job where wf_id=%s) """ % wfid

    SQL="""SELECT * FROM cloudprov.WfCloudVMs WHERE hostname IN
       (
         SELECT hostip inst.job_id FROM pegasusdb.job_instance AS inst WHERE inst.host_id IS NULL
         AND inst.job_id IN
         ( SELECT inst.job_id
         FROM pegasusdb.job_instance AS inst LEFT JOIN pegasusdb.job ON job.job_id = inst.job_id
         WHERE job.wf_id =%s
         )
       )AND wfID =%s""" % (wfid, cloudprovid)

    clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
    target_db = clouddb.get_connection()

    try:
        map_result = clouddb.execute_query({'sql': SQL})
        records = []
        for row in map_result:
            d={'wfID': row['wfID'],
               'jobid': row['jobid'],
               'nodename':row['hostname'],
               'hostip':row['hostip'],
               'middleware':row['middleware'],
               'minRAM':row['minRAM'],
               'minHD':row['minHD'],
               'vCPU':row['vCPU'],
               'flavorid':  row['flavorid'],
               'flavorname': row['flavorname'],
               'image_name':  row['image_name'],
               'image_id':row['image_id'],
               'extra':json.dumps({})
            }
            records.append(d)

        insertreq={'conn':target_db, 'table_name':'WfCloudMapping', \
                   'insert_values': records}
        clouddb.insert_record(insertreq)

    except:
        print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );

'''##################### Eager Approach Phase 2 ##########################'''
def establish_eager_mapping(wfid, cloudprovid):

    '''get all jobs for which there is no host id and its mapping is present in temp table.
    since we can rely on host info from pegasus db so we ll use the mapping from temp table.
    moreover, it also assumes that the required VMs are already destroyed and no longer available
    in the Cloud infrastructure.

    Here we need to do two phase mapping.
    1) map all jobs with temp table for which we don' have host info from pegasus.
    2) if some jobs have host info, match them with temp table and link them
    to have one mapping and avoid the scenario in which a mapping could be missed.
    dont look for null hostids.
    get all the host info (null or not null)'''

    #, host.host_id,hostname,ip from job_instance as inst left join host on inst.host_id=host.host_id
    SQL="""SELECT * FROM cloudprov.WfCloudTempMapping WHERE jobid IN
       (
         SELECT inst.job_id FROM pegasusdb.job_instance AS inst WHERE inst.job_id IN
         ( SELECT inst.job_id
         FROM pegasusdb.job_instance AS inst LEFT JOIN pegasusdb.job ON job.job_id = inst.job_id
         WHERE job.wf_id =%s
         )
       )AND wfID =%s""" % (wfid, cloudprovid)

    clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
    target_db = clouddb.get_connection()

    try:
        map_result = clouddb.execute_query({'sql': SQL})
        records = []
        for row in map_result:
            d={'wfID':       row['wfID'],
               'jobid':      row['jobid'],
               'nodename':   row['hostname'],
               'hostip':     row['hostip'],
               'middleware': row['middleware'],
               'minRAM':     row['minRAM'],
               'minHD':      row['minHD'],
               'vCPU':       row['vCPU'],
               'flavorid':   row['flavorid'],
               'flavorname': row['flavorname'],
               'image_name': row['image_name'],
               'image_id':   row['image_id'],
               'extra':      json.dumps({})
            }
            print d
            records.append(d)

        #move mapping from WfCloudTempMapping to WfCloudMapping
        insertreq={'conn':target_db, 'table_name':'WfCloudMapping', \
                   'insert_values': records}
        clouddb.insert_record(insertreq)

    except:
        print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );

'''############ Eager Approach Phase 2 ends ###############'''

'''Eager Approach Phase 1 '''
def create_eager_temp_mapping(hostrecord, condor_mips):
    print hostrecord

    orghostname = hostrecord['hostname']  #this will be used for mips
    host_mips = -1
    if ( condor_mips.has_key(orghostname)):
        host_mips = condor_mips[orghostname]

    hostname = hostrecord['hostname']
    #OpenStack' hostname is different from Condor's. So remove the domain part from hostname
    #this could be one possible problem if hostname contains . (which is difficult). but still.
    #one workaround could be to you condor_history information and use IP from there but this work around
    #would assume that the VM still exists which is not the case for eager approach

    hostname = hostname[:hostname.find(".")]
    hostrecord['hostname']=hostname
    jobid = hostrecord['jobid']
    wfid = hostrecord['wfID']
    condor_id = hostrecord['condor_id']

    virt_layer = CloudVirtLayer()
    virt_layer.get_images()
    virt_layer.get_flavors()
    #TODO: change it with name or ip search coz condor can give us both
    #FIXED: now with both
    vm = virt_layer.get_node_by_name(hostname)  #look for name
    vm = vm if vm else virt_layer.get_node(hostname)   #if name not matched, look for ip
    if ( vm != None ):
        print 'found vm %s' % vm.private_ips[0]
        vm_flavor = Util.get_flavor(vm.extra['flavorId'], virt_layer.flavors)
        vm_img = virt_layer._get_image(vm.extra['imageId'], False)
        hostrecord['hostip']=vm.private_ips[0]  #this can be changed to public
        hostrecord['flavorid']=vm_flavor.id
        hostrecord['flavorname']=vm_flavor.name
        hostrecord['minRAM']=vm_flavor.ram
        hostrecord['minHD']=vm_flavor.disk
        hostrecord['vCPU']=vm_flavor.vcpus
        hostrecord['image_name']= vm_img.name
        hostrecord['image_id']=vm_img.id
        hostrecord['middleware']='OpenStack'
        hostrecord['condor_id']=condor_id
        hostrecord['mon_time']=datetime.utcnow()

        print hostrecord
        SQL = """SELECT hostip from WfCloudTempMapping
        where (jobid=%s and wfid=%s and condor_id=%s) and (hostname='%s' and hostip='%s')
        """ % (jobid, wfid, condor_id, hostname, hostrecord['hostip'])

        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()
        try:
            result = clouddb.execute_query({'sql':SQL})
            records = result.fetchone()
            if records:
                '''this record exists so do nothing and return back'''
                LOG.debug("A temporary mapping exists for job %s wf %s hostname %s" % (jobid, wfid, hostname))
                clouddb.close_connection()
                return

        except:
            print 'Error found in eager check: %s' % sys.exc_info()[0]
            traceback.print_exc(file=sys.stdout)

        insertreq={'conn':target_db, 'table_name':'WfCloudTempMapping', \
               'insert_values': [hostrecord]}
        #provdb.insert_record( target_db, 'VirtualLayerProvenance', records)
        print clouddb.insert_record(insertreq)

        #now create mapp between jobid, wfid, and mips found from condor
        d = {'wfID':wfid, 'jobid':jobid, 'mips':host_mips}
        insertreq={'conn':target_db, 'table_name':'CPUSpecs', 'insert_values':[d]}
        print clouddb.insert_record(insertreq)

        print 'closing db connection'
        clouddb.close_connection()


'''
second wfid added for cloudprov db

TODO: for dynamic flag as True, check for host info from WfCloudTempMapping if Pegasus DB does not have this
or that VM is no more exist on infrastructure.
'''
def establishJobResourceMapping(wf_id, cloudprov_wfid, dynamic=False):
    global provdb, LOG
    provdb.get_connection()

    SQL = """select inst.job_id, host.* from host left join job_instance as inst on inst.host_id=host.host_id
              where host.site<>'local' and inst.job_id in (
                 select inst.job_id
                 from job_instance as inst left join job on job.job_id=inst.job_id
                 left join workflow as wf on wf.wf_id=job.wf_id
              where wf.wf_id=%s)""" % wf_id

    '''select host.* from host where site<>'local' and host_id in (
              select inst.host_id 
              from job_instance as inst left join job on job.job_id=inst.job_id
              left join workflow as wf on wf.wf_id=job.wf_id
              where wf.wf_id=%s)""" % wf_id'''
    print SQL
    host_list={}
    try:
        host_result  = provdb.execute_query({'sql': SQL})
        #print host_result
        for row in host_result:
            print row
            if ( not host_list.has_key(row['ip'])):
                host_list[row['ip']] = {}

            if ( not host_list[row['ip']].has_key('host') ):
                host_list[row['ip']]['host'] = [row]
            else:
                host_list[row['ip']]['host'].append(row)

    except:
        print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );


    pprint(host_list)
    LOG.debug ('accessing cloud middleware for mappings for wfid %s' % wf_id)
    virt_layer = CloudVirtLayer()
    virt_layer.get_images()
    virt_layer.get_flavors()
    
    for host_ip, host_info in host_list.items():
        LOG.debug ('retrieve vm info for %s' % host_ip )
        vm = virt_layer.get_node(host_ip, True)

        if ( vm != None ):
            vm_flavor = Util.get_flavor(vm.extra['flavorId'], virt_layer.flavors)
            vm_img = virt_layer._get_image(vm.extra['imageId'], False)
            host_list[host_ip]['flavor']=[vm_flavor.id, vm_flavor.name, \
                                          vm_flavor.ram, vm_flavor.disk, vm_flavor.vcpus]
            host_list[host_ip]['image']=[vm_img.id, vm_img.name]
    pprint (host_list)
    
    #insert host_list in provdb table
    records = []
    a = {}

    #TODO: see if hostname vm_name can also be added

    for host_ip, host_detail in host_list.items():

        if ( not host_detail.has_key('flavor') ): continue

        flavor = host_detail['flavor']
        vm_img = host_detail['image']
        jobs = host_detail['host']

        for job in jobs:
            d = {'wfID':     cloudprov_wfid, #change from wfid
                 'jobid':     job[0],
                 'hostid':    job[1],
                 'nodename':  job[4],  #
                 'hostip':    host_ip,
                 'middleware':'OpenStack',
                 'flavorid':   flavor[0],
                 'flavorname': flavor[1], #added for cloudprov
                 'minRAM':     flavor[2],
                 'minHD':      flavor[3],
                 'vCPU':       flavor[4],
                 'image_name': vm_img[1],
                 'image_id':   vm_img[0],
                 'extra':      json.dumps(a)}
            records.append(d)
    print provdb
    provdb.close_connection()
    #pprint(records)
    #for cloudprov db: change VirtualLayerProvenance to WFCloudMapping
    clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
    target_db = clouddb.get_connection()
    insertreq={'conn':target_db, 'table_name':'WfCloudMapping', \
               'insert_values': records}
    #provdb.insert_record( target_db, 'VirtualLayerProvenance', records)
    clouddb.insert_record(insertreq)
    print 'closing db connection'
    clouddb.close_connection()

def establish_jobfile_mapping(job_details, cloudprov_wfid):
    cloudStore = CloudStorageLayer()
    records = []
    for job_id, job_detail in job_details.items():
        cloudfiles = job_detail['cloudfiles']
        if ( len(cloudfiles.keys())>0 ):
            for container, file_keys in cloudfiles.items():
                for file_key in file_keys:
                    cloud_obj = cloudStore.get_cloudfile(container, file_key)
                    d={
                       'container_name': container,
                       'keyname': file_key,
                       'size': cloud_obj.size,
                       'metadata': json.dumps(cloud_obj.meta_data),
                       'hash': cloud_obj.hash,
                       'extra': json.dumps(cloud_obj.extra),
                       'modified_date': cloud_obj.extra['last_modified'],
                       'wfid': cloudprov_wfid
                    }
                    records.append(d)

    clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
    target_db = clouddb.get_connection()
    insertreq={'conn':target_db, 'table_name':'CloudFileCatalog', \
               'insert_values': records}
    clouddb.insert_record(insertreq)
    """
    jobcloudfiles.append({
            'wfID':cloudprov_wfid,
            'jobid': job_id,
            'cloudFileID':1,
    })"""
    for job_instid, job_detail in job_details.items():
        jobid = job_detail['jobid']
        if ( job_detail.has_key("job_out")):
            job_outs = job_detail['job_out']
            for out in job_outs:
                SQL="""select CloudFileID from CloudFileCatalog
                    where wfid=%s and keyname='%s'""" %(cloudprov_wfid, out)
                res = clouddb.execute_query({'sql': SQL})
                one = res.fetchone()
                cloudfile_id = one['CloudFileID']

                SQL="""INSERT into JobCloudFile
                    VALUES(%s,%s,%s)
                    """ % (cloudprov_wfid, jobid, cloudfile_id)
                res = clouddb.execute_query({'sql': SQL})

    clouddb.close_connection()


def establish_cpuspec_mapping(cpuspecs_map):
    print 'Insreting CPUSpec mapping'
    print cpuspecs_map
    clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
    target_db = clouddb.get_connection()

    try:
        insertreq={'conn':target_db, 'table_name':'CPUSpecs', 'insert_values':cpuspecs_map}
        print clouddb.insert_record(insertreq)
    except:
        print 'Error found in eager check: %s' % sys.exc_info()[0]
        traceback.print_exc(file=sys.stdout)

def main():
    #global driver, nodes, flavors, images
    '''get_openstack()
    get_flavors()
    get_nodes()
    show_nodes(nodes,flavors)
    get_images()
    show_images(images)
    show_flavors(flavors)'''

if __name__=='__main__':
    #establishJobResourceMapping(114)
    #cloud_files()
    establish_eager_mapping(336, 24)

'''

########### pycounter reporting settings #####
JSONFile="./reporting.json"
reporter = reporters.JSONFileReporter(output_file=JSONFile)
register_reporter(reporter)
start_auto_reporting()
############ pycounter settings ##############

prof = hotshot.Profile("stones.prof")
prof.runcall(main)
prof.close()
stats = hotshot.stats.load("stones.prof")
stats.strip_dirs()
stats.sort_stats('time', 'calls')
stats.print_stats(20)
'''

'''
<machine page-size="4096">
    <stamp>2013-08-28T16:54:58.903+00:00</stamp>
    <uname system="linux" nodename="uwe-vm8" release="3.5.0-32-generic" machine="x86_64">#53-Ubuntu SMP Wed May 29 20:23:04 UTC 2013</uname>
   <linux>
    <ram total="2100760576" free="1557573632" shared="0" buffer="64782336"/>
    <swap total="0" free="0"/>
    <boot idle="1281346.440">2013-08-13T20:07:33.353+00:00</boot>
    <cpu count="1" speed="2394" vendor="AuthenticAMD">AMD Opteron 23xx (Gen 3 Class Opteron)</cpu>
    <load min1="0.86" min5="0.53" min15="0.25"/>
   </linux>
</machine>


select * from invocation where job_instance_id in 
(SELECT job_instance_id FROM `job_instance` WHERE job_id in 
(select job_id from job where wf_id=98)
)

'''