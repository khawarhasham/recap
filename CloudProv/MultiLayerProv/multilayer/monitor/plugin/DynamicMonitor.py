from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
import logging
import time

__author__ = 'khawar'

LOG = logging.getLogger("provlog")
dbconn = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
dbconn.get_connection()
wf_uuid="80a783ac-f37c-4dca-9286-1b2dddc0aa5c"

time.sleep(5)
LOG.debug("looking for wfid")
SQL="SELECT wf_id from workflow where wf_uuid='%s'" % wf_uuid
result = dbconn.execute_query({'sql':SQL})
record = result.fetchone()
LOG.debug(record)
wfid = record[0]

LOG.debug("found wfid :%s" % wfid)

SQL="""SELECT job.exec_job_id,job_instance.*
       FROM `job`,job_instance
       WHERE job.job_id=job_instance.job_id and wf_id=%s order by job_submit_seq
    """ % wfid

while ( True ):
    LOG.debug("Inside While-loop")
    LOG.debug(SQL)
    instances = dbconn.execute_query({'sql': SQL})
    LOG.debug("jobs :%s " % instances.rowcount)
    for inst in instances:
        job_id = inst['job_id']
        LOG.debug(" Job process %s on host %s" % (job_id, inst['host_id']))

    time.sleep(5)
