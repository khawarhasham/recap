import subprocess
from subprocess import Popen, PIPE
import shlex
from re import split
from sys import stdout

import platform
import socket
import math
from datetime import timedelta
import time
import sys
import os

'''
@Author: khawar
'''


class Proc(object):
    ''' Data structure for a processes . The class properties are
    process attributes '''
    def __init__(self, proc_info):
        self.user = proc_info[0]
        self.pid = proc_info[1]
        self.cpu = proc_info[2]
        self.mem = proc_info[3]
        self.vsz = proc_info[4]
        self.rss = proc_info[5]
        self.tty = proc_info[6]
        self.stat = proc_info[7]
        self.start = proc_info[8]
        self.time = proc_info[9]
        self.cmd = proc_info[10]
        self.extended_cmd = " ".join(proc_info[10:])


    def to_str(self):
        ''' Returns a string containing minimalistic info
        about the process : user, pid, and command '''
        return '%s %s %s' % (self.user, self.pid, self.cmd)

provenance_xml = '<?xml version="1.0"?><MonitoringInfo>'

'''
provide information about the underlined execution machine
<sysinfo></sysinfo>
'''
def get_sys_info():
    global provenance_xml
    
    xml = '<hostname>%s</hostname>' % socket.gethostname()
    xml += '<machine>%s</machine>' % platform.machine()
    xml += '<version>%s</version>' % platform.version()
    xml += '<platform>%s</platform>' % platform.platform()
    xml += '<system>%s</system>' % platform.system()
    xml += '<architecture>%s</architecture>' % str(platform.architecture())
    xml += '<uname>%s</uname>' % str(platform.uname())
#    print socket.gethostbyaddr(socket.gethostname())
#    print socket.gethostbyname_ex( socket.gethostname() )
    provenance_xml += "<SysInfo>%s</SysInfo>" % xml
    return xml

'''
provide information about CPUs inside the execution machine
<cpuinfo></cpuinfo>
'''
def get_cpu_info():
    global provenance_xml

    cpu_info = {}
    if ( platform.system() == 'Darwin'):
        CMD = "/usr/sbin/sysctl -n machdep.cpu.brand_string machdep.cpu.core_count"
        args = shlex.split(CMD)
        sub_proc = Popen(args, shell=False, stdout=PIPE)

        #out = sub_proc.stdout.readline()
        out = []
        for l in sub_proc.stdout:
            print l
            out.append(l)

        print out
        #out = out.split("\n")
        cpu_info['model_name'] = out[0]
        cpu_info['cpu_cores']  = out[1]
        
    if ( platform.system() == 'Linux'):
        CMD = "cat /proc/cpuinfo"
        args = shlex.split(CMD)
        sub_proc = Popen(args, shell=False, stdout=PIPE)
        #subout = sub_proc.stdout.readall()
        #print subout
        out=[]
        for line in sub_proc.stdout:
            if ( line.startswith("model name") ):
                cpu_info['model_name'] = line.split(":")[1]
            elif ( line.startswith("cpu cores") ):
                cpu_info['cpu_cores']=line.split(":")[1]
            elif ( line.startswith("cpu MHz") ):
                cpu_info['cpu_speed']=line.split(":")[1]

    print 'cpu info: %s' % cpu_info 
    
    xml=""
    for name, val in cpu_info.items():
        xml += "<%s>%s</%s>" % (name, val, name)
    provenance_xml += "<CPUInfo>%s</CPUInfo>" % xml
    
    return cpu_info

'''
provide information about the process running over the execution machine
<processinfo></processinfo>
'''
def get_process_info(process_name):
    global provenance_xml

    print 'looking for %s' % process_name
    pid = get_process_id(process_name)
    
    proc = get_proc_info(pid)

    xml = "<pID>%s</pID>" % pid
    #xml += '<pName>%s</pName>' % proc.name
    xml += '<pExe>%s</pExe>' %  proc.cmd
    #xml += '<pCWD>%s</pCWD>' % p.getcwd
    xml += '<pCMDline>%s</pCMDline>' % proc.extended_cmd
    xml += '<pUsername>%s</pUsername>' % proc.user
    xml += '<pCreateTime>%s</pCreateTime>' % proc.start
    xml += '<pCPUTimes>%s</pCPUTimes>' % str( proc.cpu )
    xml += '<pCPUPercent>%s</pCPUPercent>' % str( proc.stat )
    #xml += '<pOpenfiles>%s</pOpenfiles>' % get_proc_open_files(pid)
    #following require sudo access
    #print p.get_memory_maps()
    provenance_xml += "<Process>%s</Process>" % xml
    return pid
    
def get_process_id(process_name):
    cmd = "ps -ef | grep %s | grep -v 'grep' | grep -v 'Monitor.py' | awk '{print $2}'" % process_name
    #print cmd
    g = os.popen(cmd)
    proc_found = g.readlines()
    g.close()
    print proc_found
    pid = proc_found[0].replace("\n","")
    return pid

def get_proc_info(pid):
    CMD = "ps -p %s" % pid
    #get_proc_open_files(pid)
    sub_proc = Popen(['ps', 'aux' ], shell=False, stdout=PIPE)
    sub_proc.stdout.readline()
    for line in sub_proc.stdout:
        proc_info = split(" *", line.strip())
        proc = Proc(proc_info)
        if ( proc.pid == pid ):
            print 'FOUND proc %s ' % proc.to_str()
            print proc_info
            return proc

def get_proc_open_files(pid):
    CMD = 'lsof -w -p %s' % pid
    print CMD
    args = shlex.split(CMD)
    sub_proc = Popen( args , shell=False, stdout=PIPE)
    sub_proc.stdout.readline()
    opened_files = []
    for line in sub_proc.stdout:
        info = split(" *", line.strip())
        #print info[-1]
        opened_files.append(info[-1])
    print opened_files
    return "\n".join(opened_files)

def get_proc_list():
    ''' Retrieves a list [] of Proc objects representing the active
    process list list '''
    proc_list = []
    sub_proc = Popen(['ps', 'aux'], shell=False, stdout=PIPE)
    #Discard the first line (ps aux header)
    sub_proc.stdout.readline()
    for line in sub_proc.stdout:
        #The separator for splitting is 'variable number of spaces'
        proc_info = split(" *", line.strip())
        proc_list.append(Proc(proc_info))
    return proc_list

def get_process_cpu_count(pid, cpu_info):
    print 'processing %s for cpu count' % pid
    proc_file="/proc/%s/stat" % pid
    pf = open(proc_file,"r")
    line = pf.read()
    #print line
    parts = line.split(" ")
    
    print "utime:%s" % parts[14]
    print "stime:%s" % parts[15]
    print "cutime:%s" % parts[16]
    print "cstime:%s" % parts[17]
    print "proc starttime:%s" % parts[22]
    
    starttime = long(parts[22])
    current_time = time.time()
    td = timedelta(seconds = starttime)
    print str(td)
    uptime = float( get_system_uptime() )
    
    print "system uptime: %s" % uptime
    
    total_time = int(parts[14]) + int(parts[15])
    total_time = total_time + int(parts[16]) + int(parts[17])
    
    cpu_speed_hertz = float(cpu_info['cpu_speed']) * math.pow(10, 6)
    print 'cpu speed hertz: %s' % cpu_speed_hertz
    
    '''uptime- (starttime/Hertz)'''
    process_elapsed_seconds = uptime -  ( int(parts[22]) / cpu_speed_hertz  )
    
    #processor_count = total_time/process_elapsed_seconds
    processor_count = total_time/(current_time - starttime)
    print 'cpu used: %s' % processor_count
    
def get_system_uptime():
    uptime = "/proc/uptime"
    f = open(uptime, "r")
    line = f.read()
    #print line
    line = line.split(" ")
    return line[0]

def writeXML_toFile():
    global provenance_xml

    import xml.dom.minidom
    xml = xml.dom.minidom.parseString( provenance_xml )
    f = open("monitor.xml", "w")
    f.write(xml.toprettyxml())
    f.close()

def start_monitor():
    global provenance_xml
    get_sys_info()
    cpu_info = get_cpu_info()
    pid = get_process_info(sys.argv[1])
    #get_process_cpu_count(pid, cpu_info)
    provenance_xml += "</MonitoringInfo>"

    writeXML_toFile()

start_monitor()
#get_proc_info('69090')
