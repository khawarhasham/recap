import threading
import time
from datetime import datetime
import logging
from pprint import pprint

from multilayer.cloud.CloudVirtLayer import CloudVirtLayer
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults

import multilayer.util.Util as Util


__author__ = 'khawar'

class VirtLayerMonitor(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.virtlayer = CloudVirtLayer()
        self.poll_interval = 10
        self.STOP = False
        self.dbconn = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
        self.targetdb = self.dbconn.get_connection()
        self.LOG = logging.getLogger("provlog")

    def run(self):
        self.LOG.debug ('starting monitoring cloud virtual layer')
        flavors = self.virtlayer.get_flavors()
        while ( not self.STOP ):
            vm_nodes = self.virtlayer.get_nodes()
            images =  self.virtlayer.get_images()
            self.virtlayer.get_flavors()

            hostrecord = {}
            vms_info=[]
            # also generate a check SQL that would be used for making an insertion decision
            check_SQL = """SELECT hostip,hostname,flavorid,flavorname,minRAM,minHD,
                           vCPU,image_name,image_id,middleware,created
                            FROM WfCloudVMs where created in ("""
            where=""
            for vm in vm_nodes:
                vm_flavor = Util.get_flavor(vm.extra['flavorId'], self.virtlayer.flavors)
                vm_img = self.virtlayer._get_image(vm.extra['imageId'], False)
                vm_ip = vm.private_ips[0]
                #print vm.extra
                # print vm_ip
                # sample 2014-11-26T17:03:19Z
                vm_created = datetime.strptime(vm.extra['created'], '%Y-%m-%dT%H:%M:%SZ')
                hostrecord={'hostip': vm_ip,
                            'hostname':vm.name,
                'flavorid': vm.extra['flavorId'],
                'flavorname': vm_flavor.name,
                'minRAM': vm_flavor.ram,
                'minHD': vm_flavor.disk,
                'vCPU': vm_flavor.vcpus,
                'image_name': vm_img.name,
                'image_id': vm.extra['imageId'],
                'middleware': 'OpenStack',
                'created':vm_created}
                check_SQL="%s '%s',"%(check_SQL, vm_created)
                vms_info.append(hostrecord)

            check_SQL="%s)" % check_SQL[:-1]
            self.update_vmlist(vms_info, check_SQL)
            print 'sleeping'
            time.sleep(self.poll_interval)

    def update_vmlist(self, vms_info, check_SQL):
        #print check_SQL

        db_list = self.dbconn.execute_query({'sql': check_SQL})
        '''# using set difference
        vm_records = db_list.fetchall()
        print vm_records
        #new_vms= set(vms_info) - set(vms_info)
        #print new_vms
        '''
        for row in db_list:
            created = row['created']
            for vm in vms_info:
                if vm['created']==created:
                    #print 'ignore %s' % vm['hostname']
                    vms_info.remove(vm)

        #print "new VMs: %s" % new_vms
        print 'new VMs: %s' % vms_info
        if len( vms_info) > 0:
            self.dbconn.insert_record({'conn':self.targetdb,
                                   'table_name':'WfCloudVMs',
                                   'insert_values': vms_info})
            print 'new VMs are inserted'
        else:
            print 'nothing to update'



if __name__=='__main__':
    monitor = VirtLayerMonitor()
    monitor.start()

    s = "2014-11-26T17:03:19Z"
    #print datetime.strptime(s, '%Y-%m-%dT%H:%M:%S%Z')
    s = "2014-11-26T17:03:19Z"
    print datetime.strptime(s, '%Y-%m-%dT%H:%M:%SZ')