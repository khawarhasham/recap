from multilayer.parser.PegasusParser import PegasusParser
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults

import networkx as nx
import networkx.algorithms as algo
import networkx.algorithms.isomorphism as iso
import networkx.algorithms.operators as ops

from py2neo import neo4j, rel
import logging
from pprint import pprint
import json
import time
import sys
import re

__author__ = 'khawar'
logging.disable(logging.CRITICAL)

dbURI = 'http://localhost:7474/db/data/'
#graph_db = neo4j.Graph()
mysqlobj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
neoapi = PersistencyFactory.getPersistentObj("Neo4jAPI", {'dburi':dbURI})
graph_db = neoapi.get_connection()
#mysqlobj.get_connection()
print(graph_db.neo4j_version)
READS = 0
WRITES = 0
Graph1_EdgeCount = 0
Graph2_EdgeCount = 0
EdgeComparison   = 0
NodeComparison   = 0
EdgeWeightCountAtCreation = 0

edge_weights={'has_task':1.0, 'consumes': 2.0, 'produces':2.0, 'executed_on':4.0}
vm_names = {}
def disable_logs2():
    for logname in logging.Logger.manager.loggerDict.keys():
        if logname.find("py2neo")==0:
            logger = logging.getLogger(logname)
            logger.setLevel(logging.WARNING)

def disable_logs():
    neo4j_http_log = logging.getLogger("py2neo.packages.httpstream.http")
    neo4j_http_log.setLevel(logging.WARNING)

    neo4j_log = logging.getLogger("py2neo.neo4j")
    neo4j_log.setLevel(logging.WARNING)

    neo4j_http_log2 = logging.getLogger("httpstream")
    neo4j_http_log2.setLevel(logging.WARNING)
    neo4j_core_log = logging.getLogger("py2neo.packages.core")
    neo4j_core_log.setLevel(logging.WARNING)

    #neo4j_log2 = logging.getLogger("py2neo.packages.httpstream")
    print logging.Logger.manager.loggerDict

def clear_db():
    graph_db.clear()
    print 'db size: %s' % graph_db.order

def createGraphNode(node_details, label):
    node = graph_db.insert_record({'c':'node', 'values':[node_details, label]})
    return node

def creatWorkflowJobRel(wfnode, jobnode):
    pass

def createJobRel(jobnode1, jobnode2, rel_name):
    global EdgeWeightCountAtCreation
    props = {'weight':0.5}
    if ( rel_name == 'executed_on'):
        props = {'weight':3}
    EdgeWeightCountAtCreation = EdgeWeightCountAtCreation + props['weight']

    #rel = jobnode1.create_path((rel_name, props), jobnode2)
    rel = neoapi.insert_record({'c':'rel', 'values':[jobnode1, jobnode2, rel_name, props]})

def createJobOutputRel(jobnode, output, rel_name):
    global EdgeWeightCountAtCreation

    if ( rel_name == 'consumes' ):
        props = {'weight': 1}
    elif ( rel_name == 'produces' ):
        props = {'weight': 2}
    EdgeWeightCountAtCreation = EdgeWeightCountAtCreation + props['weight']
    #rel = jobnode.create_path(("produces",props), output)
    rel = jobnode.create_path((rel_name,props), output)

def createJobInputRel(jobnode, input):
    props = { }
    rel = jobnode.create_path(("consumes",props), input)

def createWFNode(wfid):
    SQL="""SELECT * from workflow where wf_id=%s""" % wfid

    result = mysqlobj.execute_query({'sql': SQL})
    print(result.rowcount)
    wfoutput = result.fetchone()

    wfnode = {}
    for key, value in wfoutput.items():
        wfnode[key] = value
    wfnode['name']='workflow'
    wfnode['type']='workflow'
    wfnode['wfid']=wfid
    print wfnode

    return createGraphNode(wfnode, "workflow")

def getworkflow_job_hasParent(wfid, jobname):
    SQL="""SELECT * from job_edge
        where child_exec_job_id='%s' and wf_id=%s
        """ % (jobname, wfid)
    result = mysqlobj.execute_query({'sql': SQL})
    one = result.fetchone()
    if ( one ):
        return True

    return False

def get_workflownode(wfid):
    cypher_sql = """MATCH (n:workflow)
                 where n.wfid=%s
                 return n
                 """ % wfid
    #query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = graph_db.cypher.execute(cypher_sql)
    if ( len(result) == 1 ):
        wfnode = result[0][0]
        return wfnode

    return None

def getworkflow_job_edges(wfid):
    SQL="""SELECT parent_exec_job_id, child_exec_job_id from job_edge
        where wf_id=%s order by parent_exec_job_id
        """ % (wfid)

    result = mysqlobj.execute_query({'sql': SQL})
    for edge in result:
        parent = edge['parent_exec_job_id']
        child = edge['child_exec_job_id']

        parent_cypher_sql = """START n=node(*)
                     where n.name ='%s' and n.wfid =%s
                     return n limit 1
                     """ % (parent, wfid)

        cypher_sql = """START n=node(*)
                     where n.name ='%s' and n.wfid =%s
                     return n limit 1
                     """ % (child, wfid)

        #child_query = neo4j.CypherQuery(graph_db, cypher_sql)
        #result = child_query.execute()

        #parent_query = neo4j.CypherQuery(graph_db, parent_cypher_sql)
        #result2 = parent_query.execute()
        result = graph_db.cypher.execute(cypher_sql)
        result2 = graph_db.cypher.execute(parent_cypher_sql)

        child_node=None
        parent_node=None

        if ( len(result) == 1 ):
            child_node = result[0][0]
        if ( len(result2) == 1):
            parent_node = result2[0][0]

        if ( (child_node is not None) and (parent_node is not None) ):
            print '%s -> %s mapping' % (parent, child)
            createJobRel(parent_node, child_node, "has_task")
        else:
            pass
            #print 'no edge created for %s %s' % (parent, child)

def getworkflow_job_host(wfid, jobid):
    #for cloudprov db: use WFCloudMapping instead of VirtualLayerProvenance
    SQL="""SELECT nodename as name, flavorid, minRAM, minHD, vCPU,image_id, image_name
        From cloudprov.WfCloudMapping
        Where wfID in (select wfid from cloudprov.WorkflowSource where peg_wfid=%s) and jobid=%s
        """ % (wfid, jobid)

    result = mysqlobj.execute_query({'sql': SQL})
    jobhost = result.fetchone()

    if ( jobhost is None):
        return None

    host = {'wfid':wfid}
    for key, value in jobhost.items():
        host[key] = value

    if ( len ( host.keys() ) > 2):
        #print 'creating host node in graph'
        return createGraphNode(host, "host")

    return None

def getworkflow_output(wfid):
    parser = PegasusParser()
    wfjobs = parser.get_joboutputs(wfid)
    return wfjobs

'''
Select inv.argv, inv.abs_task_id
From invocation as inv, job_instance as inst
Where inst.job_instance_id=inv.job_instance_id and inv.task_submit_seq=1
and (inst.job_id in (select job_id from job where wf_id=131)) and inv.abs_task_id<>'NULL'
'''
def getjob_invocation(wfid, job_inst_id, jobname):

    SQL="""SELECT argv
        FROM `invocation`
        WHERE job_instance_id=%s and task_submit_seq=1 and wf_id=%s
        """ % ( job_inst_id, wfid )

    result = mysqlobj.execute_query({'sql': SQL})
    #print result.rowcount
    one = result.fetchone()
    if ( one ):
        argv = one['argv']
        if ( argv == None):
            return None

        pattern = r"-i (.+) -o (.+)"
        m = re.search(pattern, argv)
        if ( m ):
            inputs = m.group(1)
            inputs = inputs.split(" ")
            outputs = m.group(2)
            outputs = outputs.split(" ")
            return [ argv, inputs, outputs ]

    return None

def getfile_input(filename, wfid):
    file_cypher_sql = """MATCH (n:File)
                     where n.name ="%s" and n.wfid=%s
                     return n
                     """ % (filename, wfid)
    #query = neo4j.CypherQuery(graph_db, file_cypher_sql)
    #result = query.execute()
    result = graph_db.cypher.execute(file_cypher_sql)
    if ( len(result) >= 1 ):
        filenode = result[0][0]
        return filenode

    return None

def getjob_states(job_inst_id):
    SQL="""SELECT state, timestamp from jobstate
           WHERE job_instance_id=%s order by jobstate_submit_seq
        """ % job_inst_id
    result = mysqlobj.execute_query({'sql': SQL})
    #print result.rowcount
    states={'start':-9, 'finish':-9}
    for state in result:
        if ( state['state']=='EXECUTE'):
            states['start']=state['timestamp']
        elif ( state['state']=='POST_SCRIPT_SUCCESS'):
            states['finish']=state['timestamp']

    return states

def getworkflow_job_instances(wfid, wfnode):
    global EdgeWeightCountAtCreation
    SQL="""SELECT job_instance_id, job.job_id, exec_job_id, argv, job.type_desc
        FROM `job_instance` left join job on job.job_id=job_instance.job_id
        WHERE wf_id=%s
        order by job_submit_seq
        """ % ( wfid )

    result = mysqlobj.execute_query({'sql': SQL})
    print result.rowcount
    files = []
    for row in result:
        jobname = row['exec_job_id']
        jobid = row['job_id']
        job_inst_id = row['job_instance_id']
        argv = row['argv']
        job_type = row['type_desc']

        inv_args = getjob_invocation(wfid, job_inst_id, jobname)
        job_states = getjob_states(job_inst_id)
        #TODO: add timing details later
        job_args = inv_args[0] if ( inv_args ) else ''
        job = {'name': jobname, 'wfid': wfid, 'job_id':job_inst_id, 'argv': argv, 'inv_args': job_args, 'type':'task', 'task_type':job_type}
        job.update(job_states)
        print job
        jobnode = createGraphNode(job, "job")

        #if no parents for this job. add this to workflow node
        if ( not getworkflow_job_hasParent(wfid, jobname) ):
            createJobRel(wfnode, jobnode, "has_task")

        if ( inv_args ):
            inputs = inv_args[1]
            outputs = inv_args[2]

            for inp in inputs:
                job_input = None
                if ( inp not in files ):
                    job_input = createGraphNode({'name':inp, 'type': 'file', 'wfid':wfid}, "File")
                    files.append(inp)
                else:
                    job_input = getfile_input(inp, wfid)

                if ( job_input ):
                    createJobOutputRel(jobnode, job_input, "consumes")

            for out in outputs:
                job_output = None
                if ( out not in files ):
                    job_output = createGraphNode({'name':out, 'type':'file', 'wfid':wfid}, "File")
                    files.append(out)
                else:
                    job_output = getfile_input(out, wfid)

                if ( job_output ):
                    createJobOutputRel(jobnode, job_output, "produces")

        #get host used for execution the job
        hostnode = getworkflow_job_host(wfid, jobid)
        if ( hostnode ):
            createJobRel(jobnode, hostnode, "executed_on")

    print 'Edge weight at Creation: %s' % EdgeWeightCountAtCreation
    EdgeWeightCountAtCreation = 0

def createWorkflowProvGraph(wfid):
    #clear_db()
    wfnode = createWFNode(wfid)
    getworkflow_job_instances(wfid, wfnode)
    getworkflow_job_edges(wfid)

def compare_hostnodes(host1, host2):
    attr_list=['image_id', 'vCPU', 'minRAM', 'minHD', 'flavorid'] #, 'image_name']
    node_weight = 0
    status = True

    ''' don't compare two host names because they are not significant
    in terms of job performance or acquiring a new resource in cloud'''

    #if ( host1['name'] != host2['name']):
    #    node_weight = 0

    for attr in attr_list:
        if ( host1[ attr ] == host2[ attr ]):
            continue
        node_weight+=1
        print attr, host1[attr], host2[attr], ' do not match'

    if ( node_weight > 0):
        status = False
    print host1, host2
    print 'host comparison : %s' % status
    return status


def analysis_job_host(wfid):
    print '\n analysis_job_host'
    cypher_sql = """START n=node(*)
                 MATCH n-[:executed_on]->m
                 where n.wfid=%s and m.wfid=%s
                 return n, m
                 """ % (wfid, wfid)

    query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = query.execute()

    for node in result:
        job = node['n']
        host =  node['m']
        print job,' executed on ', host

def analyse_graph(pydot_graph):
    import networkx as nx
    nx_G = nx.from_pydot(pydot_graph)
    G = nx.DiGraph(nx_G)
    print G.nodes()
    print 'Nodes: %s' % G.number_of_nodes()
    print G.edges()
    print '#Edges: %s' % G.number_of_edges()
    #for edge in G.edges_iter():
    #    print edge
    #    print G.get_edge_data(edge[0], edge[1])
    print nx.single_source_dijkstra_path(G, "workflow")

class WFProvGraphGenerator(object):
    def __init__(self, wfid):
        self.wfid = wfid

    def traverseProvGraph(self):
        cypher_sql = """START n=node(*)
                 where n.root_wf_id =%s
                 return n limit 1
                 """ % self.wfid

        #query = neo4j.CypherQuery(graph_db, cypher_sql)
        result = graph_db.cypher.execute(cypher_sql)
        wfrootNode=None
        if ( len(result) == 1 ):
            wfrootNode = result[0][0]
            print wfrootNode

        if ( wfrootNode ):
            traverser = wfrootNode.match(rel_type="has_task")
            print traverser
            for node in traverser:
                print node

    def getworkflows_nodes(sefl):
        cypher_sql = """START n=node(*)
                 MATCH n-[r]->m
                 return n,r, m
                 """
        #query = neo4j.CypherQuery(graph_db, cypher_sql)
        result = graph_db.cypher.execute(cypher_sql)
        #node['n'].update_properties({'name':'workflow'})
        for node in result:
            print "node: %s, %s node: %s" % (node['n']['name'], node['r'], node['m']['name'])

    def drawgraph_workflow(self, EXPORT=False):
        print '\ngenerating pydot graph fron neo4j data'
        import pydot
        graph = pydot.Dot(graph_type="digraph")

        cypher_sql = """START n=node(*)
                 MATCH n-[r]->m
                 where n.wfid=%s and m.wfid=%s
                 return n, r, m
                 """ % (self.wfid, self.wfid)
        #query = neo4j.CypherQuery(graph_db, cypher_sql)
        result = graph_db.cypher.execute(cypher_sql)
        #node['n'].update_properties({'name':'workflow'})

        traversed_nodes = []
        edge_weight_count = 0
        for node in result:
            rel = node['r']
            start = node['n']['name']
            end =  node['m']['name']

            #process the names and remove condorpool or local
            start = self.cleanse_name(start)
            end = self.cleanse_name(end)
            #print '%s => %s' % (type(node['m']), node['m'])

            fill_color="green"
            fill_color2="green"

            shape = "ellipse"  #default shape
            edge_style="solid"
            SWAPPED=False
            #print 'node: "%s", %s node: "%s"' % (start, rel.type, end)
            if (start=='workflow'):
                fill_color="gray"

            if ( rel.type == 'executed_on'):
                #end = node['m']['nodename']
                fill_color2="red"
                shape="box"
                edge_style="bold"

            elif (rel.type == 'consumes'):  #swap direction
                print start, end
                tmp = end
                end = start
                start = tmp
                SWAPPED = True

            if ( node['m']['type'] == 'file'):
                fill_color2="cyan"
                shape="triangle"
                edge_style="dotted"

            if ( start not in traversed_nodes ):
                #print "START:", start, fill_color, shape
                if ( SWAPPED ): fill_color = fill_color2
                node_tmp = pydot.Node('"%s"'%start, shape=shape, style="filled", fillcolor = fill_color )
                graph.add_node( node_tmp )
                traversed_nodes.append( start )

            if ( end not in traversed_nodes ):
                #print "END:", end, fill_color, shape
                node_tmp = pydot.Node('"%s"'%end, shape=shape, style="filled", fillcolor = fill_color2)
                graph.add_node( node_tmp )
                traversed_nodes.append( end )

            #if ( rel.type == 'consumes' or rel.type=='produces'):
            #    print start, end, rel.type
            edge_weight_count += edge_weights[rel.type]
            #add edges
            #graph.add_edge( pydot.Edge(start, end, label=rel.type, style=edge_style, fillcolor=fill_color2, weight=edge_weights[rel.type]) )
            graph.add_edge( pydot.Edge(start, end, style=edge_style, fillcolor=fill_color2, weight=edge_weights[rel.type]) )
        print '\nEdge Weights COUNT : %s' % edge_weight_count

        #for legends in output
        legends = pydot.Cluster(graph_name="Legends", label="Legend")
        legends.add_node(pydot.Node("Job",shape="ellipse",  fillcolor="green", style="filled"))
        legends.add_node(pydot.Node("VM", shape="box",  fillcolor="red", style="filled"))
        legends.add_node(pydot.Node("Data", shape="triangle",  fillcolor="cyan", style="filled"))
        legends.add_edge(pydot.Edge("Job", "VM", label="executed_on", style="bold",  fillcolor="red"))
        legends.add_edge(pydot.Edge("Job", "Job", label="has_task", style="solid",  fillcolor="green"))
        legends.add_edge(pydot.Edge("Job", "Data", label="produces", style="dotted",  fillcolor="cyan"))
        legends.add_edge(pydot.Edge("Data", "Job", label="consumes", style="dotted",  fillcolor="cyan"))
        graph.add_subgraph(legends)
        if ( EXPORT ):
            graph.write("./test%s.pdf" % wfid, format="pdf")

    def cleanse_name(self, iname):
        iname = iname.replace("condorpool_level","")
        iname = iname.replace("condorpool","")
        iname = iname.replace("local","")
        iname = iname.replace("_","")
        return iname

    def getWorkflowProvGraph(self):
        print '\ngenerating pydot graph fron neo4j data'
        import pydot
        graph = pydot.Dot(graph_type="digraph")

        cypher_sql = """START n=node(*)
                 MATCH n-[r]->m
                 where n.wfid=%s and m.wfid=%s
                 return n, r, m
                 """ % (self.wfid, self.wfid)
        #query = neo4j.CypherQuery(graph_db, cypher_sql)
        result = graph_db.cypher.execute(cypher_sql)
        #node['n'].update_properties({'name':'workflow'})

        traversed_nodes = []
        edge_weight_count = 0
        vm_counter=1
        vm_names = {}
        for node in result:
            rel = node['r']
            start = node['n']['name']
            end =  node['m']['name']
            #print '\n\n %s => %s\n\n' % (type(node['m']), node['m'])

            fill_color="green"
            fill_color2="green"

            #print "node: %s, %s node: %s" % (start, rel.type, end)
            if (start=='workflow'):
                fill_color="gray"

            if ( rel.type == 'executed_on'):
                #end = node['m']['nodename']
                fill_color2="red"
                if (vm_names.has_key(end)):
                    pass
                else:
                    vm_names[end]=vm_counter
                    vm_counter = vm_counter+1
                end="VM%s"% vm_names[end]
                #print 'VM found:%s' % end

            if ( node['m']['type'] == 'file'):
                fill_color2="cyan"

            if ( start not in traversed_nodes ):
                node_tmp = pydot.Node(start, style="filled", fillcolor = fill_color, nodename=start, nodeattr=node['n'].get_properties() )
                graph.add_node( node_tmp )
                traversed_nodes.append( start )

            if ( end not in traversed_nodes ):
                node_tmp = pydot.Node(end, style="filled", fillcolor = fill_color2, nodename=end, nodeattr=node['m'].get_properties() )
                graph.add_node( node_tmp )
                traversed_nodes.append( end )

            #if ( rel.type == 'consumes' or rel.type=='produces'):
            #    print start, end, rel.type
            edge_weight_count += edge_weights[rel.type]
            #add edges
            graph.add_edge( pydot.Edge(start, end, label=rel.type, fillcolor=fill_color2, weight=edge_weights[rel.type], nodenames=[start, end]) )

        #print '\nEdge Weights COUNT : %s' % edge_weight_count
        print 'VMs found %s' % vm_names
        return graph



if __name__ == '__main__':
    disable_logs()

    #print getjob_invocation(131, 1838,'')
    st = time.time()
    #createWorkflowProvGraph(533)
    print time.time()-st
    #createWorkflowProvGraph(134)

    #traverseProvGraph(131)

    #pydot_graph = drawgraph_workflow(161, True)
    #analyse_graph(pydot_graph)

    #compare_graphs(132, 161)

    #analysis_job_host(132)
    #analysis_job_host(131)
    #print getworkflow_job_hasParent(130, 'create_dir_WordCountWF_0_condorpool')
    obj = WFProvGraphGenerator(132)
    obj.getWorkflowProvGraph()
