from multilayer.cloud.CloudStorageProv import CloudStorageLayer
__author__ = 'khawar'

class CloudFileComparison(object):
    def __init__(self, source_wf, source_container, source, dest_wf, dest_container, dest):
        self.source_wf = source_wf
        self.source = source
        self.source_container = source_container
        self.dest_wf = dest_wf
        self.dest = dest
        self.dest_container = dest_container
        self.cloudStore = CloudStorageLayer()

    def compare(self):
        sourcefile = self.cloudStore.get_cloudfile(self.source_container, self.source)
        destfile = self.cloudStore.get_cloudfile(self.dest_container, self.dest)

        if sourcefile.hash == destfile.hash:
            #print 'both files are same'
            print "%s\t%s\t%s\t%s" % (self.source_wf, self.source_container, self.source, sourcefile.hash)
            print "%s\t%s\t%s\t%s" % (self.dest_wf, self.dest_container, self.dest, destfile.hash)
            return True
        else:
            print 'files are different'

        return False


if __name__=='__main__':
    file_comp = CloudFileComparison('wfoutput29042014215119', 'merge_output', 'wfoutput30042014115628','analysis2')
    file_comp.compare()


