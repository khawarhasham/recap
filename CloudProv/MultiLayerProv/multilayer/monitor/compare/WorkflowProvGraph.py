from WorkflowProvGraphGenerator import WFProvGraphGenerator

from multilayer.parser.PegasusParser import PegasusParser
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
from kwms.DAGParser import DAGParser

import networkx as nx
import networkx.algorithms as algo
import networkx.algorithms.isomorphism as iso
import networkx.algorithms.operators as ops

from py2neo import neo4j, rel
import logging
from pprint import pprint
import json
import time
import tempfile
import sys
import re

__author__ = 'khawar'

dbURI = 'http://localhost:7474/db/data/'
#graph_db = neo4j.Graph()
mysqlobj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
neoapi = PersistencyFactory.getPersistentObj("Neo4jAPI", {'dburi':dbURI})
graph_db = neoapi.get_connection()
dagparser = None

#mysqlobj.get_connection()
print(graph_db.neo4j_version)
READS = 0
WRITES = 0
Graph1_EdgeCount = 0
Graph2_EdgeCount = 0
EdgeComparison   = 0
NodeComparison   = 0
EdgeWeightCountAtCreation = 0

edge_weights={'has_task':1.0, 'consumes': 2.0, 'produces':2.0, 'executed_on':4.0}
task_edges = {}
in_edges   = {}
out_edges  = {}
host_edges = {}
vm_names = {}

def populate_dagparser(recapid):
    global dagparser
    SQL="""SELECT wfDAG from WorkflowSource where wfID=%s""" % recapid
    conn = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
    result = conn.execute_query({'sql': SQL})
    record = result.fetchone()

    if ( record ):
        dag_data = record['wfDAG']
        dag_file = tempfile.NamedTemporaryFile(suffix='.xml', delete=False)
        dag_file.write(dag_data)
        dag_file.close()
        #instantiate the dagparser
        dagparser = DAGParser(dag_file.name)
        dagparser.get_jobs()
        print 'DagParser is initialized with %s' % dag_file.name
    #close this DB object
    conn.check_and_close()

def disable_logs():
    neo4j_http_log = logging.getLogger("py2neo.packages.httpstream.http")
    neo4j_http_log.setLevel(logging.WARNING)

    neo4j_log = logging.getLogger("py2neo.neo4j")
    neo4j_log.setLevel(logging.WARNING)

    neo4j_http_log2 = logging.getLogger("httpstream")
    neo4j_http_log2.setLevel(logging.WARNING)
    neo4j_core_log = logging.getLogger("py2neo.packages.core")
    neo4j_core_log.setLevel(logging.WARNING)

    #neo4j_log2 = logging.getLogger("py2neo.packages.httpstream")
    print logging.Logger.manager.loggerDict

def disable_logs2():
    for logname in logging.Logger.manager.loggerDict.keys():
        if logname.find("py2neo"):
            logger = logging.getLogger(logname)
            logger.setLevel(logging.WARNING)

def clear_db():
    graph_db.clear()
    print 'db size: %s' % graph_db.order

def createGraphNode(node_details, label):
    #node = graph_db.insert_record({'c':'node', 'values':[node_details, label]})
    node = neoapi.insert_record({'c':'node', 'values':[node_details, label]})
    return node

def creatWorkflowJobRel(wfnode, jobnode):
    pass

def createJobRel(jobnode1, jobnode2, rel_name):
    global EdgeWeightCountAtCreation
    props = {'weight':0.5}
    if ( rel_name == 'executed_on'):
        props = {'weight':3}
    EdgeWeightCountAtCreation = EdgeWeightCountAtCreation + props['weight']

    #rel = jobnode1.create_path((rel_name, props), jobnode2)
    rel = neoapi.insert_record({'c':'rel', 'values':[jobnode1, jobnode2, rel_name, props]})

def createJobOutputRel(jobnode, output, rel_name):
    global EdgeWeightCountAtCreation

    if ( rel_name == 'consumes' ):
        props = {'weight': 1}
    elif ( rel_name == 'produces' ):
        props = {'weight': 2}
    EdgeWeightCountAtCreation = EdgeWeightCountAtCreation + props['weight']
    #rel = jobnode.create_path(("produces",props), output)
    rel = jobnode.create_path((rel_name,props), output)

def createJobInputRel(jobnode, input):
    props = { }
    rel = jobnode.create_path(("consumes",props), input)

def createWFNode(wfid):
    SQL="""SELECT * from workflow where wf_id=%s""" % wfid

    result = mysqlobj.execute_query({'sql': SQL})
    print(result.rowcount)
    wfoutput = result.fetchone()

    wfnode = {}
    for key, value in wfoutput.items():
        wfnode[key] = value
    wfnode['name']='workflow'
    wfnode['type']='workflow'
    wfnode['wfid']=wfid
    print wfnode

    return createGraphNode(wfnode, "workflow")

def getworkflow_job_hasParent(wfid, jobname):
    SQL="""SELECT * from job_edge
        where child_exec_job_id='%s' and wf_id=%s
        """ % (jobname, wfid)
    result = mysqlobj.execute_query({'sql': SQL})
    one = result.fetchone()
    if ( one ):
        return True

    return False

def get_workflownode(wfid):
    cypher_sql = """MATCH (n:workflow)
                 where n.wfid=%s
                 return n
                 """ % wfid
    #query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = graph_db.cypher.execute(cypher_sql)
    if ( len(result) == 1 ):
        wfnode = result[0][0]
        return wfnode

    return None

def getworkflow_job_edges(wfid):
    SQL="""SELECT parent_exec_job_id, child_exec_job_id from job_edge
        where wf_id=%s order by parent_exec_job_id
        """ % (wfid)

    result = mysqlobj.execute_query({'sql': SQL})
    for edge in result:
        parent = edge['parent_exec_job_id']
        child = edge['child_exec_job_id']

        parent_cypher_sql = """START n=node(*)
                     where n.name ='%s' and n.wfid =%s
                     return n limit 1
                     """ % (parent, wfid)

        cypher_sql = """START n=node(*)
                     where n.name ='%s' and n.wfid =%s
                     return n limit 1
                     """ % (child, wfid)

        #child_query = neo4j.CypherQuery(graph_db, cypher_sql)
        #result = child_query.execute()

        #parent_query = neo4j.CypherQuery(graph_db, parent_cypher_sql)
        #result2 = parent_query.execute()
        result = graph_db.cypher.execute(cypher_sql)
        result2 = graph_db.cypher.execute(parent_cypher_sql)

        child_node=None
        parent_node=None

        if ( len(result) == 1 ):
            child_node = result[0][0]
        if ( len(result2) == 1):
            parent_node = result2[0][0]

        if ( (child_node is not None) and (parent_node is not None) ):
            print '%s -> %s mapping' % (parent, child)
            createJobRel(parent_node, child_node, "has_task")
        else:
            pass
            #print 'no edge created for %s %s' % (parent, child)

def getworkflow_job_host(wfid, jobid):
    #for cloudprov db: use WFCloudMapping instead of VirtualLayerProvenance
    SQL="""SELECT nodename as name, flavorid, minRAM, minHD, vCPU,image_id, image_name
        From cloudprov.WfCloudMapping
        Where wfID in (select wfid from cloudprov.WorkflowSource where peg_wfid=%s) and jobid=%s
        """ % (wfid, jobid)

    result = mysqlobj.execute_query({'sql': SQL})
    jobhost = result.fetchone()

    if ( jobhost is None):
        return None

    host = {'wfid':wfid}
    for key, value in jobhost.items():
        host[key] = value

    if ( len ( host.keys() ) > 2):
        #print 'creating host node in graph'
        return createGraphNode(host, "host")

    return None

def getworkflow_output(wfid):
    parser = PegasusParser()
    wfjobs = parser.get_joboutputs(wfid)
    return wfjobs

'''
Select inv.argv, inv.abs_task_id
From invocation as inv, job_instance as inst
Where inst.job_instance_id=inv.job_instance_id and inv.task_submit_seq=1
and (inst.job_id in (select job_id from job where wf_id=131)) and inv.abs_task_id<>'NULL'
'''
def getjob_invocation(wfid, job_inst_id, jobname):

    SQL="""SELECT argv
        FROM `invocation`
        WHERE job_instance_id=%s and task_submit_seq=1 and wf_id=%s
        """ % ( job_inst_id, wfid )

    result = mysqlobj.execute_query({'sql': SQL})
    #print result.rowcount
    one = result.fetchone()
    if ( one ):
        argv = one['argv']
        if ( argv == None):
            return None

        pattern = r"-i (.+) -o (.+)"
        m = re.search(pattern, argv)
        if ( m ):
            inputs = m.group(1)
            inputs = inputs.split(" ")
            outputs = m.group(2)
            outputs = outputs.split(" ")
            return [ argv, inputs, outputs ]

    return None

def getfile_input(filename, wfid):
    file_cypher_sql = """MATCH (n:File)
                     where n.name ="%s" and n.wfid=%s
                     return n
                     """ % (filename, wfid)
    #query = neo4j.CypherQuery(graph_db, file_cypher_sql)
    #result = query.execute()
    result = graph_db.cypher.execute(file_cypher_sql)
    if ( len(result) >= 1 ):
        filenode = result[0][0]
        return filenode

    return None

def getjob_states(job_inst_id):
    SQL="""SELECT state, timestamp from jobstate
           WHERE job_instance_id=%s order by jobstate_submit_seq
        """ % job_inst_id
    result = mysqlobj.execute_query({'sql': SQL})
    #print result.rowcount
    states={'start':-9, 'finish':-9}
    for state in result:
        if ( state['state']=='EXECUTE'):
            states['start']=state['timestamp']
        elif ( state['state']=='POST_SCRIPT_SUCCESS'):
            states['finish']=state['timestamp']

    return states

def getworkflow_job_instances(wfid, wfnode):
    global EdgeWeightCountAtCreation, dagparser
    SQL="""SELECT job_instance_id, job.job_id, exec_job_id, argv, job.type_desc
        FROM `job_instance` left join job on job.job_id=job_instance.job_id
        WHERE wf_id=%s
        order by job_submit_seq
        """ % ( wfid )

    result = mysqlobj.execute_query({'sql': SQL})
    print result.rowcount
    files = []
    for row in result:
        jobname = row['exec_job_id']
        jobid = row['job_id']
        job_inst_id = row['job_instance_id']
        argv = row['argv']
        job_type = row['type_desc']

        #inv_args = getjob_invocation(wfid, job_inst_id, jobname)
        #get this variable from the dag
        print dagparser
        inv_args = dagparser.get_job_args(row['exec_job_id'])
        job_states = getjob_states(job_inst_id)
        #TODO: add timing details later
        job_args = inv_args['args'] if ( inv_args ) else ''
        job = {'name': jobname, 'wfid': wfid, 'job_id':job_inst_id, 'argv': argv, 'inv_args': job_args, 'type':'task', 'task_type':job_type}
        job.update(job_states)
        print job
        jobnode = createGraphNode(job, "job")

        #if no parents for this job. add this to workflow node
        if ( not getworkflow_job_hasParent(wfid, jobname) ):
            createJobRel(wfnode, jobnode, "has_task")

        if ( inv_args ):
            print inv_args
            inputs = inv_args['uses']['input'] if inv_args['uses'].has_key('input') else []
            outputs = inv_args['uses']['output'] if inv_args['uses'].has_key('output') else []

            for inp in inputs:
                job_input = None
                if ( inp not in files ):
                    job_input = createGraphNode({'name':inp, 'type': 'file', 'wfid':wfid}, "File")
                    files.append(inp)
                else:
                    job_input = getfile_input(inp, wfid)

                if ( job_input ):
                    createJobOutputRel(jobnode, job_input, "consumes")

            for out in outputs:
                job_output = None
                if ( out not in files ):
                    job_output = createGraphNode({'name':out, 'type':'file', 'wfid':wfid}, "File")
                    files.append(out)
                else:
                    job_output = getfile_input(out, wfid)

                if ( job_output ):
                    createJobOutputRel(jobnode, job_output, "produces")

        #get host used for execution the job
        hostnode = getworkflow_job_host(wfid, jobid)
        if ( hostnode ):
            print 'Creating mapping between job %s with host %s' % (jobnode[0], hostnode[0])
            createJobRel(jobnode, hostnode, "executed_on")
        else:
            print 'No host created for wfid %s jobid %s' % (wfid, jobid)

    print 'Edge weight at Creation: %s' % EdgeWeightCountAtCreation
    EdgeWeightCountAtCreation = 0

def createWorkflowProvGraph(wfid, recapid):
    #get the dagparser
    populate_dagparser(recapid)
    #clear_db()
    wfnode = createWFNode(wfid)
    getworkflow_job_instances(wfid, wfnode)
    getworkflow_job_edges(wfid)

def traverseProvGraph(wfid):
    cypher_sql = """START n=node(*)
                 where n.root_wf_id =%s
                 return n limit 1
                 """ % wfid

    #query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = graph_db.cypher.execute(cypher_sql)
    wfrootNode=None
    if ( len(result) == 1 ):
        wfrootNode = result[0][0]
        print wfrootNode

    if ( wfrootNode ):
        traverser = wfrootNode.match(rel_type="has_task")
        print traverser
        for node in traverser:
            print node

def getworkflows_nodes(wfid):
    cypher_sql = """START n=node(*)
                 MATCH n-[r]->m
                 return n,r, m
                 """
    #query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = graph_db.cypher.execute(cypher_sql)
    #node['n'].update_properties({'name':'workflow'})
    for node in result:
        print "node: %s, %s node: %s" % (node['n']['name'], node['r'], node['m']['name'])

def drawgraph_workflow(wfid, EXPORT=False):
    print '\ngenerating pydot graph fron neo4j data'
    import pydot
    graph = pydot.Dot(graph_type="digraph")

    cypher_sql = """START n=node(*)
                 MATCH n-[r]->m
                 where n.wfid=%s and m.wfid=%s
                 return n, r, m
                 """ % (wfid, wfid)
    #query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = graph_db.cypher.execute(cypher_sql)
    #node['n'].update_properties({'name':'workflow'})

    traversed_nodes = []
    edge_weight_count = 0
    #vm_names ={'uwe-vm9.nova':'uwe-vm4-rep', 'uwe-vm8.nova':'uwe-vm3-rep'}
    vm_names ={'uwe-vm9.nova':'uwe-vm4.nova', 'uwe-vm8.nova':'uwe-vm3.nova'}
    for node in result:
        rel = node['r']
        start = node['n']['name']
        end =  node['m']['name']
        print "Task Type:", node['n']['task_type']
        #process the names and remove condorpool or local
        start = cleanse_name(start)
        end = cleanse_name(end)
        #print '%s => %s' % (type(node['m']), node['m'])

        fill_color="green"
        fill_color2="green"
        '''
        change for the thesis.
        use gray colour for workflow root
        use different colour for the workflow jobs
        use different colour for the jobs created by pegasus
        '''

        shape = "ellipse"  #default shape
        edge_style="solid"
        SWAPPED=False
        #print 'node: "%s", %s node: "%s"' % (start, rel.type, end)
        if (start=='workflow'):
            fill_color="gray"

        if (node['n']['task_type']!='compute' and start!='workflow'):
            print 'Updating task colour to magenta'
            fill_color='magenta'
        if (node['m']['task_type']!='compute'):
            fill_color2='magenta'


        if ( rel.type == 'executed_on'):
            #end = node['m']['nodename']
            fill_color2="red"
            shape="box"
            edge_style="bold"
            #update vm name for 132, 134
            print 'incomng vm name %s' % end
            if (vm_names.has_key(end)):
                end = vm_names[end]
            print 'new vm name: %s' % end

        elif (rel.type == 'consumes'):  #swap direction
            print start, end
            tmp = end
            end = start
            start = tmp
            SWAPPED = True

        if ( node['m']['type'] == 'file'):
            fill_color2="cyan"
            shape="triangle"
            edge_style="dotted"



        if ( start not in traversed_nodes ):
            print "START:", start, fill_color, shape

            if ( SWAPPED ): fill_color = fill_color2
            print fill_color, fill_color2
            node_tmp = pydot.Node('"%s"'%start, shape=shape, style="filled", fillcolor = fill_color )
            graph.add_node( node_tmp )
            traversed_nodes.append( start )

        if ( end not in traversed_nodes ):
            #print "END:", end, fill_color, shape
            node_tmp = pydot.Node('"%s"'%end, shape=shape, style="filled", fillcolor = fill_color2)
            graph.add_node( node_tmp )
            traversed_nodes.append( end )

        #if ( rel.type == 'consumes' or rel.type=='produces'):
        #    print start, end, rel.type
        edge_weight_count += edge_weights[rel.type]
        #add edges
        #graph.add_edge( pydot.Edge(start, end, label=rel.type, style=edge_style, fillcolor=fill_color2, weight=edge_weights[rel.type]) )
        graph.add_edge( pydot.Edge(start, end, style=edge_style, fillcolor=fill_color2, weight=edge_weights[rel.type]) )
    print '\nEdge Weights COUNT : %s' % edge_weight_count

    #for legends in output
    legends = pydot.Cluster(graph_name="Legends", label="Legend")
    legends.add_node(pydot.Node("Workflow Job",shape="ellipse",  fillcolor="green", style="filled"))
    legends.add_node(pydot.Node("Augmented Job",shape="ellipse",  fillcolor="magenta", style="filled"))

    legends.add_node(pydot.Node("VM", shape="box",  fillcolor="red", style="filled"))
    legends.add_node(pydot.Node("Data", shape="triangle",  fillcolor="cyan", style="filled"))

    legends.add_edge(pydot.Edge("Augmented Job", "Workflow Job", label="has_task", style="bold",  fillcolor="green"))
    legends.add_edge(pydot.Edge("Workflow Job", "Augmented Job", label="has_task", style="bold",  fillcolor="magenta"))

    legends.add_edge(pydot.Edge("Workflow Job", "VM", label="executed_on", style="bold",  fillcolor="red"))
    #legends.add_edge(pydot.Edge("Workflow Job", "Workflow Job", label="has_task", style="solid",  fillcolor="green"))
    legends.add_edge(pydot.Edge("Workflow Job", "Data", label="produces", style="dotted",  fillcolor="cyan"))
    legends.add_edge(pydot.Edge("Data", "Workflow Job", label="consumes", style="dotted",  fillcolor="cyan"))
    graph.add_subgraph(legends)
    if ( EXPORT ):
        graph.write("./updateGraphThesis%s.pdf" % wfid, format="pdf")

def cleanse_name(iname):
    iname = iname.replace("condorpool_level","")
    iname = iname.replace("condorpool","")
    iname = iname.replace("local","")
    iname = iname.replace("_","")
    return iname

def getWorkflowProvGraph(wfid):
    print '\ngenerating pydot graph fron neo4j data'
    import pydot
    graph = pydot.Dot(graph_type="digraph")

    cypher_sql = """START n=node(*)
                 MATCH n-[r]->m
                 where n.wfid=%s and m.wfid=%s
                 return n, r, m
                 """ % (wfid, wfid)
    #query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = graph_db.cypher.execute(cypher_sql)
    #node['n'].update_properties({'name':'workflow'})

    traversed_nodes = []
    edge_weight_count = 0
    vm_counter=1
    for node in result:
        rel = node['r']
        start = node['n']['name']
        end =  node['m']['name']
        #print '\n\n %s => %s\n\n' % (type(node['m']), node['m'])

        fill_color="green"
        fill_color2="green"

        #print "node: %s, %s node: %s" % (start, rel.type, end)
        if (start=='workflow'):
            fill_color="gray"

        if ( rel.type == 'executed_on'):
            #end = node['m']['nodename']
            fill_color2="red"
            if (vm_names.has_key(end)):
                pass
            else:
                vm_names[end]=vm_counter
                vm_counter = vm_counter+1
            end="VM%s"% vm_names[end]
            print 'VM found:%s' % end

        if ( node['m']['type'] == 'file'):
            fill_color2="cyan"

        if ( start not in traversed_nodes ):
            node_tmp = pydot.Node(start, style="filled", fillcolor = fill_color, nodename=start, nodeattr=node['n'].get_properties() )
            graph.add_node( node_tmp )
            traversed_nodes.append( start )

        if ( end not in traversed_nodes ):
            node_tmp = pydot.Node(end, style="filled", fillcolor = fill_color2, nodename=end, nodeattr=node['m'].get_properties() )
            graph.add_node( node_tmp )
            traversed_nodes.append( end )

        #if ( rel.type == 'consumes' or rel.type=='produces'):
        #    print start, end, rel.type
        edge_weight_count += edge_weights[rel.type]
        #add edges
        graph.add_edge( pydot.Edge(start, end, label=rel.type, fillcolor=fill_color2, weight=edge_weights[rel.type], nodenames=[start, end]) )

    print '\nEdge Weights COUNT : %s' % edge_weight_count
    return graph



def process_edges(wfid, edges):
    for edge in edges:
        edge_label = edge[2]['label']
        if edge_label == 'consumes':
            in_edges[wfid].append([edge[0], edge[1]])
        elif edge_label == 'produces':
            out_edges[wfid].append([edge[0], edge[1]])
        elif edge_label == 'executed_on':
            host_edges[wfid].append([edge[0], edge[1]])
        elif edge_label == 'has_task':
            task_edges[wfid].append([edge[0], edge[1]])

def compare_nodes(graph1, graph2, n1, n2):
    print n1
    print n2
    return
    n1_childs = graph1.successors(n1['nodeattr']['name'])
    n2_childs = graph2.successors(n2['nodeattr']['name'])

    #print '***** Childrens. going one step down*****'
    #first check childs lengths
    if (len(n1_childs) - len(n2_childs)) == 0:
        #print 'child count is similar. go ahead'
        for n in n1_childs:
            print n
            if n2 in n2_childs:
                node1 = graph1.node[n]
                node2 = graph2.node[n]
                node1_wfid = node1['nodeattr']['wfid']
                node2_wfid = node2['nodeattr']['wfid']

                #print node1, node2
                #get all edges for the node
                node1_edges = graph1.edges(n, data=True)
                node2_edges = graph2.edges(n, data=True)
                print node1_edges
                print node2_edges
                process_edges(node1_wfid, node1_edges)
                process_edges(node2_wfid, node2_edges)


                """if ( node1['nodeattr']['type']=='task'):
                    #could be file or
                    if ( node1['nodeattr']['task_type']=='compute'):
                        print node1
                        print 'compute nodes: process time and relation to execute node'
                        print graph1.edges(n, data=True)

                    else:
                        print 'other taskss'

                elif ( node1['nodeattr']['type']=='file'):
                      print 'process file'
                """
                found = compare_nodes(graph1, graph2, n, n2)

            else:
                print '%s not found in second graph' % n
                return n
    else:
        print 'find missing', set(n1_childs) - set(n2_childs)

def compareAlgo2(wfid1, wfid2):
    wfgen1 = WFProvGraphGenerator(wfid1)
    wfgen2 = WFProvGraphGenerator(wfid2)
    graph1 = nx.from_pydot(wfgen1.getWorkflowProvGraph())
    graph2 = nx.from_pydot(wfgen2.getWorkflowProvGraph())

    def normalize_graph_nodes(graph):
        '''update this node (rep VM) name '''
        mapping = {}
        for i, n in enumerate(graph.nodes(data=True)):
            attr = n[1]
            #print attr
            if attr['fillcolor']=='red':

                new_name = "%s-%s-%s-%s" % (attr['nodeattr']['vCPU'], \
                                            attr['nodeattr']['minRAM'], \
                                            attr['nodeattr']['minHD'], \
                                            attr['nodeattr']['image_id'])
                #mapping.update({i:new_name})
                mapping[n[0]] = new_name
        print 'mapping %s' % mapping
        graph = nx.relabel_nodes(graph, mapping)
        return graph

    graph1 = normalize_graph_nodes(graph1)
    graph2 = normalize_graph_nodes(graph2)
    nodes1 = set(graph1.nodes())
    nodes2 = set(graph2.nodes())
    edges1 = set(graph1.edges())
    edges2 = set(graph2.edges())
    node_diff = nodes1.symmetric_difference(nodes2)
    FAILED = False
    if not node_diff:
        #means nodes are equals causing above check to return empty set
        print 'nodes in both graph are same. now check for edges'
        edge_diff = edges1.symmetric_difference(edges2)
        if not edge_diff:
            print 'edges are same in both graphs'
        else:
            print 'Edges difference found'
            print edge_diff
            FAILED = True
    else:
        print 'node different found'
        print node_diff
        FAILED = True

    if FAILED:
        print 'Boolean Comparison Failed for (%s, %s)' % (wfid1, wfid2)
    else:
        print 'Boolean comparison Passed for (%s, %s)' % (wfid1, wfid2)


def compareAlgo(wfid1, wfid2):
    #graph1 = nx.from_pydot( getWorkflowProvGraph(wfid1) )
    #graph2 = nx.from_pydot( getWorkflowProvGraph(wfid2) )
    wfgen1 = WFProvGraphGenerator(wfid1)
    wfgen2 = WFProvGraphGenerator(wfid2)
    graph1 = nx.from_pydot(wfgen1.getWorkflowProvGraph())
    graph2 = nx.from_pydot(wfgen2.getWorkflowProvGraph())

    #export graph object
    #nx.write_graphml(graph1, "%sGraph.graphml"%wfid1)
    #nx.write_graphml(graph2, "%sGraph.graphml"%wfid2)

    def normalize_graph_nodes(graph):
        '''update this node (rep VM) name '''
        mapping = {}
        for i, n in enumerate(graph.nodes(data=True)):
            attr = n[1]
            #print attr
            if attr['fillcolor']=='red':

                new_name = "%s-%s-%s-%s" % (attr['nodeattr']['vCPU'], \
                                            attr['nodeattr']['minRAM'], \
                                            attr['nodeattr']['minHD'], \
                                            attr['nodeattr']['image_id'])
                #mapping.update({i:new_name})
                mapping[n[0]] = new_name
        print 'mapping %s' % mapping
        graph = nx.relabel_nodes(graph, mapping)
        return graph

    graph1 = normalize_graph_nodes(graph1)
    graph2 = normalize_graph_nodes(graph2)

    nodes1 = graph1.nodes(data=True)
    nodes2 = graph2.nodes(data=True)
    print set(graph1.nodes()) - set(graph2.nodes())
    #print set(nodes1) - set(nodes2)
    s_time = time.time()
    FAILED = 0
    if (len(nodes1) - len(nodes2))==0:
        print 'Equal nodes: continue comparison'
        #for i in range(len(nodes1)):
        #    n1, n2 = nodes1[i], nodes2[i]
        #    print n1, n2
        #    #check its edges, job-job, job-file, job-host
        #    comparejobs(graph1, graph2, n1, n2)
        #for n,nbrs in graph1.adjacency_iter():
        #    print n, nbrs
        root1 = graph1.node['workflow']
        root2 = graph2.node['workflow']

        #initialize collections
        task_edges[wfid1]=[]
        task_edges[wfid2]=[]

        in_edges[wfid1]=[]
        in_edges[wfid2]=[]

        out_edges[wfid1]=[]
        out_edges[wfid2]=[]

        host_edges[wfid1]=[]
        host_edges[wfid2]=[]

        #print root1
        #print root2
        compare_nodes (graph1, graph2, root1, root2)

        print task_edges[wfid1]

        #if ( task_edges[wfid1] == task_edges[wfid2]):
        if compare_edge_sets( task_edges[wfid1], task_edges[wfid2]):
            print 'All Task Edges are equal'
        else:
            print 'All Task edges are not equal'
            FAILED += 1

        #if ( in_edges[wfid1]==in_edges[wfid2]):
        if compare_edge_sets( in_edges[wfid1], in_edges[wfid2]):
            print 'All InFile edges are equal'
        else:
            print 'All InFile edges are not equal'
            FAILED += 1
        #if ( out_edges[wfid1]==out_edges[wfid2]):
        if compare_edge_sets(out_edges[wfid1], out_edges[wfid2]):
            print 'All OutFile edges are equal'
        else:
            print 'All OutFile edges are not equal'
            FAILED += 1
        #if (host_edges[wfid1] == host_edges[wfid2]):
        if compare_edge_sets(host_edges[wfid1], host_edges[wfid2]):
            print 'All ExecuteOn edges are equal'
        else:
            print 'All ExecuteOn edges are not equal'
            FAILED += 1

        print host_edges[wfid1]
        print host_edges[wfid2]
        for e in host_edges[wfid1]:
            if e not in host_edges[wfid2]:
                print 'not found:', e
    else:
        print 'length not equal'
    if FAILED == 0:
        print 'Boolean Comparison result: True'
    else:
        print 'Boolean Comparsion result: False %s' % FAILED
    e_time = time.time()
    print 'Time Taken: %s' % (e_time - s_time)

def compare_edge_sets(wf1edges, wf2edges):
    wf1set = set(wf1edges)
    wf2set = set(wf2edges)
    if ( wf2set ==  wf1set):
        print 'All edges are same'
        return True
    print wf2set.symmetric_difference(wf1set)
    return False
'''
1) check workflow structure
2) check inputs and outputs
2(a) check input sources
2(b) check output similarities
3) check underlined infrastructure
3(a) check infrastructure similarities
3(b) check whether the jobs are executed on same machine
     (how many jobs executed on same machine as was done in earlier run)
combine them all, can lead us to reproducible experiment.

TODO: what about binaries and libraries installation on zero state VM
'''

def compare_graphs(wfid1, wfid2):

    graph1 = nx.from_pydot( getWorkflowProvGraph(wfid1) )
    graph2 = nx.from_pydot( getWorkflowProvGraph(wfid2) )

    print 'Both graphs for %s, %s are constructed' % (wfid1, wfid2)
    nodes1 = graph1.nodes()
    nodes2 = graph2.nodes()

    print 'nodes for wf %s: %s' % (wfid1, graph1.number_of_nodes())
    print 'nodes for wf %s: %s' % (wfid2, graph2.number_of_nodes())
    print 'nodes difference: %s' % ( set(nodes1) - set(nodes2) )

    print 'edges for wf %s: %s' % (wfid1, graph1.number_of_edges())
    print 'edges for wf %s: %s' % (wfid2, graph2.number_of_edges())
    edges1 = graph1.edges(data=True)
    edges2 = graph2.edges(data=True)
    #print ( list( set(edges1) - set(edges2) ) )

    analysis_countedge_weight(edges1)
    analysis_countedge_weight(edges2)
    print(" ")
    calculate_graph_weights(nodes1, graph1)
    calculate_graph_weights(nodes2, graph2)
    print(" Edges from a given node")
    print graph1.edges(['analyse_ID0000002'], data=True)
    print graph2.edges(['analyse_ID0000002'], data=True)

    print(" overall wf execution performance ")
    get_performance_comparison(graph1, graph2)
    #iso_morphic test
    EdgeComparison=0
    print 'Both graphs isomorphic: %s' % nx.is_isomorphic(graph1, graph2,
                                                          node_match = analysis_node_match,
                                                          edge_match = analysis_edge_match)
    DiGM = iso.DiGraphMatcher(graph1, graph2, node_match=iso.categorical_node_match(['name'],
                                                                                    ['root']))
    print 'DiGM isomorphic: %s' % DiGM.is_isomorphic()
    #print 'mapping'
    #pprint(DiGM.mapping)

    analysis_EgdeCount()
    graph_diff = ops.difference(graph1, graph2)
    print len(graph_diff.nodes()), graph_diff.nodes()
    #difference of edges between arg1 (Graph1) and arg2 (Graph2). edges found in Graph1 not present or different in Graph2
    print graph_diff.edges()

    print algo.shortest_path(graph1, source="workflow", target="merge_output", weight="weight")

    print ops.intersection_all([graph1, graph2])
    print 'Node comparsions count: %s' % NodeComparison
    #get edges and their weights
    #print [ (u, v, edata['weight']) for u, v, edata in edges1 if 'weight' in edata ]

def compare_hostnodes(host1, host2):
    attr_list=['image_id', 'vCPU', 'minRAM', 'minHD', 'flavorid'] #, 'image_name']
    node_weight = 0
    status = True

    ''' don't compare two host names because they are not significant
    in terms of job performance or acquiring a new resource in cloud'''

    #if ( host1['name'] != host2['name']):
    #    node_weight = 0

    for attr in attr_list:
        if ( host1[ attr ] == host2[ attr ]):
            continue
        node_weight+=1
        print attr, host1[attr], host2[attr], ' do not match'

    if ( node_weight > 0):
        status = False
    print host1, host2
    print 'host comparison : %s' % status
    return status

def analysis_node_match(G1_node, G2_node):
    global NodeComparison
    NodeComparison +=1
    #print 'node1=>', G1_node['nodename'], G1_node['fillcolor']
    #print 'node2=>', G2_node['nodename'], G2_node['fillcolor']
    if ( G1_node['fillcolor']=='red' and G2_node['fillcolor']=='red'):
        #host node
        return compare_hostnodes(G1_node['nodeattr'], G2_node['nodeattr'])
    elif ( G1_node[ 'nodename'] == G2_node['nodename']):
        return True
    return False

def analysis_edge_match(G1_edge, G2_edge):
    global Graph1_EdgeCount, Graph2_EdgeCount, EdgeComparison

    EdgeComparison   += 1
    Graph1_EdgeCount += G1_edge[0]['weight']
    Graph2_EdgeCount += G2_edge[0]['weight']

    #if (G1_edge[0]['weight'] == G2_edge[0]['weight'] ):
    #    print 'edges weight not equal %s, %s' % (G1_edge, G2_edge)
    if ( G1_edge == G2_edge ):
        return True

    #print G1_edge
    #print G2_edge
    #print 'edges not equal. return False'
    return False

def analysis_job_host(wfid):
    print '\n analysis_job_host'
    cypher_sql = """START n=node(*)
                 MATCH n-[:executed_on]->m
                 where n.wfid=%s and m.wfid=%s
                 return n, m
                 """ % (wfid, wfid)

    query = neo4j.CypherQuery(graph_db, cypher_sql)
    result = query.execute()

    for node in result:
        job = node['n']
        host =  node['m']
        print job,' executed on ', host

def calculate_graph_weights(nodes, graph):
    edges_total_weight = 0
    for node in nodes:
        #print node
        edges = graph.edges([node], data=True)
        for edge in edges:
            #print "%s %s %s from %s" % (edge[0], edge[2]['label'],edge[1], node)
            e = edge[2]
            edges_total_weight += e['weight']

    print 'calculate_graph_edges=%s' % edges_total_weight

def analysis_countedge_weight(edges):
    weight=0
    for e in xrange(len(edges)):
        edge = edges[e][2]
        #print edge
        weight += edge['weight']
    print 'count weighted edges: %s' % weight

def analysis_EgdeCount():
    print 'edge counter %s' % EdgeComparison
    if ( Graph1_EdgeCount == Graph2_EdgeCount):
        print 'edge count for both is equal'
        print Graph1_EdgeCount, Graph2_EdgeCount
    else:
        print 'edge count for both is not equal'
        print Graph1_EdgeCount, Graph2_EdgeCount

def analyse_graph(pydot_graph):
    import networkx as nx
    nx_G = nx.from_pydot(pydot_graph)
    G = nx.DiGraph(nx_G)
    print G.nodes()
    print 'Nodes: %s' % G.number_of_nodes()
    print G.edges()
    print '#Edges: %s' % G.number_of_edges()
    #for edge in G.edges_iter():
    #    print edge
    #    print G.get_edge_data(edge[0], edge[1])
    print nx.single_source_dijkstra_path(G, "workflow")

def get_performance_comparison(graph1, graph2):
    print 'perform individual job performance comparison'
    graph1_nodes = graph1.nodes(data=True)
    graph2_nodes = graph2.nodes(data=True)
    node_length = len(graph1_nodes)
    job_performance = {}
    other_performance = {}
    wf_performance = {}
    for i in range(node_length):
        node1 = graph1_nodes[i]
        if ( node1[0]=='stage_worker_local_WordCountWF_0_local'):
            continue;
        #print node1[1]
        if ( node1[1]['nodeattr'].has_key('type') and node1[1]['nodeattr']['type']=='task'):

            node2 = graph2_nodes[i]
            if ( node1[0] != node2[0]):
                print 'node names not matched. so return'
                continue

            wf1_id = node1[1]['nodeattr']['wfid']
            wf2_id = node2[1]['nodeattr']['wfid']

            task1_performance = float(node1[1]['nodeattr']['finish']) - float(node1[1]['nodeattr']['start'])
            task2_performance = float(node2[1]['nodeattr']['finish']) - float(node2[1]['nodeattr']['start'])
            #print '%s %s %s %s' % (node1[0], task1_performance, node2[0], task2_performance)
            #print node1[0],task1_performance, node2[0], task2_performance
            '''job performance differences executed on vms i.e. compute jobs'''
            if ( task1_performance != task2_performance):
                if ( node1[1]['nodeattr']['task_type'] == 'compute'):
                    job_performance[node1[0]] = (task2_performance - task1_performance)
                else:
                    other_performance[node1[0]] = (task2_performance - task1_performance)
            else:
                pass

            '''WF performance '''
            if (wf_performance.has_key(wf1_id)):
                wf_performance[wf1_id] = wf_performance[wf1_id] + task1_performance
            else:
                wf_performance[wf1_id] = task1_performance

            if (wf_performance.has_key(wf2_id)):
                wf_performance[wf2_id] = wf_performance[wf2_id] + task2_performance
            else:
                wf_performance[wf2_id] = task2_performance



    print 'Compute jobs performance'
    print job_performance
    print 'Other jobs performance differences'
    print other_performance
    print 'overall workflow performance'
    print wf_performance

    sumup_difference_performance(job_performance)
    sumup_difference_performance(other_performance)

def sumup_difference_performance(performance_dict):
    sum = 0
    for k, v in performance_dict.items():
        sum = sum + v
    print 'sumup difference: %s' % sum


if __name__ == '__main__':
    disable_logs2()

    #print getjob_invocation(131, 1838,'')
    st = time.time()
    #createWorkflowProvGraph(533, 78)
    #print time.time()-st
    #createWorkflowProvGraph(134)
    #drawgraph_workflow(134, True)
    drawgraph_workflow(132, True)
    #traverseProvGraph(131)

    #pydot_graph = drawgraph_workflow(161, True)
    #analyse_graph(pydot_graph)

    #compare_graphs(132, 161)

    #analysis_job_host(132)
    #analysis_job_host(131)
    #print getworkflow_job_hasParent(130, 'create_dir_WordCountWF_0_condorpool')

    ''' first approach in thesis to compare two given workflow execution traces'''
    #compareAlgo2(132, 161) #wordcount example  with different vms
    #compareAlgo2(132, 134) #wordcount example  with same vms
    #compareAlgo2(397, 395) #wordcount example with scheduling effect
    #compareAlgo2(316, 207)  #wordcount example with scheduling effect
    #compareAlgo2(163, 161)  #163 uses one VM only, 161 uses two Vms
    #compareAlgo2(161, 165)  #same VMs and same scheduling

    #compareAlgo2(548, 549) #montage 0.5 example
    #compareAlgo2(533, 534) #montage 0.2 example same VMs

    print time.time()-st