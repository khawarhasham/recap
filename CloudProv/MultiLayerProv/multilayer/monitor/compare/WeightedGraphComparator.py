from WorkflowProvGraphGenerator import WFProvGraphGenerator
import networkx as nx
import time

__author__ = 'khawar'

task_edges = {}
task_edge_count = {}

in_edges = {}
in_edge_count = {}

out_edges = {}
out_edge_count = {}

host_edges = {}
host_edge_count = {}

edge_weight = {}

def process_weighted_edges(weighted_edges, wfid1, wfid2):
    wf1edges = weighted_edges[wfid1]
    wf2edges = weighted_edges[wfid2]


def process_edges(wfid, edges):
    #print task_edges
    edges_total_weight = 0
    for edge in edges:
        edge_label = edge[2]['label']
        e = edge[2]
        if edge_label == 'consumes':
            in_edges[wfid].append([edge[0], edge[1]])
            in_edge_count[wfid].append([edge[0], edge[1], edge[2]['weight']])
        elif edge_label == 'produces':
            out_edges[wfid].append([edge[0], edge[1]])
            out_edge_count[wfid].append([edge[0], edge[1], edge[2]['weight']])
        elif edge_label == 'executed_on':
            host_edges[wfid].append([edge[0], edge[1]])
            host_edge_count[wfid].append([edge[0], edge[1], edge[2]['weight']])
        elif edge_label == 'has_task':
            task_edges[wfid].append([edge[0], edge[1]])
            task_edge_count[wfid].append([edge[0], edge[1], edge[2]['weight']])
        edges_total_weight += e['weight']
    return edges_total_weight

def process_edges2(wfid, graph, edges):
    edges_total_weight = 0
    for edge in edges:
        #print edge
        e = edge[2]
        if( e['label']=='executed_on'): #it is job->host edge: store it for later processing
            #job = edge[0]
            #host = edge[1]
            #print 'host node', host
            #print graph.node[host]
            #print e
            host_edges[wfid].append(e)

        #print e[0], e[1]
        edges_total_weight += e['weight']
    return edges_total_weight

def compare_nodes(graph1, graph2, n1, n2):
    n1_name = n1['nodeattr']['name']
    n2_name = n2['nodeattr']['name']

    n1_childs = nx.bfs_successors(graph1, n1_name)
    n2_childs = nx.bfs_successors(graph2, n2_name)
    #from dictionary get this node's connected vertices
    if (not n1_childs):
        n1_childs = []
    else:
        n1_childs = n1_childs[n1_name]

    if (not n2_childs):
        n2_childs = []
    else:
        n2_childs = n2_childs[n2_name]

    #n2_childs = graph2.bfs_successors(n2['nodeattr']['name'])

    # print '***** Childrens. going one step down*****'
    # first check childs lengths
    if (len(n1_childs) - len(n2_childs)) == 0:
        # print 'child count is similar. go ahead'
        for n in n1_childs:
            print 'for node %s -> %s' % (n1_name, n)
            if n in n2_childs:
                node1 = graph1.node[n]
                node2 = graph2.node[n]

                '''check if both nodes are compute nodes.. not processing augmented jobs'''
                if ( not node1['fillcolor']=="green" or not node2['fillcolor']=="green" ):
                    #print "dont process"
                    continue

                #    if ( node1['nodeattr']['task_type']=='compute' and node1['nodeattr']['task_type']=='compute' ):
                #print node1
                #print node2


                #if(node1['nodeattr'])
                node1_wfid = node1['nodeattr']['wfid']
                node2_wfid = node2['nodeattr']['wfid']

                #print node1, node2
                #get all edges for the node
                node1_edges = graph1.edges(n, data=True)
                node2_edges = graph2.edges(n, data=True)
                #print node1_edges
                #print node2_edges

                w1 = process_edges(node1_wfid, node1_edges)
                w2 = process_edges(node2_wfid, node2_edges)
                '''TODO: rather see the node edge count'''
                edge_weight[node1_wfid] =  edge_weight[node1_wfid] + w1
                edge_weight[node2_wfid] = edge_weight[node2_wfid] + w2

                """if ( node1['nodeattr']['type']=='task'):
                    #could be file or
                    if ( node1['nodeattr']['task_type']=='compute'):
                        print node1
                        print 'compute nodes: process time and relation to execute node'
                        print graph1.edges(n, data=True)

                    else:
                        print 'other taskss'

                elif ( node1['nodeattr']['type']=='file'):
                      print 'process file'
                """
                found = compare_nodes(graph1, graph2, node1, node2)

            else:
                print '%s not found in second graph' % n
                return n
    else:
        print 'find missing', set(n1_childs) - set(n2_childs)


def init_collections(wfid1, wfid2):
    task_edges[wfid1] = []
    task_edges[wfid2] = []
    task_edge_count[wfid1] = []
    task_edge_count[wfid2] = []
    print task_edges

    in_edges[wfid1] = []
    in_edges[wfid2] = []
    in_edge_count[wfid1] = []
    in_edge_count[wfid2] = []

    out_edges[wfid1] = []
    out_edges[wfid2] = []
    out_edge_count[wfid1] = []
    out_edge_count[wfid2] = []

    host_edges[wfid1] = []
    host_edges[wfid2] = []
    host_edge_count[wfid1] = []
    host_edge_count[wfid2] = []

    edge_weight[wfid1] = 0
    edge_weight[wfid2] = 0

def compare_weighted_algo(wfid1, wfid2):
    wfgen1 = WFProvGraphGenerator(wfid1)
    wfgen2 = WFProvGraphGenerator(wfid2)
    graph1 = nx.from_pydot(wfgen1.getWorkflowProvGraph())
    graph2 = nx.from_pydot(wfgen2.getWorkflowProvGraph())
    nodes1 = graph1.nodes(data=True)
    nodes2 = graph2.nodes(data=True)
    print nodes1
    # print set(nodes1) - set(nodes2)
    s_time = time.time()
    if (len(nodes1) - len(nodes2)) == 0:
        print 'Equal nodes: continue comparison'
        # for i in range(len(nodes1)):
        #    n1, n2 = nodes1[i], nodes2[i]
        #    print n1, n2
        #    #check its edges, job-job, job-file, job-host
        #    comparejobs(graph1, graph2, n1, n2)
        # for n,nbrs in graph1.adjacency_iter():
        #    print n, nbrs
        root1 = graph1.node['workflow']
        root2 = graph2.node['workflow']


        #new graph holding similarities

        # initialize collections
        init_collections(wfid1, wfid2)

        # print root1
        # print root2
        compare_nodes(graph1, graph2, root1, root2)

        print task_edges[wfid1]

        if task_edges[wfid1] == task_edges[wfid2]:
            print 'All Task Edges are equal'
        else:
            print 'All Task edges are not equal'

        if in_edges[wfid1]==in_edges[wfid2]:
            print 'All InFile edges are equal'
        else:
            print 'All InFile edges are not equal'
            print set(in_edges[wfid1]) - set(in_edges[wfid2])

        if out_edges[wfid1]==out_edges[wfid2]:
            print 'All OutFile edges are equal'
        else:
            print 'All OutFile edges are not equal'

        if host_edges[wfid1] == host_edges[wfid2]:
            print 'All ExecuteOn edges are equal'
            print host_edges[wfid1]
        else:
            print 'All ExecuteOn edges are not equal'
            print set(host_edges[wfid1]) - set(host_edges[wfid2])

        print "Graph Weight for %s: %s" % (wfid1, edge_weight[wfid1])
        print "Graph Weight for %s: %s" % (wfid2, edge_weight[wfid2])
        #they can be equal. but we need to see if host-job edges are also same

        #print host_edges[wfid1]
        #print host_edges[wfid2]
        #for e in host_edges[wfid1]:
        #    if e not in host_edges[wfid2]:
        #        print 'not found:', e

        process_weighted_edges(in_edge_count, wfid1, wfid2)
        process_weighted_edges(out_edge_count, wfid1, wfid2)
        process_weighted_edges(task_edge_count, wfid1, wfid2)
        process_weighted_edges(host_edge_count, wfid1, wfid2)

    e_time = time.time()
    print 'Time Taken: %s' % (e_time - s_time)

def select_k(spectrum, minimum_energy = 0.9):
    running_total = 0.0
    total = sum(spectrum)
    if total == 0.0:
        return len(spectrum)
    for i in range(len(spectrum)):
        running_total += spectrum[i]
        if running_total / total >= minimum_energy:
            return i + 1
    return len(spectrum)

#not working for Directed Graphs :(
def graph_similarity1(wfid1, wfid2):
    wfgen1 = WFProvGraphGenerator(wfid1)
    wfgen2 = WFProvGraphGenerator(wfid2)
    graph1 = nx.from_pydot(wfgen1.getWorkflowProvGraph())
    graph2 = nx.from_pydot(wfgen2.getWorkflowProvGraph())

    laplacian1 = nx.spectrum.laplacian_spectrum(graph1)
    laplacian2 = nx.spectrum.laplacian_spectrum(graph2)

    k1 = select_k(laplacian1)
    k2 = select_k(laplacian2)
    k = min(k1, k2)
    similarity = sum((laplacian1[:k] - laplacian2[:k])**2)
    print similarity

#graph_similarity1(132, 161)
compare_weighted_algo(132, 161)
