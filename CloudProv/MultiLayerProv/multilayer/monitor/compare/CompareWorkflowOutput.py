from multilayer.monitor.compare.CloudFileComparison import CloudFileComparison
from multilayer.parser.PegasusParser import PegasusParser

__author__ = 'khawar'

class CompareWorkflowOutput(object):

    def __init__(self, source_wf, dest_wf):
        self.source_wf = source_wf
        self.dest_wf = dest_wf

    def compare(self):
        parser = PegasusParser()
        source_jobs = parser.get_joboutputs(self.source_wf)
        dest_jobs = parser.get_joboutputs(self.dest_wf)
        #pprint(source_jobs)
        #pprint(dest_jobs)
        compare_count = 0
        file_count = 0
        for jobname, files in source_jobs.items():
            dest_files = dest_jobs[jobname]
            source_container = files[0]
            source_files=files[1]
            dest_container = dest_files[0]
            dest_files = dest_files[1]
            filec = len(source_files)
            for x in range(filec):
            #for keyname in source_files:
                file_count = file_count + 1
                keyname = source_files[x]
                filecomp = CloudFileComparison (self.source_wf, source_container, keyname, self.dest_wf, dest_container, dest_files[x])
                if ( filecomp.compare()  ):
                    compare_count = compare_count + 1

        if ( file_count == compare_count ):
            print 'all files are the same'
        else:
            print file_count, compare_count


if __name__=='__main__':
    #115, 114
    obj = CompareWorkflowOutput(145, 147)
    obj.compare()
