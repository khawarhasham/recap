from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
from multilayer.monitor.compare.NumericGraphComparator import graph_similarity
#from .compare.WorkflowProvGraph import compareAlgo2
from multilayer.monitor.compare.CompletenessCheck import check
from pprint import pprint

__author__ = 'khawar'


def infrastructurecomp(wfid1, wfid2):
    db = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
    SQL = "select peg_wfid, WfCloudMapping.wfID, vCPU, minRAM, minHD, flavorid, flavorname, image_name, image_id " \
          "from WfCloudMapping, WorkflowSource where WfCloudMapping.wfID=WorkflowSource.wfID and " \
          " peg_wfid in (%s, %s)" % (wfid1, wfid2)
    print SQL
    result = db.execute_query({'sql': SQL});
    data = result.fetchall()
    infra_map = {};
    for row in data:
        wfid = row['peg_wfid']
        if (not infra_map.has_key(wfid)):
            infra_map[wfid]=[]

        map = infra_map[wfid]
        vm = "%s-%s-%s-%s-%s-%s" % (row['vCPU'],row['minRAM'],row['minHD'],row['flavorid'],row['flavorname'],row['image_id'])
        map.append(vm)

    db.check_and_close();
    return infra_map



if __name__ == '__main__':
    wfid1=533
    wfid2=534

    infra = infrastructurecomp(wfid1, wfid2)
    wfid_infra = infra[wfid1]
    wfid2_infra = infra[wfid2]

    if (set(wfid_infra) == set(wfid2_infra)):
        print 'ReCAP Infrastructure: True'
    else:
        print 'ReCAP Infrastructure: Flase'
        print wfid_infra
        print wfid2_infra

    SS = graph_similarity(wfid1, wfid2)
    print "ReCAP FinalComp SS: %s" % SS

    Completeness = check({wfid1:78, wfid2:79})
    print 'ReCAP completeness: %s' % Completeness





