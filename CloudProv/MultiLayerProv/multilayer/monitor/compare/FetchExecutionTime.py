from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults

__author__ = 'khawar'

def get_timings(wftype):


    SQL = """select MAX(IF(state='WORKFLOW_STARTED',`timestamp`,NULL)) as wfst,
        MAX(IF(state='WORKFLOW_TERMINATED', `timestamp`, NULL)) as wfend, wf_id
        from workflowstate where wf_id in
        (SELECT wf_id FROM `workflow` WHERE submit_hostname like 'osdc-vm1%%' and dax_label='montage')
        group by wf_id"""
    print SQL
    dbObj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
    result = dbObj.execute_query({'sql': SQL})
    wfexecs = result.fetchall()
    for wfexec in wfexecs:
        #print wfexec
        wfid = wfexec['wf_id']
        wfst = wfexec['wfst']
        wfend = wfexec['wfend']
        if wfst and wfend:
            print wfid, (wfend - wfst)

    dbObj.check_and_close()

get_timings('WordCountWF')