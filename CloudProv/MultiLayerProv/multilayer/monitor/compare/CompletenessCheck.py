from kwms.DAGParser import DAGParser
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults

import tempfile
import time
import os

__author__ = 'khawar'
dbObj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
wfdbObj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])


def check_CAPParameters(jobmap):
    if jobmap['minRAM'] and jobmap['minHD'] \
            and jobmap['vCPU'] and jobmap['flavorname'] \
            and jobmap['image_name']:

        return True
    return False

def get_workflow_dag(peg_wfid, recap_id):
    SQL = "SELECT wfDAG from WorkflowSource where wfID=%s and peg_wfid=%s" % (recap_id, peg_wfid)
    result = dbObj.execute_query({'sql': SQL})
    record = result.fetchone()
    if ( record ):
        dag_data = record['wfDAG']

        dag_file = tempfile.NamedTemporaryFile("w", suffix='.dag', delete=False)
        dag_file.write(dag_data)
        dag_file.close()
        #print dag_file.name

        dagparser = DAGParser(dag_file.name)
        dag_job_desc = dagparser.get_job_desc()

        os.unlink(dag_file.name)
        return dag_job_desc
    return None

#full 4 jobs workflows 12, 15
#full dummy job 26, 27
#may be incomplete 39, 40

cwf_ids=[12, 15, 26, 27, 39]
peg_wf_ids = [207, 316, 340, 342, 377]
peg_cwfs = {207:12, 316:15, 340:26, 342:27, 395:48, 377:39}  #wordcount examples, 39 recapid is modified for testing. see details at bottom
#peg_cwfs = {548:81, 549:82} #montage 0.5 examples
#peg_cwfs = {533:78, 534:79} #montage 0.2 degree examples
def check(peg_cwfs):
    for wfid in peg_cwfs.keys():
        print 'Processing wfid %s ' % wfid
        #get the workflow graph from the dag file
        wfDag = get_workflow_dag(wfid, peg_cwfs[wfid])
        actualjobs =  len(wfDag.keys()) if wfDag else None
        if ( actualjobs is None):
            print 'Completeness test could not be performed for wfid %s' % wfid
            continue
        #print '************** checking WF %s *************' % wfid
        #get workflow jobs count
        SQL_jobs = "SELECT * FROM  `job` WHERE type_desc='compute' and wf_id =%s" % wfid
        result = wfdbObj.execute_query({'sql': SQL_jobs})
        wfjobs = result.fetchall()
        #print 'Pegasus wf %s' % wfid
        jobids = [str(job['job_id']) for job in wfjobs]
        print "Found jobs in the database %s" % ",".join( jobids)
        SQL_job_map = """SELECT * FROM cloudprov.WfCloudMapping
                    where wfID=%s and jobid in (%s) order by jobid""" \
                    % (peg_cwfs[wfid], ",".join( jobids))

        result2 =  dbObj.execute_query({'sql': SQL_job_map})
        data = result2.fetchall()
        #jobmap = [{jmap['jobid']:[jmap['vCPU'], jmap['minRAM'], jmap['minHD'], jmap['flavorid'],jmap['image_id']]} for jmap in data]
        jobmap = {jmap['jobid']:jmap for jmap in data}
        #print jobmap
    #result table: wfid, jobid: Map Exists (T/F), Parameters correct (T/F)
        completeness = 0
        print '-----------------------------------------------------'
        print 'WF ID  Job ID MapExists Params Exist'
        for jobid in jobids:
            mapexists = False
            recap_exists = False
            if jobmap.has_key(int(jobid)):
                mapexists = True
                recap_exists = check_CAPParameters(jobmap[int(jobid)])
                if mapexists and recap_exists:
                    completeness += 1

            print '%s \t%s \t%s \t%s' % (wfid, jobid, mapexists, recap_exists)
        print '-----------------------------------------------------'
        print 'Completeness Result: Found %s/%s with actualjobs %s ' % (completeness, len(jobids), actualjobs)
        SCore = True
        if  len(jobids) < actualjobs or completeness < actualjobs:
            print 'Completion Result for Wf %s Failed' % wfid
            SCore = False
        else:
            print 'Completion Result for WF %s Passed' % wfid
            SCore = True
        return SCore


if __name__ == '__main__':
    check(peg_cwfs = {207:12, 316:15, 340:26, 342:27, 395:48, 377:39})
    dbObj.check_and_close()
    wfdbObj.check_and_close()

'''For testing this procedure, db record for jobid 3703 is changed.
its flavorname is replaced with NULL. original mapped value was m1.small.
So change it when the testing is finished'''

'''Test results
check with original workflow dag to see how many jobs where actually there.
This will give us good estimate of what jobs were recorded and what supposed to be the jobs, and how much correct it was

if a cloud parameter is missing. It can detect false and thus raise incomplete signal
if job map is missing, it can detect false and thus raise incomplete signal
if all jobs are present in the map along with no missing cloudparam, it declares True and Success
'''