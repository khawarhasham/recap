from WorkflowProvGraphGenerator import WFProvGraphGenerator
import networkx as nx
import logging
import time

__author__ = 'khawar'

def disable_logs():
    neo4j_http_log = logging.getLogger("py2neo.packages.httpstream.http")
    neo4j_http_log.setLevel(logging.WARNING)

    neo4j_log = logging.getLogger("py2neo.neo4j")
    neo4j_log.setLevel(logging.WARNING)

    neo4j_http_log2 = logging.getLogger("httpstream")
    neo4j_http_log2.setLevel(logging.WARNING)
    neo4j_core_log = logging.getLogger("py2neo.packages.core")
    neo4j_core_log.setLevel(logging.WARNING)

    #neo4j_log2 = logging.getLogger("py2neo.packages.httpstream")
    print logging.Logger.manager.loggerDict

def disable_logs2():
    for logname in logging.Logger.manager.loggerDict.keys():
        #print logname
        if logname.find("py2neo")==0:
            #print 'changing log level for %s' % logname
            logger = logging.getLogger(logname)
            logger.setLevel(logging.WARNING)

def numeric_graph_analysis(G1, G2):
    print 'inside analysis'
    def get_common_nodes(G1, G2):
        nodes1 = G1.nodes()
        nodes2 = G2.nodes()
        common_nodes = []
        not_common = 0
        for n in nodes1:
            if n in nodes2:
                #TODO: for host node, treat it differently. without name but with attributes: DONE using attrib concat
                #TODO: for job node, only take the name at the moment
                common_nodes.append(n)
            else:
                not_common += 1
        print 'not common nodes: %s' % not_common
        #print 'common nodes: %s' % common_nodes
        return common_nodes

    def get_common_edges(G1, G2, common_nodes):
        common_edges = []
        not_common = 0
        for n in common_nodes:
            edges1 = G1.edges(n)
            edges2 = G2.edges(n)
            #following three lines shows difference in both sets
            set1 = set(edges1)
            set2 = set(edges2)
            not_common += len( set1.symmetric_difference(set2))
            common_edges.extend(list(set1.intersection(set2)))
            '''for e in edges1:/Users/khawar/Desktop/Screen Shot 2017-10-30 at 00.10.18.png
                if e in edges2:
                    #print e
                    common_edges.append(e)
                else:
                    not_common += 1
                    #print e
            '''

        print 'not common edges: %s' % not_common
        #print 'common edges: %s' % common_edges
        return common_edges

    def calculate_numeric_ss(G1, G2):
        N_o = len(G1.nodes())
        N_r = len(G2.nodes())
        E_o = len(G1.edges())
        E_r = len(G2.edges())

        #get common graph structure
        CN = get_common_nodes(G1, G2)
        CE = get_common_edges(G1, G2, CN)
        print N_o, E_o, N_r, E_r, len(CN), len(CE)
        #calculate the final value for SS
        SS =  ((float(len(CN)) / (N_o + N_r)) + (float(len(CE)) / (E_o + E_r))) * 100
        print 'Structure Similarity value :%s %%' % SS
        return SS

    return calculate_numeric_ss(G1, G2)

def graph_similarity(wfid1, wfid2):
    wfgen1 = WFProvGraphGenerator(wfid1)
    wfgen2 = WFProvGraphGenerator(wfid2)
    graph1 = nx.from_pydot(wfgen1.getWorkflowProvGraph())
    graph2 = nx.from_pydot(wfgen2.getWorkflowProvGraph())

    def normalize_graph_nodes(graph):
        '''update this node (rep VM) name '''
        mapping = {}
        for i, n in enumerate(graph.nodes(data=True)):
            attr = n[1]
            #print attr
            if attr['fillcolor']=='red':
                print attr['nodeattr']
                new_name = "%s-%s-%s-%s" % (attr['nodeattr']['vCPU'], \
                                            attr['nodeattr']['minRAM'], \
                                            attr['nodeattr']['minHD'], \
                                            attr['nodeattr']['image_id'])
                #mapping.update({i:new_name})
                mapping[n[0]] = new_name
        print 'mapping %s' % mapping
        graph = nx.relabel_nodes(graph, mapping)
        return graph

    s_time = time.time()
    #normalize node names, for hostname: create name using vcpu-ram-hd combination
    graph1 = normalize_graph_nodes(graph1)
    graph2 = normalize_graph_nodes(graph2)

    SS = numeric_graph_analysis(graph1, graph2)
    e_time = time.time()
    print 'Time Taken: %s' % (e_time - s_time)
    return SS

if __name__ == 'main':
    disable_logs2()
    '''Wordcount examples '''
    #graph_similarity(132, 161) #wordcount example
    #graph_similarity(132, 134) #wordcount example with same infrastructure
    #graph_similarity(316, 207)  #wordcount example scheduling changes causing 96.511627907%
    #graph_similarity(397, 395)

    #graph_similarity(161, 163)  #163 uses one VM only, 161 uses two Vms
    #graph_similarity(161, 165)  #same VMs and same scheduling shoud produce 100%
    '''Montage examples '''
    #graph_similarity(548, 549) #montage 0.5 examples with 4 and 3 vms respectively
    graph_similarity(533, 534) #montage 0.2 examples with 2 and 2 vms respectively: scheduling changes in re-run



'''
SELECT j.exec_job_id, h.hostname, COUNT( * )
FROM  `job_instance` AS ji, job AS j, host AS h
WHERE ji.job_id = j.job_id
AND h.host_id = ji.host_id
AND j.wf_id =535
AND j.type_desc =  'compute'
GROUP BY h.hostname
LIMIT 0 , 30
'''
