import subprocess
import platform
import socket
import psutil
import math
from datetime import timedelta
import time
import sys
import os

'''
@Author: khawar
'''

provenance_xml = '<?xml version="1.0"?><MonitoringInfo>'

'''
provide information about the underlined execution machine
<sysinfo></sysinfo>
'''
def get_sys_info():
    global provenance_xml
    
    xml = '<hostname>%s</hostname>' % socket.gethostname()
    xml += '<machine>%s</machine>' % platform.machine()
    xml += '<version>%s</version>' % platform.version()
    xml += '<platform>%s</platform>' % platform.platform()
    xml += '<system>%s</system>' % platform.system()
    xml += '<architecture>%s</architecture>' % str(platform.architecture())
    xml += '<uname>%s</uname>' % str(platform.uname())
#    print socket.gethostbyaddr(socket.gethostname())
#    print socket.gethostbyname_ex( socket.gethostname() )
    provenance_xml += "<SysInfo>%s</SysInfo>" % xml
    return xml

'''
provide information about CPUs inside the execution machine
<cpuinfo></cpuinfo>
'''
def get_cpu_info():
    global provenance_xml

    cpu_info = {}
    if ( platform.system() == 'Darwin'):
        out = subprocess.check_output(['/usr/sbin/sysctl', "-n", 
        "machdep.cpu.brand_string", "machdep.cpu.core_count"]).strip()
        
        out = out.split("\n")
        cpu_info['model_name'] = out[0]
        cpu_info['cpu_cores']  = out[1]
        
    if ( platform.system() == 'Linux'):
        command = "cat /proc/cpuinfo"
        subout = subprocess.check_output(command, shell=True).strip()
        out=[]
        for line in subout.split("\n"):
            if ( line.startswith("model name") ):
                cpu_info['model_name'] = line.split(":")[1]
            elif ( line.startswith("cpu cores") ):
                cpu_info['cpu_cores']=line.split(":")[1]
            elif ( line.startswith("cpu MHz") ):
                cpu_info['cpu_speed']=line.split(":")[1]
                
    #if ( psutil.NUM_CPUS ):
    #    cpu_info['cpu_cores'] = psutil.NUM_CPUS

    print 'cpu info: %s' % cpu_info 
    
    xml=""
    for name, val in cpu_info.items():
        xml += "<%s>%s</%s>" % (name, val, name)
    provenance_xml += "<CPUInfo>%s</CPUInfo>" % xml
    
    return cpu_info

'''
provide information about the process running over the execution machine
<processinfo></processinfo>
'''
def get_process_info(process_name):
    global provenance_xml

    print 'looking for %s' % process_name
    pid = get_process_id(process_name)
    
    p = psutil.Process(pid)

    xml = "<pID>%s</pID>" % pid
    xml += '<pName>%s</pName>' % p.name
    xml += '<pExe>%s</pExe>' %  p.exe
    #xml += '<pCWD>%s</pCWD>' % p.getcwd
    xml += '<pCMDline>%s</pCMDline>' % p.cmdline
    xml += '<pUsername>%s</pUsername>' % p.username
    xml += '<pCreateTime>%s</pCreateTime>' % p.create_time
    xml += '<pCPUTimes>%s</pCPUTimes>' % str( p.get_cpu_times() )
    xml += '<pCPUPercent>%s</pCPUPercent>' % str( p.get_cpu_percent() )
    xml += '<pOpenfiles>%s</pOpenfiles>' % str ( p.get_open_files() )
    #following require sudo access
    #print p.get_memory_maps()
    provenance_xml += "<Process>%s</Process>" % xml
    return pid
    
def get_process_id(process_name):
    cmd = "ps -ef | grep %s | grep -v 'grep' | grep -v 'Monitor.py' | awk '{print $2}'" % process_name
    #print cmd
    g = os.popen(cmd)
    proc_found = g.readlines()
    g.close()
    print proc_found
    pid = proc_found[0].replace("\n","")
    return int(pid)

def get_process_cpu_count(pid, cpu_info):
    print 'processing %s for cpu count' % pid
    proc_file="/proc/%s/stat" % pid
    pf = open(proc_file,"r")
    line = pf.read()
    #print line
    parts = line.split(" ")
    
    print "utime:%s" % parts[14]
    print "stime:%s" % parts[15]
    print "cutime:%s" % parts[16]
    print "cstime:%s" % parts[17]
    print "proc starttime:%s" % parts[22]
    
    starttime = long(parts[22])
    current_time = time.time()
    td = timedelta(seconds = starttime)
    print str(td)
    uptime = float( get_system_uptime() )
    
    print "system uptime: %s" % uptime
    
    total_time = int(parts[14]) + int(parts[15])
    total_time = total_time + int(parts[16]) + int(parts[17])
    
    cpu_speed_hertz = float(cpu_info['cpu_speed']) * math.pow(10, 6)
    print 'cpu speed hertz: %s' % cpu_speed_hertz
    
    '''uptime- (starttime/Hertz)'''
    process_elapsed_seconds = uptime -  ( int(parts[22]) / cpu_speed_hertz  )
    
    #processor_count = total_time/process_elapsed_seconds
    processor_count = total_time/(current_time - starttime)
    print 'cpu used: %s' % processor_count
    
def get_system_uptime():
    uptime = "/proc/uptime"
    with open(uptime,"r") as f:
         line = f.read()
         #print line
         line = line.split(" ")
         return line[0] 

def writeXML_toFile():
    global provenance_xml

    import xml.dom.minidom
    xml = xml.dom.minidom.parseString( provenance_xml )
    f = open("monitor.xml", "w")
    f.write(xml.toprettyxml())
    f.close()

def start_monitor():
    global provenance_xml
    get_sys_info()
    cpu_info = get_cpu_info()
    #pid = get_process_info(sys.argv[1])
    #get_process_cpu_count(pid, cpu_info)
    provenance_xml += "</MonitoringInfo>"

    writeXML_toFile()

start_monitor()