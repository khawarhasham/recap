import importlib
import traceback
import sys

__author__ = 'khawar'

'''
dynamically load plugin
'''
def get_monitor(plugin_name):
    try:
       monitor_mod = importlib.import_module("multilayer.wms.monitor." + plugin_name)
       monitor_class = getattr(monitor_mod, plugin_name)
       monitor_obj = monitor_class()
       return monitor_obj
    except:
        print sys.exc_info()
        raise Exception("Plugin %s not found" % plugin_name)


if __name__=='__main__':
    obj = get_monitor("PegasusMonitor")
    obj.set_wfid("c62b9b05-bce9-4120-a08e-4c22871c3696")
    #obj.start()
    #my_import("multilayer.monitor.plugin.PegasusMonitor")