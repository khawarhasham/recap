import importlib
import sys

__author__ = 'khawar'

def getPlugin(wms_plugin, app, db):
    try:
        wrapper_mod = importlib.import_module("plugin." + wms_plugin)
        plugin_class = getattr(wrapper_mod, wms_plugin)
        plugin_obj = plugin_class(app, db)
        return plugin_obj
    except:
        print sys.exc_info()
        raise Exception("Plugin %s not found" % wms_plugin)
