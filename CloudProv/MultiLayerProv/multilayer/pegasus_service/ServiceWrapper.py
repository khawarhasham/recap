#!/usr/bin/env python

from flask import Flask, request, url_for, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import *
from werkzeug import secure_filename
#support for User authentication
from PegPropGen import generate_pegasusrc
from flask.ext.httpauth import HTTPBasicAuth
#from WrapperConfig import config as wrapper_config
#for loading plugins
import factory
import util
import os
import re

app = Flask(__name__)
app.config.from_pyfile('app.cfg')
auth = HTTPBasicAuth()
db = SQLAlchemy(app)
#BASE_DIR=os.getcwd()
#print BASE_DIR
#app.config['BASE_DIR'] = BASE_DIR
#print app.config
wms_plugin = factory.getPlugin(app.config['WMS_PLUGIN'], app, db)
#metadata = MetaData(db.engine.connect())
#metadata.reflect(bind=db.engine.connect())
#for t in metadata.tables:
#    print t
    
#result = db.engine.execute("<sql here>")

@auth.verify_password
def verify_password(username, password):
    if username=='khawar' and password=='oxy123':
        return True
    return False

@app.route("/pegasus_service/api/v1.0/wfstatus", methods=['GET'])
@auth.login_required
def wfstatus():
   return "hello, world %s" % wms_plugin.test()


@app.route("/pegasus_service/api/v1.0/submit", methods=['POST'])
@auth.login_required
def submit():   
   result = wms_plugin.submit(request)
   return result

@app.route("/pegasus_service/api/v1.0/submit2", methods=['POST'])
@auth.login_required
def submit2():
    f_path = "%s/%s"%(BASE_DIR, app.config['UPLOAD_FOLDER'])
    print f_path
    try:
        os.stat(f_path)
    except:
        os.mkdir( f_path )
        print 'upload folder created'

    dax_file = request.files['dax']
    site_file = request.files['site']

    if ( file and site_file):
        ''' store sites.xml file '''
        filename2 = secure_filename (site_file.filename)
        wf_folder = "%s/%s" % (app.config['PEGASUS_DIR'], filename2)
        try:
            os.mkdir( wf_folder )
        except:
            pass

        site_file.save(os.path.join(wf_folder, "sites.xml"))
        site_path = os.path.join(wf_folder, "sites.xml")

        ''' store dax file '''
        filename1 = secure_filename (dax_file.filename)
        dax_file.save(os.path.join(wf_folder, filename1))
        wf_path = os.path.join(wf_folder, filename1)

        '''write pegasusrc file '''
        pegasusrc = generate_pegasusrc(os.path.abspath(site_path))
        f = open("%s/pegasusrc" % wf_folder, "w")
        f.write(pegasusrc)
        f.close()

        return jsonify({'status':'submitted',
                        'output': call_command( os.path.abspath(wf_folder), \
                                                os.path.basename(wf_path) )
        })
        #return url_for('uploads', filename=filename2)

    return "Unable to upload your file"

@app.route("/pegasus_service/api/v1.0/call/<wf_folder>/<wfname>", methods=['GET'])
@auth.login_required
def call_command(wf_folder, wfname):
    return wms_plugin.call_command(wf_folder, wfname)

@app.route("/pegasus_service/api/v1.0/update_wf", methods=['POST'])
@auth.login_required
def update_wfid():
    return wms_plugin.update_wfid(request)
    #return SQL

def insert_workflow(site_file, dax_file, prop_file):
    SQL="INSERT into WorkflowSource(wfSite, wfDAG, wfProps) Values('%s', '%s', '%s')" % \
        (getfile_data(site_file), getfile_data(dax_file), getfile_data(prop_file))
    result = db.engine.execute(SQL)
    if ( result is not None ):
        SQL = "SELECT LAST_INSERT_ID() as last_id;"
        result = db.engine.execute(SQL)
        row = result.fetchone()
        id = row['last_id']
        app.logger.debug("Workflow added to WorkflowSource %s" % id)
        return id
        
        
def getfile_data(file_path):
    f = open(file_path, "r")
    data = f.read()
    f.close()
    return data
    
@app.route("/pegasus_service/api/v1.0/uploads/<filename>", methods=['GET'])
@auth.login_required
def uploads(filename):
    app.logger.debug("looking for %s" % filename)
    file_path = "%s%s" % (app.config['UPLOAD_FOLDER'], filename)
    if ( os.path.isfile(file_path) and os.path.exists(file_path) ):
        return "%s yes exists:" % filename
    return "Not exists"

@app.route("/pegasus_service/api/v1.0/site-map", methods=['GET'])
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        url = url_for(rule.endpoint)
        links.append((url, rule.endpoint))
    print links
    return "Test"

@app.route("/pegasus_service/api/v1.0/jobmon/<sched_id>", methods=['GET'])
def jobmon(sched_id):
    '''sched_id is an ID given to a job in condor pool'''
    condor_output = util.monitor_job(sched_id)
    return jsonify({sched_id:condor_output})

@app.route("/pegasus_service/api/v1.0/cpool_mips", methods=['GET'])
@auth.login_required
def cpool_mips():
    '''retrieve condor pool and server's mips information'''
    condor_output = util.monitor_pool()
    return jsonify({"cpool":condor_output})

if __name__ == '__main__':
    app.run(
            host="0.0.0.0",
            #port=int("3434"),
            debug=True
            )
