from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
import util

from Queue import Queue
import threading


__author__ = 'khawar'

class CondorJobMon(threading.Thread):
    def __init__(self, mode):
        threading.Thread.__init__(self)
        self.mon_queue= Queue()
        #self.dbconn = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
        '''all or single job mode'''
        self.mode=mode

    def add_queue(self, condor_id):
        self.mon_queue.put(condor_id)

    def shutdown(self):
        self.shutdown = True

    def run(self):
        while (not self.shutdown):
            if self.mode == 'all':
                util.monitor_all_jobs()
            elif self.mode == 'single':
                '''process job queue'''
                if ( self.mon_queue.empty()):
                    '''do nothing'''
                    continue
                else:
                    condor_id = self.mon_queue.get()
                    util.monitor_job(condor_id)

            '''put this thread to sleep if no further work'''



