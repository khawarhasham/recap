from base import PluginBase
from flask import jsonify
from werkzeug import secure_filename
import subprocess
import os
import re

__author__ = 'khawar'

class pegasus(PluginBase):
    def __init__(self, app, appdb):
        PluginBase.__init__(self, app, appdb)
        self.counter=0

    def test(self):
        self.counter+=1
        return self.counter

    def submit(self, request):
        try:
            os.stat(self.app.config['UPLOAD_FOLDER'])
        except:
            os.mkdir(self.app.config['UPLOAD_FOLDER'] )

        print 'upload folder created'

        dax_file = request.files['dax']
        site_file = request.files['site']
        #rc_file = request.files['rc']
        tc_file = request.files['tc']

        if ( file and site_file):
            ''' store sites.xml file '''
            filename2 = secure_filename (site_file.filename)
            wf_folder = "%s/%s" % (self.app.config['PEGASUS_DIR'], filename2)
            try:
                os.mkdir( wf_folder )
            except:
                pass

            site_file.save(os.path.join(wf_folder, "sites.xml"))
            site_path = os.path.join(wf_folder, "sites.xml")

            ''' store dax file '''
            filename1 = secure_filename (dax_file.filename)
            dax_file.save(os.path.join(wf_folder, filename1))
            wf_path = os.path.join(wf_folder, filename1)

            '''write pegasusrc file '''
            pegasusrc = self.generate_pegasusrc(os.path.abspath(site_path))
            f = open("%s/pegasusrc" % wf_folder, "w")
            f.write(pegasusrc)
            f.close()

            '''add workflow to cloudprov db.
            TODO: HOW ABOUT: doing it only if a worklfow is submitted successfully'''
            wfid = self.__insert_workflow(site_path, wf_path, "%s/pegasusrc" % wf_folder)

            return jsonify({'wfid': wfid,
                            'output':self.call_command( os.path.abspath(wf_folder), \
                                          os.path.basename(wf_path) )})

        return jsonify({'error':'NotSubmitted'})


    def call_command(self, wf_folder, wfname):
        arg_str = "pegasus-plan --conf pegasusrc --sites condorpool --dir tmpwork --output-site s3 --dax %s --submit" % wfname
        #arg_str = "./submit %s" % wfname
        args = arg_str.split(" ")
        print arg_str
        #curr_path = os.getcwd()
        #process = subprocess.Popen(args, stdout=subprocess.PIPE, cwd="%s/%s" % (curr_path, PEGASUS_DIR) )
        process = subprocess.Popen(args, stdout=subprocess.PIPE, cwd=wf_folder  )
        out, err = process.communicate()
        if ( err == None ):
            print 'process the output to get wfuuid'
            pattern = r"(.+)\s+(\w+:)\s+ pegasus-status -l (.+)"
            m = re.search(pattern, out)
            if ( m ):
                wf_work_dir = m.group(3)
                wf_work_dir = wf_work_dir.strip()

                brainfile = "%s/braindump.txt" % wf_work_dir
                f = open (brainfile, "r")
                brain_data = f.read()
                f.close()
                return brain_data
            else:
                return "out:%s <BR/> err:%s" % (out, err)

    def __insert_workflow(self, site_file, dax_file, prop_file):
        SQL="INSERT into WorkflowSource(wfSite, wfDAG, wfProps) Values('%s', '%s', '%s')" % \
            (self.getfile_data(site_file), self.getfile_data(dax_file), self.getfile_data(prop_file))
        result = self.db.engine.execute(SQL)
        if ( result is not None ):
            SQL = "SELECT LAST_INSERT_ID() as last_id;"
            result = self.db.engine.execute(SQL)
            row = result.fetchone()
            id = row['last_id']
            self.app.logger.debug("Workflow added to WorkflowSource %s" % id)
            return id

    def getfile_data(self, file_path):
        f = open(file_path, "r")
        data = f.read()
        f.close()
        return data

    def update_wfid(self, request):
        print request.values
        wfid = request.values['wfid']
        pegwfid = request.values['pegwfid']

        SQL="update WorkflowSource set peg_wfid=%s where wfID=%s" % (pegwfid, wfid)
        result = self.db.engine.execute(SQL)
        return str(result)

    def get_files(self, jobinst_id):
        SQL="""SELECT stdout_text, submit_dir, stdout_file
               FROM job_instance
               WHERE job_instance_id=%s
            """ % jobinst_id
        result = self.db.engine.execute(SQL)
        row = result.fetchone()

        if ( row ):
            sub_dir = row['submit_dir']
            out_file = row['stdout_file']
            err_file = row['stderr_file']
            fo = open("%s/%s" % (sub_dir, out_file) )
            fo_data = fo.readall()
            fo.close()
            fe = open("%s/%s" % (sub_dir, err_file))
            fe_data = fe.readall()
            fe.close()
            return {'out':fo_data, 'err':fe_data}

    def generate_pegasusrc(self, site_file):

        pegasusrc = """pegasus.dir.storage.deep=false
        pegasus.catalog.replica.db.url=jdbc\:mysql\://localhost\:3306/pegasusdb
        pegasus.catalog.transformation=File
        dagman.maxpre=2
        pegasus.home.schemadir=/home/khawar/pegasus-4.2.0/share/pegasus/schema
        pegasus.execute.*.filesystem.local=true
        pegasus.condor.logs.symlink=false
        pegasus.monitord.events=true
        pegasus.catalog.transformation.file=/home/khawar/pegasus_service/peg_files/tc.data
        pegasus.data.configuration=nonsharedfs
        pegasus.catalog.provenance=InvocationSchema
        pegasus.catalog.site.file=%s
        pegasus.gridstart=PegasusLite
        pegasus.catalog.site=XML3
        pegasus.monitord.output=mysql\://pegasus\:oxy123@localhost\:3306/pegasusdb
        pegasus.home.sysconfdir=/home/khawar/pegasus-4.2.0/etc
        pegasus.catalog.replica.db.password=oxy123
        pegasus.catalog.replica.db.driver=MySQL
        pegasus.home.bindir=/home/khawar/pegasus-4.2.0/bin
        pegasus.transfer.worker.package=true
        pegasus.home.sharedstatedir=/home/khawar/pegasus-4.2.0/share/pegasus
        pegasus.dashboard.output=mysql\://pegasus\:oxy123@localhost\:3306/pegasusdb
        pegasus.catalog.replica.db.user=pegasus""" % site_file
        return pegasusrc
