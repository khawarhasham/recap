from multilayer.monitor.plugin.VirtLayerMonitor import VirtLayerMonitor
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults

import json
import sys

__author__ = 'khawar'

class LazyMapper(object):
    def __init__(self):
        #SEE: if VirtLayerMonitor can be instantiate here. Otherwise leave it.
        monitor = VirtLayerMonitor()
        monitor.start()


    '''lazy mapping Phase 2. Phase 1 is handled through monitor.plugin.VirtLayerMonitor'''
    def establish_mapping(self, wfid, cloudprovid):

        '''get all jobs for which there is no host id and its mapping in temp table.
        since we can rely on host info from pegasus db so we ll use the mapping from temp table.
        moreover, it also assumes that the required VMs are already destroyed and no longer available
        in the Cloud infrastructure'''

        #, host.host_id,hostname,ip from job_instance as inst left join host on inst.host_id=host.host_id
        jobSQL = """select job_id, host_id, hostname, ip from job_instance as inst, host as h
                 where job_id in (select job_id from job where wf_id=%s) """ % wfid

        SQL="""SELECT * FROM cloudprov.WfCloudVMs WHERE hostname IN
        (
         SELECT hostip inst.job_id FROM pegasusdb.job_instance AS inst WHERE inst.host_id IS NULL
         AND inst.job_id IN
         ( SELECT inst.job_id
         FROM pegasusdb.job_instance AS inst LEFT JOIN pegasusdb.job ON job.job_id = inst.job_id
         WHERE job.wf_id =%s
         )
       )AND wfID =%s""" % (wfid, cloudprovid)

        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()

        try:
            map_result = clouddb.execute_query({'sql': SQL})
            records = []
            for row in map_result:
                d={'wfID': row['wfID'],
                    'jobid': row['jobid'],
                    'nodename':row['hostname'],
                    'hostip':row['hostip'],
                    'middleware':row['middleware'],
                    'minRAM':row['minRAM'],
                    'minHD':row['minHD'],
                    'vCPU':row['vCPU'],
                    'flavorid':  row['flavorid'],
                    'flavorname': row['flavorname'],
                    'image_name':  row['image_name'],
                    'image_id':row['image_id'],
                    'extra':json.dumps({})
                }
                records.append(d)

            insertreq={'conn':target_db, 'table_name':'WfCloudMapping', \
                   'insert_values': records}
            clouddb.insert_record(insertreq)

        except:
            print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );

    def establish_cpuspec_mapping(self, cpuspecs_map):
        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()

        try:
            insertreq={'conn':target_db, 'table_name':'CPUSpecs', 'insert_values':cpuspecs_map}
            print clouddb.insert_record(insertreq)
        except:
            print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );