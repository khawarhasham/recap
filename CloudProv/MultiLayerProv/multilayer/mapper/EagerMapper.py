from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
from multilayer.util import Util
from multilayer.cloud.CloudVirtLayer import CloudVirtLayer
from multilayer.cloud.CloudStorageProv import CloudStorageLayer
import logging
from datetime import datetime
import json
import sys
import traceback

__author__ = 'khawar'

#TODO: see if a job changes host during execution. invocation should catch this and so this approach.
#TODO: in that case, I should use the last entry for mapping so for the results. (GOOD and simple :) )
class EagerMapper(object):
    def __init__(self):
        self.LOG = logging.getLogger("provlog")

    '''##################### Eager Approach Phase 2 ##########################'''
    def establish_mapping(self, wfid, cloudprovid):

        '''get all jobs for which there is no host id and its mapping is present in temp table.
        since we can rely on host info from pegasus db so we ll use the mapping from temp table.
        moreover, it also assumes that the required VMs are already destroyed and no longer available
        in the Cloud infrastructure.

        Here we need to do two phase mapping.
        1) map all jobs with temp table for which we don' have host info from pegasus.
        2) if some jobs have host info, match them with temp table and link them
        to have one mapping and avoid the scenario in which a mapping could be missed.
        dont look for null hostids.
        get all the host info (null or not null)'''

        #, host.host_id,hostname,ip from job_instance as inst left join host on inst.host_id=host.host_id
        SQL="""SELECT * FROM cloudprov.WfCloudTempMapping WHERE jobid IN
        (
         SELECT inst.job_id FROM pegasusdb.job_instance AS inst WHERE inst.job_id IN
         ( SELECT inst.job_id
         FROM pegasusdb.job_instance AS inst LEFT JOIN pegasusdb.job ON job.job_id = inst.job_id
         WHERE job.wf_id =%s
         )
       )AND wfID =%s""" % (wfid, cloudprovid)

        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()

        try:
            map_result = clouddb.execute_query({'sql': SQL})
            records = []
            for row in map_result:
                d={'wfID':       row['wfID'],
                'jobid':      row['jobid'],
                'nodename':   row['hostname'],
                'hostip':     row['hostip'],
                'middleware': row['middleware'],
                'minRAM':     row['minRAM'],
                'minHD':      row['minHD'],
                'vCPU':       row['vCPU'],
                'flavorid':   row['flavorid'],
                'flavorname': row['flavorname'],
                'image_name': row['image_name'],
                'image_id':   row['image_id'],
                'extra':      json.dumps({})
                }
                print d
                records.append(d)

            #move mapping from WfCloudTempMapping to WfCloudMapping
            insertreq={'conn':target_db, 'table_name':'WfCloudMapping', \
                    'insert_values': records}
            clouddb.insert_record(insertreq)

        except:
            print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );

    '''############ Eager Approach Phase 2 ends ###############'''

    '''Eager Approach Phase 1 '''
    def create_temp_mapping(self, hostrecord, condor_mips):
        print hostrecord

        orghostname = hostrecord['hostname']  #this will be used for mips
        host_mips = -1
        if ( condor_mips.has_key(orghostname)):
            host_mips = int(condor_mips[orghostname])

        hostname = hostrecord['hostname']
        #OpenStack' hostname is different from Condor's. So remove the domain part from hostname
        #this could be one possible problem if hostname contains . (which is difficult). but still.
        #one workaround could be to you condor_history information and use IP from there but this work around
        #would assume that the VM still exists which is not the case for eager approach

        hostname = hostname[:hostname.find(".")]
        hostrecord['hostname']=hostname
        jobid = hostrecord['jobid']
        wfid = hostrecord['wfID']
        condor_id = hostrecord['condor_id']

        virt_layer = CloudVirtLayer()
        virt_layer.get_images()
        virt_layer.get_flavors()
        #TODO: change it with name or ip search coz condor can give us both
        #FIXED: now with both
        vm = virt_layer.get_node_by_name(hostname)  #look for name
        vm = vm if vm else virt_layer.get_node(hostname)   #if name not matched, look for ip
        if ( vm != None ):
            print 'found vm %s' % vm.private_ips[0]
            vm_flavor = Util.get_flavor(vm.extra['flavorId'], virt_layer.flavors)
            vm_img = virt_layer._get_image(vm.extra['imageId'], False)
            hostrecord['hostip']=vm.private_ips[0]  #this can be changed to public
            hostrecord['flavorid']=vm_flavor.id
            hostrecord['flavorname']=vm_flavor.name
            hostrecord['minRAM']=vm_flavor.ram
            hostrecord['minHD']=vm_flavor.disk
            hostrecord['vCPU']=vm_flavor.vcpus
            hostrecord['image_name']= vm_img.name
            hostrecord['image_id']=vm_img.id
            hostrecord['middleware']='OpenStack'
            hostrecord['condor_id']=condor_id
            hostrecord['mon_time']=datetime.utcnow()
            print hostrecord
            SQL = """SELECT hostip from WfCloudTempMapping
            where (jobid=%s and wfid=%s and condor_id=%s) and (hostname='%s' and hostip='%s')
            """ % (jobid, wfid, condor_id, hostname, hostrecord['hostip'])

            clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
            target_db = clouddb.get_connection()
            try:
                result = clouddb.execute_query({'sql':SQL})
                records = result.fetchone()
                if records:
                    '''this record exists so do nothing and return back'''
                    self.LOG.debug("A temporary mapping exists for job %s wf %s hostname %s" % (jobid, wfid, hostname))
                    clouddb.close_connection()
                    return

            except:
                print 'Error found in eager check: %s' % sys.exc_info()[0]
                traceback.print_exc(file=sys.stdout)

            insertreq={'conn':target_db, 'table_name':'WfCloudTempMapping', \
                    'insert_values': [hostrecord]}
            #provdb.insert_record( target_db, 'VirtualLayerProvenance', records)
            print clouddb.insert_record(insertreq)

            #now create mapp between jobid, wfid, and mips found from condor
            d = {'wfID':wfid, 'jobid':jobid, 'mips':host_mips}
            insertreq={'conn':target_db, 'table_name':'CPUSpecs', 'insert_values':[d]}
            print clouddb.insert_record(insertreq)

            print 'closing db connection'
            clouddb.close_connection()

    def establish_cpuspec_mapping(self, cpuspecs_map):
        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()

        try:
            insertreq={'conn':target_db, 'table_name':'CPUSpecs', 'insert_values':cpuspecs_map}
            print clouddb.insert_record(insertreq)
        except:
            print 'Error found in eager check: %s' % sys.exc_info()[0]
            traceback.print_exc(file=sys.stdout)
