from pprint import pprint
import logging
import json
import sys
from datetime import datetime
import time
import traceback

from multilayer.cloud.CloudVirtLayer import CloudVirtLayer
from multilayer.cloud.CloudStorageProv import CloudStorageLayer
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
from multilayer.util import Util

__author__ = 'khawar'

class SimpleMapper(object):
    def __init__(self):
        self.LOG = logging.getLogger("provlog")
        self.provdb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings']) #change from db_settings

    '''
    second wfid added for cloudprov db

    TODO: for dynamic flag as True, check for host info from WfCloudTempMapping if Pegasus DB does not have this
    or that VM is no more exist on infrastructure.
    '''
    def establish_mapping(self, wf_id, cloudprov_wfid):
        self.provdb.get_connection()

        SQL = """select inst.job_id, host.* from host left join job_instance as inst on inst.host_id=host.host_id
              where host.site<>'local' and inst.job_id in (
                 select inst.job_id
                 from job_instance as inst left join job on job.job_id=inst.job_id
                 left join workflow as wf on wf.wf_id=job.wf_id
              where wf.wf_id=%s)""" % wf_id

        '''select host.* from host where site<>'local' and host_id in (
              select inst.host_id
              from job_instance as inst left join job on job.job_id=inst.job_id
              left join workflow as wf on wf.wf_id=job.wf_id
              where wf.wf_id=%s)""" % wf_id'''
        print SQL

        #host_list collection is required from WMS database.
        host_list={}
        try:
            host_result  = self.provdb.execute_query({'sql': SQL})
            #print host_result
            for row in host_result:
                print row
                if ( not host_list.has_key(row['ip'])):
                    host_list[row['ip']] = {}

                if ( not host_list[row['ip']].has_key('host') ):
                    host_list[row['ip']]['host'] = [row]
                else:
                    host_list[row['ip']]['host'].append(row)

        except:
            print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );


        pprint(host_list)
        self.LOG.debug ('accessing cloud middleware for mappings for wfid %s' % wf_id)
        virt_layer = CloudVirtLayer()
        virt_layer.get_images()
        virt_layer.get_flavors()

        for host_ip, host_info in host_list.items():
            self.LOG.debug ('retrieve vm info for %s' % host_ip )
            vm = virt_layer.get_node(host_ip, True)

            if ( vm != None ):
                vm_flavor = Util.get_flavor(vm.extra['flavorId'], virt_layer.flavors)
                vm_img = virt_layer._get_image(vm.extra['imageId'], False)
                host_list[host_ip]['flavor']=[vm_flavor.id, vm_flavor.name, \
                                          vm_flavor.ram, vm_flavor.disk, vm_flavor.vcpus]
                host_list[host_ip]['image']=[vm_img.id, vm_img.name]
        pprint (host_list)

        #insert host_list in provdb table
        records = []
        a = {}

        #TODO: see if hostname vm_name can also be added

        for host_ip, host_detail in host_list.items():

            if ( not host_detail.has_key('flavor') ): continue

            flavor = host_detail['flavor']
            vm_img = host_detail['image']
            jobs = host_detail['host']

            for job in jobs:
                d = {'wfID':     cloudprov_wfid, #change from wfid
                 'jobid':     job[0],
                 'hostid':    job[1],
                 'nodename':  job[4],  #
                 'hostip':    host_ip,
                 'middleware':'OpenStack',
                 'flavorid':   flavor[0],
                 'flavorname': flavor[1], #added for cloudprov
                 'minRAM':     flavor[2],
                 'minHD':      flavor[3],
                 'vCPU':       flavor[4],
                 'image_name': vm_img[1],
                 'image_id':   vm_img[0],
                 'extra':      json.dumps(a)}
                records.append(d)
        print self.provdb
        self.provdb.close_connection()
        #pprint(records)
        #for cloudprov db: change VirtualLayerProvenance to WFCloudMapping
        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()
        insertreq={'conn':target_db, 'table_name':'WfCloudMapping', \
                'insert_values': records}
        #provdb.insert_record( target_db, 'VirtualLayerProvenance', records)
        clouddb.insert_record(insertreq)
        print 'closing db connection'
        clouddb.close_connection()

    def establish_cpuspec_mapping(self, cpuspecs_map):
        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()

        try:
            insertreq={'conn':target_db, 'table_name':'CPUSpecs', 'insert_values':cpuspecs_map}
            print clouddb.insert_record(insertreq)
        except:
            print('workflows() MySQLdb error %s: %s' % (sys.exc_info()[0], sys.exc_info()[1]) );
