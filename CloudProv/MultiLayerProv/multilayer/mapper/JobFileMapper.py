from multilayer.cloud.CloudStorageProv import CloudStorageLayer
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
import json

__author__ = 'khawar'

'''To insert records in JobCloudFile bridge table '''
class JobFileMapper(object):
    def __init__(self):
        pass

    def establish_mapping(self, job_details, cloudprov_wfid):
        cloudStore = CloudStorageLayer()
        records = []
        for job_id, job_detail in job_details.items():
            cloudfiles = job_detail['cloudfiles']
            if ( len(cloudfiles.keys())>0 ):
                for container, file_keys in cloudfiles.items():
                    for file_key in file_keys:
                        cloud_obj = cloudStore.get_cloudfile(container, file_key)
                        d={
                            'container_name': container,
                            'keyname': file_key,
                            'size': cloud_obj.size,
                            'metadata': json.dumps(cloud_obj.meta_data),
                            'hash': cloud_obj.hash,
                            'extra': json.dumps(cloud_obj.extra),
                            'modified_date': cloud_obj.extra['last_modified'],
                            'wfid': cloudprov_wfid
                        }
                        records.append(d)

        clouddb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings']) #change from db_settings
        target_db = clouddb.get_connection()
        insertreq={'conn':target_db, 'table_name':'CloudFileCatalog', \
                   'insert_values': records}
        clouddb.insert_record(insertreq)
        """
        jobcloudfiles.append({
                'wfID':cloudprov_wfid,
               'jobid': job_id,
               'cloudFileID':1,
        })"""
        for job_instid, job_detail in job_details.items():
            jobid = job_detail['jobid']
            if ( job_detail.has_key("job_out")):
                job_outs = job_detail['job_out']
                for out in job_outs:
                    SQL="""select CloudFileID from CloudFileCatalog
                        where wfid=%s and keyname='%s'""" %(cloudprov_wfid, out)
                    res = clouddb.execute_query({'sql': SQL})
                    one = res.fetchone()
                    cloudfile_id = one['CloudFileID']

                    SQL="""INSERT into JobCloudFile
                        VALUES(%s,%s,%s)
                        """ % (cloudprov_wfid, jobid, cloudfile_id)
                    res = clouddb.execute_query({'sql': SQL})

        clouddb.close_connection()
