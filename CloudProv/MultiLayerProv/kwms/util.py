import networkx as nx
from random import randint

__author__ = 'khawar'

def get_ctime(t):
    #return time.ctime(Decimal(t))
    return t

def draw_graph(G, wfid):
    gplt = nx.to_pydot(G)
    gplt.write("./test%s.pdf" % wfid, format="pdf")

def ss(name):
    #return sum([ord(x) for x in name])/30
    return randint(10,14)

def traverse(node, G):
    neighbors = G.neighbors(node)
    if ( neighbors is None):
        print 'No neighbors for %s' % node
        return
    print 'traversing neighbors of %s' % node
    for n in neighbors:
        traverse(n, G)