import networkx as nx
from networkx.algorithms.traversal.breadth_first_search import *

#for VM provisioing
from precip import *

from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
from kwms.provision.randprovision import RandomProvision
from kwms.provision.cap import CAPProvisioner


import util

#plotting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

from random import randint
import threading
import tempfile
from decimal import Decimal
import time
import stat
import sys
import os

import traceback

__author__ = 'khawar'

KW_st = time.time()

class JobState:
    WAITING=0
    READY=1
    PROCESSING=2
    SUBMITTING=3
    RUNNING=4
    FINISH=5
    COMPLETE=6
    FAIL=7

class CloudProvider:
    UWE=0
    OSDC=1

class TaskSimulation(threading.Thread):
    def __init__(self, name, task):
        threading.Thread.__init__(self)
        self.task = task
        self.name = name
        print '{} {} arrived '.format(self.name, self.task)

    def run(self):
        self.task['state'] = JobState.RUNNING
        try:
            print '{} {} started'.format(self.name, self.task)
            st_time = time.time() - KW_st
            exc_time = self.task['process']
            time.sleep(exc_time)
            print '{} {} finished'.format(self.name, self.task)
            fn_time = time.time() - KW_st
            self.task['s_time'] = st_time
            self.task['f_time'] = fn_time
        except:
            print 'error in task execution: %s' % sys.exc_info()[0]

        self.task['state'] = JobState.FINISH

class VirtualMachineSimulation(threading.Thread):
    def __init__(self, name, task):
        threading.Thread.__init__(self)
        self.task = task
        self.name = name
        print '{} {} arrived'.format(self.name, self.task)

    def set_resources(self, vcpu, ram, hd, mips):
        self.mips = mips
        self.vpcu = vcpu
        self.ram = ram
        self.hd = hd

    def __boot_vm(self):
        '''act as booting a virtual machine
        for the moment, use a random number. otherwise go for statistically measured
        value.
        '''
        boot_time = randint(10,14)
        time.sleep(boot_time)
        return boot_time

    def run(self):
        #first boot up the machine
        self.task['vm_boot'] = self.__boot_vm()
        #now process the task
        self.task['state'] = JobState.RUNNING
        try:
            print '{} {} started'.format(self.name, self.task)
            st_time = time.time() - KW_st
            exc_time = self.task['process']

            #extrapolate jobruntime by * with 1 hr
            exc_time = (exc_time * randint(30,60) * 60)/self.mips

            #use mips
            time.sleep(exc_time)
            print '{} {} finished'.format(self.name, self.task)
            fn_time = time.time() - KW_st
            self.task['s_time'] = st_time
            self.task['f_time'] = fn_time
            self.task['other'] = [st_time, fn_time]
        except:
            print 'error in task execution: %s' % sys.exc_info()[0]

        self.task['state'] = JobState.FINISH


class VirtualMachineExecution(threading.Thread):
    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        print kwargs
        """
        self.job_exec = kwargs['exec']
        self.job_params = kwargs['params']
        self.job_inputs = kwargs['inputs']
        self.job_outputs = kwargs['outputs']
        self.vm_config = kwargs['vm_config']
        """

    def run(self):
        # first create the execution script
        # boot up the machine
        #then send and execute the job script
        job_script = self.get_job_script()
        print job_script

    def get_job_script(self):
        #build input download part
        download = []
        for input in self.job_inputs:
             download.append("cp %s ." % input)

        download_str ="\n".join(download)

        #build output upload part
        upload = []
        for output in self.job_outputs:
            upload.append("cp %s ." % output)

        upload_str ="\n".join(upload)

        script = """#!/bin/env bash

        #download required files
        %s

        #execute the job
        python %s %s

        #upload outputs
        %s
        """ % (download_str, self.job_exec, self.job_params, upload_str)
        return script


class TestExecution(threading.Thread):
    '''Ths class actually provision a VM and submit job'''
    def __init__(self, job, stage='local'):
        threading.Thread.__init__(self)
        self.job = job
        # for OSDC settings
        self.vm_image = "wf_peg_repeat"
        self.stage = stage #default is local. means use cp command

    def save_file(self, filedata, filename):
        fileTemp = tempfile.NamedTemporaryFile(delete = False)
        #f = open(filename, "w")
        fileTemp.write(filedata)
        fileTemp.close()

        st = os.stat(fileTemp.name)
        # change to 755 file permission
        os.chmod(fileTemp.name, st.st_mode | stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)
        return fileTemp

    def run(self):
        self.job['state'] = JobState.RUNNING

        print self.job
        if ( self.job['type']=='stage-in'):
            script = self.generate_stagein_script()
        elif ( self.job['type']=='createdir'):
            script = self.generate_createdir_script()
        elif ( self.job['type']=='comp'):
            script = self.generate_script()

        #print script
        #for actual testing, comment following 3 lines
        if ( 1 == 1 ):
            self.job['state'] = JobState.FINISH
            return

        script_file = self.save_file(script, "")

        #provision a vm here.
        vm_flavor = self.job['flavor']
        exp = None
        try:
            exp = OpenStackExperiment(
                os.environ['EC2_URL'],
                os.environ['EC2_ACCESS_KEY'],
                os.environ['EC2_SECRET_KEY'])
            boot_time_s = time.time()
            exp.provision(self.vm_image, tags=["test1"],
                          instance_type=vm_flavor, count=1)
            exp.wait()
            boot_time_e = time.time()
            self.job['boot'] = boot_time_e - boot_time_s
            #get VM instance
            instances = exp.get_instances()

            st_time = time.time() - KW_st
            exit_code, stdout, stderr = exp.copy_and_run(["test1"], script_file.name, args=[], \
                                                         user="ubuntu", check_exit_code=True)

            fn_time = time.time() - KW_st
            self.job['s_time'] = st_time
            self.job['f_time'] = fn_time
            self.job['exit_code'] = exit_code
            self.job['stdout'] = stdout
            self.job['stderr'] = stderr

            ec2_inst = instances[0].ec2_instance
            #self.job['instance'] = ec2_inst
            self.job['instance.name'] = ec2_inst.private_dns_name
            self.job['instance.ip'] = [ec2_inst.private_ip_address, ec2_inst.ip_address]
            self.job['instance.launch_time'] = ec2_inst.launch_time
            self.job['state'] = JobState.FINISH
        except Exception as e:
            print "ERROR: %s" % e
            self.job['state'] = JobState.FAIL

        finally:
            if exp is not None:
                exp.deprovision()
                print 'VM shutdown for job %s' % self.job['jname']


    def generate_createdir_script(self):

        if self.stage == 's3':
            script="""
            pegasus-s3 mkdir s3://admin@uweopenstack%s
            """ % self.job['outpath']
        else:
            script="""
            mkdir -p /glusterfs/users/mkhahmad/smwf_cap%s
            """ % self.job['outpath']
        return script

    def generate_stagein_script(self):
        #script for copying files to the given shared location
        cmd = []
        for inp, path in self.job['input'].items():
            inpath = path[0].replace("file://","")
            dest = "%s/%s"% (self.job['outpath'], inp)
            #cmd.append("cp %s %s" % (inpath, dest))
            if self.stage == 's3':
                cmd.append("pegasus-s3 put %s s3://admin@uweopenstack%s" % (inpath, dest))
            else:
                cmd.append("cp %s /glusterfs/users/mkhahmad/smwf_cap%s" % (inpath, dest))

        cmd_str = "\n".join(cmd)
        script="""
        #copy files
        %s
        """ % cmd_str
        #print cmd_str
        return script

    def generate_script(self):
        output_path = self.job['outpath']

        inputs_str = ""
        if ( self.job.has_key('input')):
            inputs = []
            for inp in self.job['input']:
                # inputs.append("cp %s/%s %s"%(output_path, inp, inp) )
                if self.stage == 's3':
                    inputs.append("pegasus-s3 get s3://admin@uweopenstack%s/%s %s" % (output_path, inp, inp))
                else:
                    inputs.append("cp /glusterfs/users/mkhahmad/smwf_cap%s/%s %s" % (output_path, inp, inp))
            inputs_str = "\n".join(inputs)

        outputs_str = ""
        if ( self.job.has_key('output')):
            outputs = []
            for out in self.job['output']:
                # outputs.append("cp %s %s/%s "%(out, output_path, out))
                if self.stage == 's3':
                    outputs.append("pegasus-s3 put %s s3://admin@uweopenstack%s/%s" % (out, output_path, out))
                else:
                    outputs.append("cp %s /glusterfs/users/mkhahmad/smwf_cap%s/%s" % (out, output_path, out))
            outputs_str = '\n'.join(outputs)

        exc = ""
        if ( self.job.has_key('exc')):
            excpath = None
            for exc in self.job['exc']:
                excpath = exc.replace("file://", "")
            exc = excpath

        script = """
        #set environment
        export %s
        #download required files
        %s

        #execute the job
        %s %s

        #upload outputs
        %s
        """ % (self.job['env'], inputs_str, exc, " ".join(self.job['args']), outputs_str)
        return script


class WorkflowManager(object):
    def __init__(self, wf, cloudprovider, **kwargs):
        #self.cwfid = kwargs['cwfid'] #this param is used for CAP-based execution
        self.wfgraph = wf
        self.wfnodes = self.wfgraph.nodes(data=True)
        self.cloudprovider = cloudprovider
        #self.provisioner = RandomProvision()
        self.provisioner = CAPProvisioner()
        self.runpath = kwargs['runpath']
        print self.runpath
        self.__init()

    def __init(self):
        #set the initial state for all jobs in this graph
        for n in self.wfgraph.nodes(data=True):
            n[1]['state'] = JobState.WAITING
            n[1]['retry'] = 3

    def status(self):
        print self.wfgraph.nodes(data=True)
        #print self.wfnodes

    def change_jobstate(self, jobs, state):
        for j in jobs:
            self.wfgraph.node[j]['state'] = state

    def get_readyjobs(self):
        ready_jobs=[]
        next_nodes = [n[0] for n in self.wfnodes if n[1]['state'] == JobState.WAITING]
        for j in next_nodes:
            parents = self.wfgraph.predecessors(j)
            cp=0
            for p in parents:
                #print 'check {} for {}'.format(p, j)
                pn = self.wfgraph.node[p]
                if ( pn['state'] == JobState.COMPLETE):
                    cp +=1
            if ( cp == len(parents)):
                #mark this job as ready
                ready_jobs.append(j)
                jn = self.wfgraph.node[j]
                jn['state'] = JobState.READY
        return ready_jobs

    def run(self):
        root = self.wfgraph.node['root']
        root['state'] = JobState.COMPLETE

        next_nodes = [n for n in self.wfgraph.neighbors('root')]
        completed_jobs = 1

        #print root
        #for n, nbrs in self.wfgraph.adjacency_iter():
        while completed_jobs < len(self.wfgraph.nodes()):
            print 'completed jobs {}'.format(completed_jobs)

            #this could be improved if there is a way to avoid already walked paths. nodes
            #next_nodes = [n for n in self.wfnodes if n[1]['state'] == JobState.COMPLETE]
            print 'next candidates {}'.format(next_nodes)
            ready_jobs = self.get_readyjobs()
            '''for j in next_nodes:
                parents = self.wfgraph.predecessors(j)
                cp=0
                for p in parents:
                    #print 'check {} for {}'.format(p, j)
                    pn = self.wfgraph.node[p]
                    if ( pn['state'] == JobState.COMPLETE):
                        cp +=1
                if ( cp == len(parents)):
                    #mark this job as ready
                    ready_jobs.append(j)
                    jn = self.wfgraph.node[j]
                    jn['state'] = JobState.READY
            '''
            print 'ready jobs {}'.format(ready_jobs)
            child_jobs = ready_jobs
            for cnode in ready_jobs:
                #child_jobs = self.wfgraph.neighbors(cnode[0])
                #marked the child as ready
                #self.changejobstate(child_jobs, JobState.READY)
                #print child_jobs
                #for c in child_jobs:

                job = self.wfgraph.node[cnode]
                job_exc = TaskSimulation(cnode, job)
                #change job state
                self.change_jobstate([ cnode ], JobState.PROCESSING)
                #do VM provisioing here and submission
                job_exc.start()

            #now monitor these jobs
            finished_jobs = 0

            while finished_jobs < len(child_jobs):
                #print 'child jobs {} and finsihed {}'.format(len(child_jobs), finished_jobs)
                for c in child_jobs:
                    #print ' monitoring {} job'.format(c)
                    job = self.wfgraph.node[c]
                    if ( job['state']==JobState.FINISH):
                        completed_jobs +=1
                        finished_jobs +=1
                        job['state'] = JobState.COMPLETE

                    print 'monitoring part is sleeping for a bit'
                    time.sleep(2)
            #self.status()
            #next_nodes = [n[0] for n in self.wfnodes if n[1]['state'] == JobState.WAITING]

        self.draw_executionorder("executionorder")

    def dynamic_run(self):
        root = self.wfgraph.node['root']
        root['state'] = JobState.COMPLETE
        completed_jobs = 1
        vm_map = self.get_cloudmap(self.wfgraph.graph['wfid'])
        print vm_map
        while completed_jobs < len(self.wfgraph.nodes()):
            print 'completed jobs {}'.format(completed_jobs)

            ready_jobs = self.get_readyjobs()
            print 'ready jobs {}'.format(ready_jobs)
            child_jobs = ready_jobs
            for cnode in ready_jobs:
                job = self.wfgraph.node[cnode]
                print job
                jid = job['jobid']
                vm = VirtualMachineSimulation(cnode, job)
                vm.set_resources(vm_map[jid][0], vm_map[jid][1], \
                                 vm_map[jid][2], vm_map[jid][3])
                #change job state
                self.change_jobstate([ cnode ], JobState.PROCESSING)
                #do VM provisioing here and submission
                vm.start()

            #now monitor these jobs
            finished_jobs = 0

            while finished_jobs < len(child_jobs):
                #print 'child jobs {} and finsihed {}'.format(len(child_jobs), finished_jobs)
                for c in child_jobs:
                    #print 'monitoring {} job'.format(c)
                    job = self.wfgraph.node[c]
                    if ( job['state']==JobState.FINISH):
                        completed_jobs +=1
                        finished_jobs +=1
                        job['state'] = JobState.COMPLETE

                    print 'monitoring part is sleeping for a bit'
                    time.sleep(2)
                    #self.status()
                    #next_nodes = [n[0] for n in self.wfnodes if n[1]['state'] == JobState.WAITING]

        self.draw_executionorder("executionorder_dynamic", True)

    def dynamic_vm_run(self):
        root = self.wfgraph.node['root']
        root['state'] = JobState.COMPLETE
        completed_jobs = 1
        vm_map = self.get_cloudmap(self.wfgraph.graph['wfid'])
        print vm_map
        while completed_jobs < len(self.wfgraph.nodes()):
            print 'completed jobs {}'.format(completed_jobs)

            ready_jobs = self.get_readyjobs()
            print 'ready jobs {}'.format(ready_jobs)
            child_jobs = ready_jobs
            for cnode in ready_jobs:
                job = self.wfgraph.node[cnode]
                print job
                jid = job['jobid']
                vm = VirtualMachineExecution(name=cnode, job=job)
                vm.set_resources(vm_map[jid][0], vm_map[jid][1], \
                             vm_map[jid][2], vm_map[jid][3])
                #change job state
                self.change_jobstate([ cnode ], JobState.PROCESSING)
                #do VM provisioing here and submission
                vm.start()

            #now monitor these jobs
            finished_jobs = 0

            while finished_jobs < len(child_jobs):
                #print 'child jobs {} and finsihed {}'.format(len(child_jobs), finished_jobs)
                for c in child_jobs:
                    #print 'monitoring {} job'.format(c)
                    job = self.wfgraph.node[c]
                    if ( job['state']==JobState.FINISH):
                        completed_jobs +=1
                        finished_jobs +=1
                        job['state'] = JobState.COMPLETE

                    print 'monitoring part is sleeping for a bit'
                    time.sleep(2)
                    #self.status()
                    #next_nodes = [n[0] for n in self.wfnodes if n[1]['state'] == JobState.WAITING]

        self.draw_executionorder("executionorder_dynamic", True)

    def test_run(self):
        '''for actual VM test'''
        root = self.wfgraph.node['root']
        root['state'] = JobState.COMPLETE
        wf_env = root['env']

        completed_jobs = 1
        ABORT_WF = False
        while (completed_jobs < len(self.wfgraph.nodes())) and not ABORT_WF:
            print 'completed jobs {}'.format(completed_jobs)

            ready_jobs = self.get_readyjobs()
            print 'ready jobs {}'.format(ready_jobs)
            child_jobs = ready_jobs
            for cnode in ready_jobs:
                job = self.wfgraph.node[cnode]
                job['env'] = wf_env
                job['flavor'] = self.provisioner.get_resource(job['jname'])
                print job['flavor'], ' found for ', job['jname']
                # print job
                '''process stage-in, create-dir locally '''
                if ( job['type']=='stage-in' and self.cloudprovider == CloudProvider.UWE):
                    self.process_stagein(job)
                    self.change_jobstate([cnode], JobState.FINISH)
                    continue

                elif (job['type']=='createdir' and self.cloudprovider == CloudProvider.UWE):
                    print 'process %s locally' % job['type']
                    self.process_createdir(job)
                    self.change_jobstate([cnode], JobState.FINISH)
                    continue

                vm = TestExecution(job)
                # change job state
                self.change_jobstate([cnode], JobState.PROCESSING)
                # do VM provisioning here and submission
                vm.start()

            # now monitor these jobs
            finished_jobs = 0

            while (finished_jobs < len(child_jobs)) and not ABORT_WF:
                # print 'child jobs {} and finished {}'.format(len(child_jobs), finished_jobs)
                for c in child_jobs:
                    # print 'monitoring {} job'.format(c)
                    job = self.wfgraph.node[c]
                    if job['state'] == JobState.FINISH:
                        completed_jobs +=1
                        finished_jobs +=1
                        job['state'] = JobState.COMPLETE
                    elif job['state'] == JobState.FAIL:
                        # abort workflow
                        ABORT_WF = True

                    print 'monitoring part is sleeping for a bit'
                    time.sleep(2)


    def process_stagein(self, job):
        #for OSDC settings, process stage-in and create-dir in osdc-vm1, so find a way to do it inside a VM via SSH
        #FOR UWE settings, do it on robb locally
        try:
            job['status'] = JobState.RUNNING
            import shutil
            for inp, path in job['input'].items():

                inpath = path[0].replace("file://","")
                dest = "%s/%s"% (job['outpath'], inp)
                print dest
                shutil.copy(inpath,dest )
            job['status'] = JobState.FINISH
        except:
            print 'error in copying file: %s' % sys.exc_info()[1]
            job['status'] = JobState.FAIL

    def process_createdir(self, job):
        try:
            job['status'] = JobState.RUNNING
            path_d = "%s/%s" % (self.runpath, job['outpath'])
            if ( not os.path.exists(path_d)):
                print 'creating', path_d

                os.makedirs(path_d)

            job['status'] = JobState.FINISH
        except:
            print 'error in creating directory'
            job['status'] = JobState.FAIL

    def get_cloudmap(self, wfid):
        UWE_mips = 15321
        SQL="""SELECT jobid, vCPU, minRAM, minHD from WfCloudMapping
        where wfid in (SELECT wfID from WorkflowSource where peg_wfid=%s)
        """ % wfid
        cloudb = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['cloudprov_settings'])
        conn = cloudb.get_connection()
        records = cloudb.execute_query({'sql': SQL})
        d={}
        for r in records:
            d[r['jobid']]=[r['vCPU'], r['minRAM'], r['minHD'], UWE_mips]

        cloudb.close_connection()
        return d

    def provenance(self):
        for job in self.wfgraph.nodes(data=True):
            print "provenance {}".format(job)

    def draw_executionorder(self, fig_name, dynamic=False):
        #fig_name="executionorder"
        fig_title="Job Execution order"
        mpl.rcParams['font.size']=12
        plt.figure(figsize=(10, 16), dpi=300)

        d = {}
        for n in self.wfnodes:
            if ( n[0]=='root'):
                continue
            #print n[1]
            #d[n[0]] = [get_ctime(n[1]['st']) - get_ctime(n[1]['fn'])]
            if (dynamic):
                d[n[0]] = [util.get_ctime(n[1]['s_time']), util.get_ctime(n[1]['f_time']), n[1]['vm_boot']]
            else:
                d[n[0]] = [util.get_ctime(n[1]['s_time']), util.get_ctime(n[1]['f_time'])]

        df = pd.DataFrame.from_dict(d, "index")
        if ( dynamic ):
            df.columns=['Start','Finish', 'Boot']
        else:
            df.columns=['Start','Finish']

        df = df.sort_index(by='Start')
        print df
        ax = df.plot(kind='bar', legend=True)
        #ax = df.plot(kind='scatter', x='a', y='b', legend=True)
        labels = ax.get_xticklabels()
        for label in labels:
            #help(label)
            label.set_fontsize(9)
            label.set_rotation('horizontal')

        ylabels = ax.get_yticklabels()
        for label in ylabels:
            #help(label)
            label.set_fontsize(9)

        ax.set_xlabel("Jobs")
        ax.set_ylabel("Job Time (secs)")
        ax.set_title(fig_title)
        plt.xticks(rotation=15)
        plt.savefig("%s.pdf" % fig_name, format="PDF")



