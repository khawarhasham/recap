from kwms.provision.base import ProvisionBase
from random import randint

__author__ = 'khawar'


class RandomProvision(ProvisionBase):
    def __init__(self):
        ProvisionBase.__init__(self)

    def get_resource(self, jid=0, **kwargs):
        c = randint(0, 3)
        return self.flavors[c]


if __name__=='__main__':
    p = RandomProvision()
    print p.get_resource()
