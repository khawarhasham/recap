from kwms.provision.base import ProvisionBase

__author__ = 'khawar'

class CAPProvisioner(ProvisionBase):
    def __init__(self):
        ProvisionBase.__init__(self)
        #use it for dynamic run purpose. TODO: change it with actual db values
        self.jobmap ={'merge':['merge','m1.small'],
                      'preprocess':['split', 'm1.small'],
                      'analyse':['analyse1', 'm1.small'],
                      'analyse':['analyse2', 'm1.small'],}

    def get_resource(self, jid, **kwargs):

        return self.jobmap[jid][1]  if self.jobmap.has_key(jid) else None