from SpiffWorkflow.specs import *
from SpiffWorkflow import Workflow
from SpiffWorkflow import Task

__author__ = 'khawar'
spec = WorkflowSpec()
# (Add tasks to the spec here.)

task1    = Simple(spec, 'Simple 1')
task2    = Simple(spec, 'Simple 2')
task3    = Simple(spec, 'Simple 3')
task4    = Simple(spec, 'Simple 4')

wf = Workflow(spec)
root     = Task(wf, task1)
c1       = root._add_child(task2)
c11      = c1._add_child(task3)
c111     = c11._add_child(task4)
c3       = Task(wf, task4, root)
c3.state = Task.COMPLETED

print wf.get_tasks(Task.WAITING)
for thetask in Task.Iterator(root, Task.MAYBE):
    print thetask.get_dump(0,False)