from Pegasus.DAX3 import *
import networkx as nx

from pprint import pprint
import time

__author__ = 'khawar'

class DAGParser(object):
    def __init__(self, dax_file, output=None):
        print dax_file
        self.dax_file = dax_file
        self.adag = parse(self.dax_file)
        '''this is for Pegasus job parsing. in database jobname from job table is jobname_jobid '''
        self.jobs_desc = None

        data_out = "%s"%time.time()
        data_out = data_out[:data_out.find(".")]
        self.output = "%s/%s"% (output, data_out)

    def get_files(self):
        files = {}
        for f in self.adag.files:
            files[f.name]=[]
            for pfn in f.pfns:
                files[f.name].append( pfn.url)

        #print files
        return files

    def get_deps(self):
        job_deps={}
        for d in self.adag.dependencies:
            #print d.parent, ' of ', d.child
            if ( not job_deps.has_key(d.parent)):
                job_deps[d.parent]=[]
            job_deps[d.parent].append(d.child)

        #pprint(job_deps)
        return job_deps

    def get_job_args(self, peg_db_jobname):
        '''
        this is for Pegasus job parsing. in database jobname from job table is jobname_jobid
        '''
        if (self.jobs_desc.has_key(peg_db_jobname)):
            return self.jobs_desc[peg_db_jobname]
        return None

    def get_job_desc(self):
        if not self.jobs_desc:
            self.get_jobs()
        return self.jobs_desc

    def get_jobs(self):
        jobs = {}
        self.jobs_desc = {}
        for jid in self.adag.jobs.keys():
            jobs[jid] = {}
            job = self.adag.getJob(jid)
            #print job.toXML()
            jobs[jid]['name'] = job.name
            jobs[jid]['uses'] = {}
            '''THINK: DONOT treat uses with executable=true as input files'''
            for use in job.used:
                #print use
                if ( not jobs[jid]['uses'].has_key(use.link)):
                    jobs[jid]['uses'][use.link]=[]

                jobs[jid]['uses'][use.link].append(use.name)

            args=[]
            for a in job.arguments:
                if isinstance(a, File):
                    #print a.name
                    #print a.pfns
                    args.append(a.name)
                else:
                    args.append(a)
            jobs[jid]['args'] = args
            self.jobs_desc["%s_%s"%(job.name, jid)] = {'uses':jobs[jid]['uses'], 'args':jobs[jid]['args']}
        #pprint (jobs)
        return jobs

    def get_executables(self):
        execs = {}
        for exc in self.adag.executables:
            execs[exc.name]=[]
            for pfn in exc.pfns:
                execs[exc.name].append(pfn.url)

        #pprint(execs)
        return execs

    def generate_dag_graph(self, SAVE=False):
        '''This will generate Graph object from nx framework using the DAG'''
        jobs = self.get_jobs()
        executables = self.get_executables()
        G=nx.DiGraph(wfid=self.adag.name)

        for n,v in jobs.items():
            #print v

            if executables.has_key(v['name']):
                exec_n = executables[v['name']]
            else:
                exec_n = v['name']
            #for montage change n with v['name']
            n = "%s_%s" % (v['name'], n)
            inputs = v['uses']['input'] if v['uses'].has_key('input') else []
            outputs = v['uses']['output'] if v['uses'].has_key('output') else []

            #G.add_node(n, jname=v['name'], type='comp', exc=exec_n, \
            #           args=v['args'], input=v['uses']['input'], output=v['uses']['output'])
            G.add_node(n, jname=v['name'], type='comp', exc=exec_n, \
                       args=v['args'], input=inputs, output=outputs)

        deps = self.get_deps()
        first_parent = None
        for parent, childs in deps.items():
            #for montage add two lines
            parentjob = jobs[parent]
            parent = "%s_%s" % (parentjob['name'], parent)
            if ( not first_parent):
                first_parent = parent
            for child in childs:
                childjob = jobs[child]
                #print childjob
                #for montage
                child = "%s_%s" % (childjob['name'], child)
                G.add_edge(parent, child)
        if SAVE:
            gplt = nx.to_pydot(G)
            gplt.write("./test_DAGSRC_%s.pdf" % self.adag.name, format="pdf")
        return G

    def generate_graph(self):
        init_files = self.get_files()
        jobs = self.get_jobs()
        executables = self.get_executables()

        #add wfid here
        G=nx.DiGraph(wfid=self.adag.name)
        G.add_node('root', env="PYTHONPATH=/glusterfs/users/mkhahmad/pegasus_jobs/wordcount:/opt/work_area/pegasus_jobs/wordcount:$PYTHONPATH")

        G.add_node("create-dir", jname="create-dir", type='createdir', outpath=self.output)
        G.add_edge('root','create-dir')

        #add first stage-in jobs
        #for name, files in init_files.items():
        #files = ','.join(files)
        G.add_node("stage-in", jname="stage-in", type='stage-in', args=[], input=init_files, outpath=self.output)
        G.add_edge('create-dir', 'stage-in')

        for n,v in jobs.items():
            #print v
            if executables.has_key(v['name']):
                exec_n = executables[v['name']]
            else:
                exec_n = v['name']


            G.add_node(n, jname=v['name'], type='comp', exc=exec_n, \
                       args=v['args'], input=v['uses']['input'], output=v['uses']['output'], outpath=self.output)

        deps = self.get_deps()
        first_parent = None
        for parent, childs in deps.items():
            if ( not first_parent):
                first_parent = parent
            for child in childs:
                G.add_edge(parent, child)


        #add stage-in job at top
        G.add_edge('stage-in', first_parent)
        gplt = nx.to_pydot(G)
        gplt.write("./test%s.pdf" % self.adag.name, format="pdf")
        return G


if __name__ == '__main__':
    parser = DAGParser('/Users/khawar/Documents/CloudProv/CloudProv/MultiLayerProv/multilayer/client/montage.xml', \
                        '/opt/parallel_jobs/')
    #parser = DAGParser('/var/folders/xf/6l0zggkj3dn4lp0gwl_9y10c0000gr/T/tmp2svtyc.xml', '/aa')
    #parser.get_files()
    dax_jobs = parser.get_jobs()
    #parser.get_executables()
    #dependencies
    #parser.get_deps()
    '''generate graph for KWMS working'''
    #parser.generate_graph()
    '''get dag description in graph object'''
    parser.generate_dag_graph(True)
    #print parser.get_job_desc()