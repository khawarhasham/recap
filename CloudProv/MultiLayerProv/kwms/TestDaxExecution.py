from KWorkflowManager import CloudProvider
from KWorkflowManager import WorkflowManager
from DAGParser import DAGParser
import os

__author__ = 'khawar'

'''execute this file to run dax '''
parser = DAGParser(os.path.expanduser('osdc_wordcount.dax'), \
                   '')
graph = parser.generate_graph()
wms = WorkflowManager(graph, CloudProvider.OSDC, runpath="Test")
wms.test_run()
wms.provenance()
