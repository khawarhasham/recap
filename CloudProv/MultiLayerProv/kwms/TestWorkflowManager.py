import networkx as nx
from multilayer.persistency import PersistencyFactory
import multilayer.Defaults as Defaults
from KWorkflowManager import WorkflowManager
from KWorkflowManager import CloudProvider
import util

__author__ = 'khawar'

mysqlobj = PersistencyFactory.getPersistentObj("SqlAlchemyApi", Defaults.config['db_settings'])
mysqlobj.get_connection()

wfid=161
G=nx.DiGraph(wfid=wfid)
G.add_node("root")

SQL="""SELECT parent_abs_task_id as p, child_abs_task_id as c, job_id
    FROM task_edge, job
    where task_edge.wf_id=job.wf_id and type_desc='compute' and task_edge.wf_id=%s""" % wfid

result = mysqlobj.execute_query({'sql':SQL})
first = None

for r in result:
    if ( not first ):
        first = r['p']
    #print G.add_node(r['parent_abs_task_id'])
    G.add_node(r['p'], process=util.ss(r['p']), jobid=r['job_id'])
    G.add_node(r['c'], process=util.ss(r['c']), jobid=r['job_id'])
    G.add_edge(r['p'], r['c'])

mysqlobj.close_connection()

'''G.add_node('node5', process=ss('node5'))
G.add_node('node6', process=ss('node6'))
G.add_node('node7', process=ss('node7'))
G.add_node('node8', process=ss('node8'))
G.add_node('node9', process=ss('node9'))
#G.add_node('fazool', process=ss('fazool'))

G.add_edge('ID0000004', "node5")
G.add_edge('ID0000004', "node6")
G.add_edge('node6', "node7")
G.add_edge('node7', "node8")
G.add_edge('node5', "node8")
G.add_edge('ID0000001', "node9")
G.add_edge('node9', "node6")'''

G.add_edge("root", first)


#print G.graph['wfid']
print G.number_of_nodes()
print G.number_of_edges()
#print G.nodes(data=True)
#print G.edges()

util.draw_graph(G, wfid)
nodes = G.nodes(data=True)
#print nx.bfs_successors(G, 'root')
#traverse('root', G)
wf = WorkflowManager(G, CloudProvider.UWE)
wf.status()
'''simple execution'''
#wf.run()
'''dynamic (vm) execution'''
wf.dynamic_run()

'''dynamic vm (actual) execution '''
#wf.dynamic_vm_run()
wf.provenance()