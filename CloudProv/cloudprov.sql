-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cloudprov
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CPUSpecs`
--

DROP TABLE IF EXISTS `CPUSpecs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CPUSpecs` (
  `wfID` int(11) NOT NULL,
  `jobid` int(11) DEFAULT NULL,
  `hostid` int(11) DEFAULT NULL,
  `count` varchar(45) DEFAULT NULL,
  `vendor` varchar(45) DEFAULT NULL,
  `speed` varchar(45) DEFAULT NULL,
  `processor` varchar(45) DEFAULT NULL,
  `mips` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`wfID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CPUSpecs`
--

LOCK TABLES `CPUSpecs` WRITE;
/*!40000 ALTER TABLE `CPUSpecs` DISABLE KEYS */;
/*!40000 ALTER TABLE `CPUSpecs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CloudFileCatalog`
--

DROP TABLE IF EXISTS `CloudFileCatalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CloudFileCatalog` (
  `cloudFileID` int(11) NOT NULL,
  `container_name` varchar(45) DEFAULT NULL,
  `keyname` varchar(45) DEFAULT NULL,
  `metadata` text,
  `hash` varchar(45) DEFAULT NULL,
  `creation_date` varchar(45) DEFAULT NULL,
  `modified_date` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cloudFileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CloudFileCatalog`
--

LOCK TABLES `CloudFileCatalog` WRITE;
/*!40000 ALTER TABLE `CloudFileCatalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `CloudFileCatalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JobCloudFile`
--

DROP TABLE IF EXISTS `JobCloudFile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JobCloudFile` (
  `wfID` int(11) NOT NULL,
  `jobid` int(11) NOT NULL,
  `cloudFileID` int(11) NOT NULL,
  KEY `fk_JobCloudFiles_CloudFileCatalog_idx` (`cloudFileID`),
  KEY `fk_JobCloudFile_WfCloudMapping1_idx` (`wfID`,`jobid`),
  CONSTRAINT `fk_JobCloudFiles_CloudFileCatalog` FOREIGN KEY (`cloudFileID`) REFERENCES `CloudFileCatalog` (`cloudFileID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_JobCloudFile_WfCloudMapping1` FOREIGN KEY (`wfID`, `jobid`) REFERENCES `WfCloudMapping` (`wfID`, `jobid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JobCloudFile`
--

LOCK TABLES `JobCloudFile` WRITE;
/*!40000 ALTER TABLE `JobCloudFile` DISABLE KEYS */;
/*!40000 ALTER TABLE `JobCloudFile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JobHostTempMap`
--

DROP TABLE IF EXISTS `JobHostTempMap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JobHostTempMap` (
  `wfID` int(11) DEFAULT NULL,
  `jobid` varchar(255) DEFAULT NULL,
  `hostip` varchar(45) DEFAULT NULL,
  `hostname` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JobHostTempMap`
--

LOCK TABLES `JobHostTempMap` WRITE;
/*!40000 ALTER TABLE `JobHostTempMap` DISABLE KEYS */;
/*!40000 ALTER TABLE `JobHostTempMap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RepeatWorkflow`
--

DROP TABLE IF EXISTS `RepeatWorkflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RepeatWorkflow` (
  `origWfID` int(11) NOT NULL,
  `repeatWfID` int(11) NOT NULL,
  `repeatStatus` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`origWfID`,`repeatWfID`),
  KEY `fk_RepeatWorkflow_WorkflowSource1_idx` (`origWfID`),
  KEY `fk_RepeatWorkflow_WorkflowSource2_idx` (`repeatWfID`),
  CONSTRAINT `fk_RepeatWorkflow_WorkflowSource1` FOREIGN KEY (`origWfID`) REFERENCES `WorkflowSource` (`wfID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_RepeatWorkflow_WorkflowSource2` FOREIGN KEY (`repeatWfID`) REFERENCES `WorkflowSource` (`wfID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RepeatWorkflow`
--

LOCK TABLES `RepeatWorkflow` WRITE;
/*!40000 ALTER TABLE `RepeatWorkflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `RepeatWorkflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WfCloudMapping`
--

DROP TABLE IF EXISTS `WfCloudMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WfCloudMapping` (
  `wfID` int(11) NOT NULL,
  `jobid` int(11) NOT NULL,
  `hostid` int(11) DEFAULT NULL,
  `nodename` varchar(45) DEFAULT NULL,
  `hostip` varchar(45) DEFAULT NULL,
  `middleware` varchar(45) DEFAULT NULL,
  `minRAM` varchar(45) DEFAULT NULL,
  `minHD` varchar(45) DEFAULT NULL,
  `vCPU` varchar(45) DEFAULT NULL,
  `flavorid` varchar(45) DEFAULT NULL,
  `flavorname` varchar(45) DEFAULT NULL,
  `image_name` varchar(45) DEFAULT NULL,
  `image_id` varchar(45) DEFAULT NULL,
  `extra` text,
  PRIMARY KEY (`wfID`,`jobid`),
  KEY `fk_WfCloudMapping_WorkflowSource1_idx` (`wfID`),
  CONSTRAINT `fk_WfCloudMapping_WorkflowSource1` FOREIGN KEY (`wfID`) REFERENCES `WorkflowSource` (`wfID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WfCloudMapping`
--

LOCK TABLES `WfCloudMapping` WRITE;
/*!40000 ALTER TABLE `WfCloudMapping` DISABLE KEYS */;
INSERT INTO `WfCloudMapping` VALUES (6,6362,689,'mon1.novalocal','172.16.1.98','OpenStack','2048','20','1','2','m1.small','montage-condor-setup','2d9787e4-b0e9-4802-bf4f-4a0c868bb11a','{}'),(6,6367,689,'mon1.novalocal','172.16.1.98','OpenStack','2048','20','1','2','m1.small','montage-condor-setup','2d9787e4-b0e9-4802-bf4f-4a0c868bb11a','{}');
/*!40000 ALTER TABLE `WfCloudMapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WfCloudTempMapping`
--

DROP TABLE IF EXISTS `WfCloudTempMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WfCloudTempMapping` (
  `wfID` int(11) NOT NULL,
  `jobid` int(11) NOT NULL,
  `hostname` varchar(45) DEFAULT NULL,
  `hostip` varchar(45) DEFAULT NULL,
  `middleware` varchar(45) DEFAULT NULL,
  `minRAM` varchar(45) DEFAULT NULL,
  `minHD` varchar(45) DEFAULT NULL,
  `vCPU` varchar(45) DEFAULT NULL,
  `flavorid` varchar(45) DEFAULT NULL,
  `flavorname` varchar(45) DEFAULT NULL,
  `image_name` varchar(45) DEFAULT NULL,
  `image_id` varchar(45) DEFAULT NULL,
  `extra` text,
  PRIMARY KEY (`wfID`,`jobid`),
  KEY `fk_WfCloudMapping_WorkflowSource1_idx` (`wfID`),
  CONSTRAINT `fk_WfCloudMapping_WorkflowSource10` FOREIGN KEY (`wfID`) REFERENCES `WorkflowSource` (`wfID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WfCloudTempMapping`
--

LOCK TABLES `WfCloudTempMapping` WRITE;
/*!40000 ALTER TABLE `WfCloudTempMapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `WfCloudTempMapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WorkflowSource`
--

DROP TABLE IF EXISTS `WorkflowSource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WorkflowSource` (
  `wfID` int(11) NOT NULL AUTO_INCREMENT,
  `wfSubmit` text,
  `wfDAG` text,
  `wfSite` text,
  `wfTC` text COMMENT 'The information in this table will enable a user to re-execute a workflow by fetching and executing the workflow definition/submission files ',
  `wfProps` text,
  `wms_wfid` int(11) DEFAULT NULL,
  `peg_wfid` int(11) DEFAULT NULL,
  PRIMARY KEY (`wfID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WorkflowSource`
--

LOCK TABLES `WorkflowSource` WRITE;
/*!40000 ALTER TABLE `WorkflowSource` DISABLE KEYS */;
INSERT INTO `WorkflowSource` VALUES (1,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- generated: 2014-11-30 23:02:07.539219 -->\n<!-- generated by: khawar -->\n<!-- generator: python -->\n<adag xmlns=\"http://pegasus.isi.edu/schema/DAX\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/DAX http://pegasus.isi.edu/schema/dax-3.4.xsd\" version=\"3.4\"\nname=\"WordCountWF\">\n	<invoke when=\"at_end\">/usr/bin/mail -s &quot;Workflow finished&quot; khawarhasham@gmail.com</invoke>\n	<invoke when=\"on_success\">/usr/bin/mail -s &quot;publish result&quot; khawarhasham@gmail.com</invoke>\n	<file name=\"wordlist\">\n		<pfn url=\"file:///opt/parallel_jobs/wordfile\" site=\"local\"/>\n	</file>\n	<executable name=\"merge\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/merge.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"preprocess\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"analyse\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/analyse.py\" site=\"condorpool\"/>\n	</executable>\n	<job id=\"ID0000001\" namespace=\"wordcount\" name=\"preprocess\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist\"/> -o <file name=\"wordlist1\"/> <file name=\"wordlist2\"/></argument>\n		<uses name=\"wordlist2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000002\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist1\"/> -o <file name=\"analysis1\"/></argument>\n		<uses name=\"analysis1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000003\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist2\"/> -o <file name=\"analysis2\"/></argument>\n		<uses name=\"wordlist2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000004\" namespace=\"wordcount\" name=\"merge\" version=\"1.0\">\n		<argument>-i <file name=\"analysis1\"/> <file name=\"analysis2\"/> -o <file name=\"merge_output\"/></argument>\n		<uses name=\"analysis1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"merge_output\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<invoke when=\"at_end\">/home/ubuntu/peg_mywork/pegasus_notifier</invoke>\n	</job>\n	<child ref=\"ID0000002\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000003\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000004\">\n		<parent ref=\"ID0000002\"/>\n		<parent ref=\"ID0000003\"/>\n	</child>\n</adag>\n','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n    <sitecatalog xmlns=\"http://pegasus.isi.edu/schema/sitecatalog\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd\" version=\"3.0\">\n    <site  handle=\"local\" arch=\"x86\" os=\"LINUX\">\n    <head-fs>\n    <scratch>\n    <shared>\n    <file-server protocol=\"file\" url=\"file://\" mount-point=\"./work\"/>\n    <internal-mount-point mount-point=\"./work\"/>\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"file\" url=\"file://\" mount-point=\"./outputs\"/>\n<internal-mount-point mount-point=\"./outputs\"/>\n</shared>\n<!--shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018142702\"/>\n<internal-mount-point mount-point=\"./outputs\" />\n</shared -->\n</storage>\n</head-fs>\n<profile namespace=\"env\" key=\"S3CFG\">/home/khawar/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/home/khawar/pegasus-4.2.0</profile>\n</site>\n<site handle=\"s3\" arch=\"x86_64\" os=\"LINUX\">\n<head-fs>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018142702\"/>\n<internal-mount-point mount-point=\"/wfoutput11022018142702\" />\n</shared>\n</storage>\n</head-fs>\n</site>\n<site  handle=\"condorpool\" arch=\"x86\" os=\"LINUX\">\n<head-fs>\n<scratch >\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wflocalscratch11022018142702\"/>\n<internal-mount-point mount-point=\"/wflocalscratch11022018142702\" />\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfshared11022018142702\"/>\n<internal-mount-point mount-point=\"/wfshared11022018142702\" />\n</shared>\n</storage>\n</head-fs>\n<profile namespace=\"pegasus\" key=\"style\" >condor</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/opt/pegasus/default</profile>\n<profile namespace=\"env\" key=\"S3CFG\">/opt/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PYTHONPATH\">/opt/parallel_jobs/peg_jobs/wordcount:/opt/parallel_jobs/peg_jobs/dummywf:/opt/parallel_jobs/peg_jobs/dynamic_req</profile>\n<profile namespace=\"condor\" key=\"universe\">vanilla</profile>\n<profile namespace=\"condor\" key=\"requirements\" >(Target.Arch == \"X86_64\")</profile>\n</site>\n</sitecatalog>',NULL,'pegasus.dir.storage.deep=false\n        pegasus.catalog.replica.db.url=jdbc:mysql://localhost:3306/pegasusdb\n        pegasus.catalog.transformation=File\n        dagman.maxpre=2\n        pegasus.home.schemadir=/home/ubuntu/pegasus-4.2/share/pegasus/schema\n        pegasus.execute.*.filesystem.local=true\n        pegasus.condor.logs.symlink=false\n        pegasus.monitord.events=true\n        pegasus.catalog.transformation.file=/home/ubuntu/pegasus_service/peg_files/tc.data\n        pegasus.data.configuration=nonsharedfs\n        pegasus.catalog.provenance=InvocationSchema\n        pegasus.catalog.site.file=/home/ubuntu/WrapperService/peg_files/tmpzZCxpH/sites.xml\n        pegasus.gridstart=PegasusLite\n        pegasus.catalog.site=XML3\n        pegasus.monitord.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.home.sysconfdir=/home/ubuntu/pegasus-4.2/etc\n        pegasus.catalog.replica.db.password=oxy123\n        pegasus.catalog.replica.db.driver=MySQL\n        pegasus.home.bindir=/home/khawar/pegasus-4.2/bin\n        pegasus.transfer.worker.package=true\n        pegasus.home.sharedstatedir=/home/ubuntu/pegasus-4.2.0/share/pegasus\n        pegasus.dashboard.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.catalog.replica.db.user=pegasus',NULL,NULL),(2,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- generated: 2014-11-30 23:02:07.539219 -->\n<!-- generated by: khawar -->\n<!-- generator: python -->\n<adag xmlns=\"http://pegasus.isi.edu/schema/DAX\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/DAX http://pegasus.isi.edu/schema/dax-3.4.xsd\" version=\"3.4\"\nname=\"WordCountWF\">\n	<invoke when=\"at_end\">/usr/bin/mail -s &quot;Workflow finished&quot; khawarhasham@gmail.com</invoke>\n	<invoke when=\"on_success\">/usr/bin/mail -s &quot;publish result&quot; khawarhasham@gmail.com</invoke>\n	<file name=\"wordlist\">\n		<pfn url=\"file:///opt/parallel_jobs/wordfile\" site=\"local\"/>\n	</file>\n	<executable name=\"merge\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/merge.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"preprocess\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"analyse\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/analyse.py\" site=\"condorpool\"/>\n	</executable>\n	<job id=\"ID0000001\" namespace=\"wordcount\" name=\"preprocess\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist\"/> -o <file name=\"wordlist1\"/> <file name=\"wordlist2\"/></argument>\n		<uses name=\"wordlist2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000002\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist1\"/> -o <file name=\"analysis1\"/></argument>\n		<uses name=\"analysis1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000003\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist2\"/> -o <file name=\"analysis2\"/></argument>\n		<uses name=\"wordlist2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000004\" namespace=\"wordcount\" name=\"merge\" version=\"1.0\">\n		<argument>-i <file name=\"analysis1\"/> <file name=\"analysis2\"/> -o <file name=\"merge_output\"/></argument>\n		<uses name=\"analysis1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"merge_output\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<invoke when=\"at_end\">/home/ubuntu/peg_mywork/pegasus_notifier</invoke>\n	</job>\n	<child ref=\"ID0000002\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000003\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000004\">\n		<parent ref=\"ID0000002\"/>\n		<parent ref=\"ID0000003\"/>\n	</child>\n</adag>\n','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n    <sitecatalog xmlns=\"http://pegasus.isi.edu/schema/sitecatalog\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd\" version=\"3.0\">\n    <site  handle=\"local\" arch=\"x86\" os=\"LINUX\">\n    <head-fs>\n    <scratch>\n    <shared>\n    <file-server protocol=\"file\" url=\"file://\" mount-point=\"./work\"/>\n    <internal-mount-point mount-point=\"./work\"/>\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"file\" url=\"file://\" mount-point=\"./outputs\"/>\n<internal-mount-point mount-point=\"./outputs\"/>\n</shared>\n<!--shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018143136\"/>\n<internal-mount-point mount-point=\"./outputs\" />\n</shared -->\n</storage>\n</head-fs>\n<profile namespace=\"env\" key=\"S3CFG\">/home/khawar/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/home/khawar/pegasus-4.2.0</profile>\n</site>\n<site handle=\"s3\" arch=\"x86_64\" os=\"LINUX\">\n<head-fs>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018143136\"/>\n<internal-mount-point mount-point=\"/wfoutput11022018143136\" />\n</shared>\n</storage>\n</head-fs>\n</site>\n<site  handle=\"condorpool\" arch=\"x86\" os=\"LINUX\">\n<head-fs>\n<scratch >\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wflocalscratch11022018143136\"/>\n<internal-mount-point mount-point=\"/wflocalscratch11022018143136\" />\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfshared11022018143136\"/>\n<internal-mount-point mount-point=\"/wfshared11022018143136\" />\n</shared>\n</storage>\n</head-fs>\n<profile namespace=\"pegasus\" key=\"style\" >condor</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/opt/pegasus/default</profile>\n<profile namespace=\"env\" key=\"S3CFG\">/opt/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PYTHONPATH\">/opt/parallel_jobs/peg_jobs/wordcount:/opt/parallel_jobs/peg_jobs/dummywf:/opt/parallel_jobs/peg_jobs/dynamic_req</profile>\n<profile namespace=\"condor\" key=\"universe\">vanilla</profile>\n<profile namespace=\"condor\" key=\"requirements\" >(Target.Arch == \"X86_64\")</profile>\n</site>\n</sitecatalog>',NULL,'pegasus.dir.storage.deep=false\n        pegasus.catalog.replica.db.url=jdbc:mysql://localhost:3306/pegasusdb\n        pegasus.catalog.transformation=File\n        dagman.maxpre=2\n        pegasus.home.schemadir=/home/ubuntu/pegasus-4.2/share/pegasus/schema\n        pegasus.execute.*.filesystem.local=true\n        pegasus.condor.logs.symlink=false\n        pegasus.monitord.events=true\n        pegasus.catalog.transformation.file=/home/ubuntu/pegasus_service/peg_files/tc.data\n        pegasus.data.configuration=nonsharedfs\n        pegasus.catalog.provenance=InvocationSchema\n        pegasus.catalog.site.file=/home/ubuntu/WrapperService/peg_files/tmpPU6Vih/sites.xml\n        pegasus.gridstart=PegasusLite\n        pegasus.catalog.site=XML3\n        pegasus.monitord.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.home.sysconfdir=/home/ubuntu/pegasus-4.2/etc\n        pegasus.catalog.replica.db.password=oxy123\n        pegasus.catalog.replica.db.driver=MySQL\n        pegasus.home.bindir=/home/khawar/pegasus-4.2/bin\n        pegasus.transfer.worker.package=true\n        pegasus.home.sharedstatedir=/home/ubuntu/pegasus-4.2.0/share/pegasus\n        pegasus.dashboard.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.catalog.replica.db.user=pegasus',NULL,NULL),(3,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- generated: 2014-11-30 23:02:07.539219 -->\n<!-- generated by: khawar -->\n<!-- generator: python -->\n<adag xmlns=\"http://pegasus.isi.edu/schema/DAX\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/DAX http://pegasus.isi.edu/schema/dax-3.4.xsd\" version=\"3.4\"\nname=\"WordCountWF\">\n	<invoke when=\"at_end\">/usr/bin/mail -s &quot;Workflow finished&quot; khawarhasham@gmail.com</invoke>\n	<invoke when=\"on_success\">/usr/bin/mail -s &quot;publish result&quot; khawarhasham@gmail.com</invoke>\n	<file name=\"wordlist\">\n		<pfn url=\"file:///opt/parallel_jobs/wordfile\" site=\"local\"/>\n	</file>\n	<executable name=\"merge\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/merge.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"preprocess\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"analyse\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/analyse.py\" site=\"condorpool\"/>\n	</executable>\n	<job id=\"ID0000001\" namespace=\"wordcount\" name=\"preprocess\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist\"/> -o <file name=\"wordlist1\"/> <file name=\"wordlist2\"/></argument>\n		<uses name=\"wordlist2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000002\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist1\"/> -o <file name=\"analysis1\"/></argument>\n		<uses name=\"analysis1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000003\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist2\"/> -o <file name=\"analysis2\"/></argument>\n		<uses name=\"wordlist2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000004\" namespace=\"wordcount\" name=\"merge\" version=\"1.0\">\n		<argument>-i <file name=\"analysis1\"/> <file name=\"analysis2\"/> -o <file name=\"merge_output\"/></argument>\n		<uses name=\"analysis1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"merge_output\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<invoke when=\"at_end\">/home/ubuntu/peg_mywork/pegasus_notifier</invoke>\n	</job>\n	<child ref=\"ID0000002\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000003\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000004\">\n		<parent ref=\"ID0000002\"/>\n		<parent ref=\"ID0000003\"/>\n	</child>\n</adag>\n','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n    <sitecatalog xmlns=\"http://pegasus.isi.edu/schema/sitecatalog\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd\" version=\"3.0\">\n    <site  handle=\"local\" arch=\"x86\" os=\"LINUX\">\n    <head-fs>\n    <scratch>\n    <shared>\n    <file-server protocol=\"file\" url=\"file://\" mount-point=\"./work\"/>\n    <internal-mount-point mount-point=\"./work\"/>\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"file\" url=\"file://\" mount-point=\"./outputs\"/>\n<internal-mount-point mount-point=\"./outputs\"/>\n</shared>\n<!--shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018143932\"/>\n<internal-mount-point mount-point=\"./outputs\" />\n</shared -->\n</storage>\n</head-fs>\n<profile namespace=\"env\" key=\"S3CFG\">/home/khawar/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/home/khawar/pegasus-4.2.0</profile>\n</site>\n<site handle=\"s3\" arch=\"x86_64\" os=\"LINUX\">\n<head-fs>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018143932\"/>\n<internal-mount-point mount-point=\"/wfoutput11022018143932\" />\n</shared>\n</storage>\n</head-fs>\n</site>\n<site  handle=\"condorpool\" arch=\"x86\" os=\"LINUX\">\n<head-fs>\n<scratch >\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wflocalscratch11022018143932\"/>\n<internal-mount-point mount-point=\"/wflocalscratch11022018143932\" />\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfshared11022018143932\"/>\n<internal-mount-point mount-point=\"/wfshared11022018143932\" />\n</shared>\n</storage>\n</head-fs>\n<profile namespace=\"pegasus\" key=\"style\" >condor</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/opt/pegasus/default</profile>\n<profile namespace=\"env\" key=\"S3CFG\">/opt/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PYTHONPATH\">/opt/parallel_jobs/peg_jobs/wordcount:/opt/parallel_jobs/peg_jobs/dummywf:/opt/parallel_jobs/peg_jobs/dynamic_req</profile>\n<profile namespace=\"condor\" key=\"universe\">vanilla</profile>\n<profile namespace=\"condor\" key=\"requirements\" >(Target.Arch == \"X86_64\")</profile>\n</site>\n</sitecatalog>',NULL,'pegasus.dir.storage.deep=false\n        pegasus.catalog.replica.db.url=jdbc:mysql://localhost:3306/pegasusdb\n        pegasus.catalog.transformation=File\n        dagman.maxpre=2\n        pegasus.home.schemadir=/home/ubuntu/pegasus-4.2/share/pegasus/schema\n        pegasus.execute.*.filesystem.local=true\n        pegasus.condor.logs.symlink=false\n        pegasus.monitord.events=true\n        pegasus.catalog.transformation.file=/home/ubuntu/WrapperService/peg_files/tc.data\n        pegasus.data.configuration=nonsharedfs\n        pegasus.catalog.provenance=InvocationSchema\n        pegasus.catalog.site.file=/home/ubuntu/WrapperService/peg_files/tmpvgBUqj/sites.xml\n        pegasus.gridstart=PegasusLite\n        pegasus.catalog.site=XML3\n        pegasus.monitord.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.home.sysconfdir=/home/ubuntu/pegasus-4.2/etc\n        pegasus.catalog.replica.db.password=oxy123\n        pegasus.catalog.replica.db.driver=MySQL\n        pegasus.home.bindir=/home/khawar/pegasus-4.2/bin\n        pegasus.transfer.worker.package=true\n        pegasus.home.sharedstatedir=/home/ubuntu/pegasus-4.2.0/share/pegasus\n        pegasus.dashboard.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.catalog.replica.db.user=pegasus',NULL,NULL),(4,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- generated: 2014-11-30 23:02:07.539219 -->\n<!-- generated by: khawar -->\n<!-- generator: python -->\n<adag xmlns=\"http://pegasus.isi.edu/schema/DAX\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/DAX http://pegasus.isi.edu/schema/dax-3.4.xsd\" version=\"3.4\"\nname=\"WordCountWF\">\n	<invoke when=\"at_end\">/usr/bin/mail -s &quot;Workflow finished&quot; khawarhasham@gmail.com</invoke>\n	<invoke when=\"on_success\">/usr/bin/mail -s &quot;publish result&quot; khawarhasham@gmail.com</invoke>\n	<file name=\"wordlist\">\n		<pfn url=\"file:///opt/parallel_jobs/wordfile\" site=\"local\"/>\n	</file>\n	<executable name=\"merge\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/merge.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"preprocess\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"analyse\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/analyse.py\" site=\"condorpool\"/>\n	</executable>\n	<job id=\"ID0000001\" namespace=\"wordcount\" name=\"preprocess\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist\"/> -o <file name=\"wordlist1\"/> <file name=\"wordlist2\"/></argument>\n		<uses name=\"wordlist2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000002\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist1\"/> -o <file name=\"analysis1\"/></argument>\n		<uses name=\"analysis1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000003\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist2\"/> -o <file name=\"analysis2\"/></argument>\n		<uses name=\"wordlist2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000004\" namespace=\"wordcount\" name=\"merge\" version=\"1.0\">\n		<argument>-i <file name=\"analysis1\"/> <file name=\"analysis2\"/> -o <file name=\"merge_output\"/></argument>\n		<uses name=\"analysis1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"merge_output\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<invoke when=\"at_end\">/home/ubuntu/peg_mywork/pegasus_notifier</invoke>\n	</job>\n	<child ref=\"ID0000002\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000003\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000004\">\n		<parent ref=\"ID0000002\"/>\n		<parent ref=\"ID0000003\"/>\n	</child>\n</adag>\n','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n    <sitecatalog xmlns=\"http://pegasus.isi.edu/schema/sitecatalog\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd\" version=\"3.0\">\n    <site  handle=\"local\" arch=\"x86\" os=\"LINUX\">\n    <head-fs>\n    <scratch>\n    <shared>\n    <file-server protocol=\"file\" url=\"file://\" mount-point=\"./work\"/>\n    <internal-mount-point mount-point=\"./work\"/>\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"file\" url=\"file://\" mount-point=\"./outputs\"/>\n<internal-mount-point mount-point=\"./outputs\"/>\n</shared>\n<!--shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018144424\"/>\n<internal-mount-point mount-point=\"./outputs\" />\n</shared -->\n</storage>\n</head-fs>\n<profile namespace=\"env\" key=\"S3CFG\">/home/khawar/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/home/khawar/pegasus-4.2.0</profile>\n</site>\n<site handle=\"s3\" arch=\"x86_64\" os=\"LINUX\">\n<head-fs>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018144424\"/>\n<internal-mount-point mount-point=\"/wfoutput11022018144424\" />\n</shared>\n</storage>\n</head-fs>\n</site>\n<site  handle=\"condorpool\" arch=\"x86\" os=\"LINUX\">\n<head-fs>\n<scratch >\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wflocalscratch11022018144424\"/>\n<internal-mount-point mount-point=\"/wflocalscratch11022018144424\" />\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfshared11022018144424\"/>\n<internal-mount-point mount-point=\"/wfshared11022018144424\" />\n</shared>\n</storage>\n</head-fs>\n<profile namespace=\"pegasus\" key=\"style\" >condor</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/opt/pegasus/default</profile>\n<profile namespace=\"env\" key=\"S3CFG\">/opt/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PYTHONPATH\">/opt/parallel_jobs/peg_jobs/wordcount:/opt/parallel_jobs/peg_jobs/dummywf:/opt/parallel_jobs/peg_jobs/dynamic_req</profile>\n<profile namespace=\"condor\" key=\"universe\">vanilla</profile>\n<profile namespace=\"condor\" key=\"requirements\" >(Target.Arch == \"X86_64\")</profile>\n</site>\n</sitecatalog>',NULL,'pegasus.dir.storage.deep=false\n        pegasus.catalog.replica.db.url=jdbc:mysql://localhost:3306/pegasusdb\n        pegasus.catalog.transformation=File\n        dagman.maxpre=2\n        pegasus.home.schemadir=/home/ubuntu/pegasus-4.2/share/pegasus/schema\n        pegasus.execute.*.filesystem.local=true\n        pegasus.condor.logs.symlink=false\n        pegasus.monitord.events=true\n        pegasus.catalog.transformation.file=/home/ubuntu/WrapperService/peg_files/tc.data\n        pegasus.data.configuration=nonsharedfs\n        pegasus.catalog.provenance=InvocationSchema\n        pegasus.catalog.site.file=/home/ubuntu/WrapperService/peg_files/tmp_k6n32/sites.xml\n        pegasus.gridstart=PegasusLite\n        pegasus.catalog.site=XML3\n        pegasus.monitord.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.home.sysconfdir=/home/ubuntu/pegasus-4.2/etc\n        pegasus.catalog.replica.db.password=oxy123\n        pegasus.catalog.replica.db.driver=MySQL\n        pegasus.home.bindir=/home/ubuntu/pegasus-4.2/bin\n        pegasus.transfer.worker.package=true\n        pegasus.home.sharedstatedir=/home/ubuntu/pegasus-4.2/share/pegasus\n        pegasus.dashboard.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.catalog.replica.db.user=pegasus',NULL,NULL),(5,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- generated: 2014-11-30 23:02:07.539219 -->\n<!-- generated by: khawar -->\n<!-- generator: python -->\n<adag xmlns=\"http://pegasus.isi.edu/schema/DAX\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/DAX http://pegasus.isi.edu/schema/dax-3.4.xsd\" version=\"3.4\"\nname=\"WordCountWF\">\n	<invoke when=\"at_end\">/usr/bin/mail -s &quot;Workflow finished&quot; khawarhasham@gmail.com</invoke>\n	<invoke when=\"on_success\">/usr/bin/mail -s &quot;publish result&quot; khawarhasham@gmail.com</invoke>\n	<file name=\"wordlist\">\n		<pfn url=\"file:///opt/parallel_jobs/wordfile\" site=\"local\"/>\n	</file>\n	<executable name=\"merge\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/merge.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"preprocess\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/pre.py\" site=\"condorpool\"/>\n	</executable>\n	<executable name=\"analyse\" namespace=\"wordcount\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"true\">\n		<pfn url=\"file:///opt/parallel_jobs/peg_jobs/wordcount/analyse.py\" site=\"condorpool\"/>\n	</executable>\n	<job id=\"ID0000001\" namespace=\"wordcount\" name=\"preprocess\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist\"/> -o <file name=\"wordlist1\"/> <file name=\"wordlist2\"/></argument>\n		<uses name=\"wordlist2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000002\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist1\"/> -o <file name=\"analysis1\"/></argument>\n		<uses name=\"analysis1\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"wordlist1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000003\" namespace=\"wordcount\" name=\"analyse\" version=\"1.0\">\n		<argument>-i <file name=\"wordlist2\"/> -o <file name=\"analysis2\"/></argument>\n		<uses name=\"wordlist2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"output\" register=\"true\" transfer=\"true\"/>\n	</job>\n	<job id=\"ID0000004\" namespace=\"wordcount\" name=\"merge\" version=\"1.0\">\n		<argument>-i <file name=\"analysis1\"/> <file name=\"analysis2\"/> -o <file name=\"merge_output\"/></argument>\n		<uses name=\"analysis1\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"analysis2\" link=\"input\" register=\"true\" transfer=\"true\"/>\n		<uses name=\"merge_output\" link=\"output\" register=\"true\" transfer=\"true\"/>\n		<invoke when=\"at_end\">/home/ubuntu/peg_mywork/pegasus_notifier</invoke>\n	</job>\n	<child ref=\"ID0000002\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000003\">\n		<parent ref=\"ID0000001\"/>\n	</child>\n	<child ref=\"ID0000004\">\n		<parent ref=\"ID0000002\"/>\n		<parent ref=\"ID0000003\"/>\n	</child>\n</adag>\n','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n    <sitecatalog xmlns=\"http://pegasus.isi.edu/schema/sitecatalog\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-3.0.xsd\" version=\"3.0\">\n    <site  handle=\"local\" arch=\"x86\" os=\"LINUX\">\n    <head-fs>\n    <scratch>\n    <shared>\n    <file-server protocol=\"file\" url=\"file://\" mount-point=\"./work\"/>\n    <internal-mount-point mount-point=\"./work\"/>\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"file\" url=\"file://\" mount-point=\"./outputs\"/>\n<internal-mount-point mount-point=\"./outputs\"/>\n</shared>\n<!--shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018145155\"/>\n<internal-mount-point mount-point=\"./outputs\" />\n</shared -->\n</storage>\n</head-fs>\n<profile namespace=\"env\" key=\"S3CFG\">/home/khawar/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/home/khawar/pegasus-4.2.0</profile>\n</site>\n<site handle=\"s3\" arch=\"x86_64\" os=\"LINUX\">\n<head-fs>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfoutput11022018145155\"/>\n<internal-mount-point mount-point=\"/wfoutput11022018145155\" />\n</shared>\n</storage>\n</head-fs>\n</site>\n<site  handle=\"condorpool\" arch=\"x86\" os=\"LINUX\">\n<head-fs>\n<scratch >\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wflocalscratch11022018145155\"/>\n<internal-mount-point mount-point=\"/wflocalscratch11022018145155\" />\n</shared>\n</scratch>\n<storage>\n<shared>\n<file-server protocol=\"s3\" url=\"s3://admin@uweopenstack\" mount-point=\"/wfshared11022018145155\"/>\n<internal-mount-point mount-point=\"/wfshared11022018145155\" />\n</shared>\n</storage>\n</head-fs>\n<profile namespace=\"pegasus\" key=\"style\" >condor</profile>\n<profile namespace=\"env\" key=\"PEGASUS_HOME\" >/opt/pegasus/default</profile>\n<profile namespace=\"env\" key=\"S3CFG\">/opt/.s3cfg</profile>\n<profile namespace=\"env\" key=\"PYTHONPATH\">/opt/parallel_jobs/peg_jobs/wordcount:/opt/parallel_jobs/peg_jobs/dummywf:/opt/parallel_jobs/peg_jobs/dynamic_req</profile>\n<profile namespace=\"condor\" key=\"universe\">vanilla</profile>\n<profile namespace=\"condor\" key=\"requirements\" >(Target.Arch == \"X86_64\")</profile>\n</site>\n</sitecatalog>',NULL,'pegasus.dir.storage.deep=false\n        pegasus.catalog.replica.db.url=jdbc:mysql://localhost:3306/pegasusdb\n        pegasus.catalog.transformation=File\n        dagman.maxpre=2\n        pegasus.home.schemadir=/home/ubuntu/pegasus-4.2/share/pegasus/schema\n        pegasus.execute.*.filesystem.local=true\n        pegasus.condor.logs.symlink=false\n        pegasus.monitord.events=true\n        pegasus.catalog.transformation.file=/home/ubuntu/WrapperService/peg_files/tc.data\n        pegasus.data.configuration=nonsharedfs\n        pegasus.catalog.provenance=InvocationSchema\n        pegasus.catalog.site.file=/home/ubuntu/WrapperService/peg_files/tmpz4Rghz/sites.xml\n        pegasus.gridstart=PegasusLite\n        pegasus.catalog.site=XML3\n        pegasus.monitord.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.home.sysconfdir=/home/ubuntu/pegasus-4.2/etc\n        pegasus.catalog.replica.db.password=oxy123\n        pegasus.catalog.replica.db.driver=MySQL\n        pegasus.home.bindir=/home/ubuntu/pegasus-4.2/bin\n        pegasus.transfer.worker.package=true\n        pegasus.home.sharedstatedir=/home/ubuntu/pegasus-4.2/share/pegasus\n        pegasus.dashboard.output=mysql://pegasus:oxy123@localhost:3306/pegasusdb\n        pegasus.catalog.replica.db.user=pegasus',NULL,583),(6,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- generated: 2018-02-11 15:44:07.024687 -->\n<!-- generated by: ubuntu -->\n<!-- generator: python -->\n<adag xmlns=\"http://pegasus.isi.edu/schema/DAX\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/DAX http://pegasus.isi.edu/schema/dax-3.4.xsd\" version=\"3.4\" name=\"hello_world\">\n        <file name=\"f.a\">\n                <pfn url=\"file:///home/ubuntu/pegasus-4.2/share/pegasus/examples/hello-world/f.a\" site=\"local\"/>\n        </file>\n        <executable name=\"hello\" namespace=\"hello_world\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"false\">\n                <pfn url=\"file:///home/ubuntu/pegasus-4.2/share/pegasus/examples/hello-world/hello.sh\" site=\"condorpool\"/>\n        </executable>\n        <executable name=\"world\" namespace=\"hello_world\" version=\"1.0\" arch=\"x86_64\" os=\"linux\" installed=\"false\">\n                <pfn url=\"file:///home/ubuntu/pegasus-4.2/share/pegasus/examples/hello-world/world.sh\" site=\"condorpool\"/>\n        </executable>\n        <job id=\"ID0000001\" namespace=\"hello_world\" name=\"hello\" version=\"1.0\">\n                <uses name=\"f.b\" link=\"output\"/>\n                <uses name=\"f.a\" link=\"input\"/>\n        </job>\n        <job id=\"ID0000002\" namespace=\"hello_world\" name=\"world\" version=\"1.0\">\n                <uses name=\"f.b\" link=\"input\"/>\n                <uses name=\"f.c\" link=\"output\"/>\n        </job>\n        <child ref=\"ID0000002\">\n                <parent ref=\"ID0000001\"/>\n        </child>\n</adag>','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<sitecatalog xmlns=\"http://pegasus.isi.edu/schema/sitecatalog\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://pegasus.isi.edu/schema/sitecatalog http://pegasus.isi.edu/schema/sc-4.0.xsd\" version=\"4.0\">\n\n    <site  handle=\"local\" arch=\"x86_64\" os=\"LINUX\">\n        <directory type=\"shared-scratch\" path=\"/home/ubuntu/pegasus-4.2/share/pegasus/examples/hello-world/work\">\n            <file-server operation=\"all\" url=\"file:///home/ubuntu/pegasus-4.2/share/pegasus/examples/hello-world/work\"/>\n        </directory>\n        <directory type=\"local-storage\" path=\"/home/ubuntu/pegasus-4.2/share/pegasus/examples/hello-world/outputs\">\n            <file-server operation=\"all\" url=\"file:///home/ubuntu/pegasus-4.2/share/pegasus/examples/hello-world/outputs\"/>\n        </directory>\n    </site>\n\n    <site  handle=\"condorpool\" arch=\"x86_64\" os=\"LINUX\">\n        <profile namespace=\"pegasus\" key=\"style\" >condor</profile>\n        <profile namespace=\"condor\" key=\"universe\" >vanilla</profile>\n    </site>\n\n</sitecatalog>',NULL,'pegasus.monitord.events       true\npegasus.monitord.output        mysql://pegasus:oxy123@localhost:3306/pegasusdb\npegasus.catalog.provenance=InvocationSchema\npegasus.catalog.*.db.driver = MySQL\npegasus.catalog.*.db.url = jdbc:mysql://localhost:3306/pegasusdb\npegasus.catalog.*.db.user = pegasus\npegasus.catalog.*.db.password = oxy123\npegasus.catalog.site=XML\npegasus.catalog.site.file=sites.xml\n\npegasus.dir.useTimestamp=true\npegasus.dir.storage.deep=false\npegasus.condor.logs.symlink=false\n\npegasus.data.configuration = condorio',NULL,591);
/*!40000 ALTER TABLE `WorkflowSource` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-18 12:06:33
